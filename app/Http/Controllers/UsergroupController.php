<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

class UsergroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.usergroup.index');
    }


    public function get() {
        $group = Group::all();

        return datatables()->of($group)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->addColumn('publication_status', function ($group) {
                if ($group->publication_status == 1) {
                    return '<a href="' . route('admin.unpublishedUsergroupRoute', $group->id) . '" class="btn btn-success btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Unpublished"><i class="icon fa fa-arrow-down"></i>Published</a>';
                }
                return '<a href="' . route('admin.publishedUsergroupRoute', $group->id) . '" class="btn btn-warning btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Published"><i class="icon fa fa-arrow-up"></i> Unpublished</a>';})
            ->addColumn('action', function ($group) {
                return '<button class="btn btn-info btn-xs view-button" data-id="' . $group->id . '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button> 
                    <button class="btn btn-primary btn-xs edit-button" data-id="' . $group->id . '"data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
                    <button class="btn btn-danger btn-xs delete-button" data-id="' . $group->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})
            ->rawColumns(['publication_status', 'action'])
            ->setRowId('id')
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'group_name'       => 'required',
            'publication_status' => 'required',
        ], [
            'group_name.required'  => 'Nama group harus diisi.',
        ]);

        if ($validator->passes()) {
            $group = Group::create([
                'group_name'           => $request->input('group_name'),
                'publication_status'    => $request->input('publication_status'),
            ]);

            $group_id = $group->id;

            if (!empty($group_id)) {
                return Response::json(['status' => true,'message' => 'Data add successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }

        }

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id=null)
    {
        if($id==null){
            $data['data'] = Group::where([['group_name','LIKE',"%{$request->get('q')}%"]])->whereNotIn('publication_status', [0])->get(['id', 'group_name']);
        }
        else{
            $data['group'] = Group::where('id', $id)->first();
        }

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = Group::find($id);

        $validator = $validator = Validator::make($request->all(), [
            'group_name'       => 'required',
            'publication_status' => 'required',
        ], [
            'group_name.required'  => 'Nama group harus diisi.',
        ]);

        if ($validator->passes()) {

            $group->group_name = $request->get('group_name');
            $group->publication_status = $request->get('publication_status');
            $affected_row = $group->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => true,'message' => 'Data update successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }

        }

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Group::find($id);
        if (count($data)) {
            $data->delete();
            return redirect()->back()->with('message', 'Data delete successfully.');
        } else {
            return redirect()->back()->with('exception', 'Operation failed !');
        }
    }


    public function published($id) {
        $affected_row = Group::where('id', $id)
            ->update(['publication_status' => 1]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Published successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    public function unpublished($id) {
        $affected_row = Group::where('id', $id)
            ->update(['publication_status' => 0]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Unpublished successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }
}
