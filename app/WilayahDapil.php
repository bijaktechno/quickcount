<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WilayahDapil extends Model
{
	protected $table = 'wilayah_dapil';

    protected $fillable = [
        'group_id', 'dapil_id', 'dapil_name', 'kabupaten_id', 'kecamatan_id', 'desa_id', 'kabupaten_name', 'kecamatan_name', 'desa_name'
    ];
}
