<?php

namespace App\Http\Controllers\api;

use App\UserVotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'caleg_id'  => 'required',
            'user_id'   => 'required',
            'score'     => 'required',
        ], [
            'score.required'     => 'score harus diisi.',
        ]);

        if ($validator->passes()) {
            $uservotes = UserVotes::create([
                'caleg_id'     => $request->input('caleg_id'),
                'user_id'      => $request->input('user_id'),
                'score'        => $request->input('score'),
            ]);

            $uservotes_id = $uservotes->id;

            if (!empty($uservotes_id)) {
                return Response::json(['status' => true,'message' => 'Data add successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }

        }

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
