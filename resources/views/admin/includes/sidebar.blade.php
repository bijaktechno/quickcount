   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        @if(!empty(Auth::user()->avatar))
        <img src="{{ asset('public/avatar/' . Auth::user()->avatar) }}" class="img-circle" alt="{{ Auth::user()->name }}">
        @else
        <img src="{{ asset('public/avatar/user.png') }}" class="img-circle" alt="{{ Auth::user()->name }}">
        @endif
      </div>
      <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <div id="mainMenu">
      <ul class="sidebar-menu" data-widget="tree">
        <!-- <li class="{{ Request::is('admin/dashboard') ? "active" : '' }}"><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li> -->
        <li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home text-yellow"></i> <span>Dashboard</span></a></li>
        @if(isAdmin())
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file text-yellow"></i> <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin.dapil.index') }}"><i class="fa fa-circle-o"></i> Dapil</a></li>
            <li><a href="{{ route('admin.kabupaten.index') }}"><i class="fa fa-circle-o"></i> Kabupaten</a></li>
            <li><a href="{{ route('admin.kecamatan.index') }}"><i class="fa fa-circle-o"></i> Kecamatan</a></li>
            <li><a href="{{ route('admin.desa.index') }}"><i class="fa fa-circle-o"></i> Desa</a></li>
            <li><a href="{{ route('admin.tps.index') }}"><i class="fa fa-circle-o"></i> TPS</a></li>
          </ul>
        </li>

        <li><a href="{{ route('admin.caleg.index') }}"><i class="fa fa-users text-yellow"></i> <span>Caleg</span></a></li>
        @endif

        <li class="treeview">
          <a href="#">
            <i class="fa fa-list text-yellow"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin.vote.index') }}"><i class="fa fa-circle-o"></i> Rekap Caleg</a></li>
            <li><a href="{{ route('admin.parpol.index') }}"><i class="fa fa-circle-o"></i> Rekap Partai</a></li>
            <li><a href="{{ route('admin.vote-compare.index') }}"><i class="fa fa-circle-o"></i> Prosentase Suara</a></li>
            <li><a href="{{ route('admin.caleg-vote.index') }}"><i class="fa fa-circle-o"></i> Perolehan Percaleg</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-user text-yellow"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if(isAdmin())
            <li><a href="{{ route('admin.usergroup.index') }}"><i class="fa fa-circle-o"></i> User Group</a></li>
            @endif
            <li><a href="{{ route('admin.users.index') }}"><i class="fa fa-circle-o"></i> User List</a></li>
          </ul>
        </li>
        <li><a href="{{ route('admin.setting.index') }}"><i class="fa fa-gears text-yellow"></i> <span>Setting</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="{{ route('admin.profile.index') }}"><i class="fa fa-user text-yellow"></i> <span>Profile</span></a></li>
        <li>
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-lock text-yellow"></i>Sign out</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
    </div>
  </section>
    <!-- /.sidebar -->