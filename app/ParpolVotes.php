<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParpolVotes extends Model
{
	protected $table = 'parpol_votes';

    protected $fillable = [
        'parpol_id', 'tps_id', 'valid', 'invalid'
    ];

    public function parpol() {
        return $this->belongsTo(Parpol::class);
    }
}
