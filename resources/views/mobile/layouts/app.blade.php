<!DOCTYPE html>
<html lang="en">
<!-- head -->
<head>
    @include('mobile.includes.head')
    @yield('style')
</head>
<!-- /.head -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">

    <!-- Preloader-->
    <div id="preloader">
      <div id="status"></div>
    </div>

    <!-- left section -->
    @yield('content')
    <!-- /. left section -->

    <!-- /. right side widget -->
    @yield('sidebar')
    <!--/. right side widget-->

    
    <!-- scripts -->
    @include('mobile.includes.scripts')
    @yield('script')
    <!-- /. script -->
</body>
</html>