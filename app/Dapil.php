<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dapil extends Model
{
	protected $table = 'dapil';

    protected $fillable = [
        'dapil_name', 'group_id', 'kab_id','kec_id','desa_id', 'publication_status'
    ];

    public function kabupaten() {
        return $this->hasMany(Kabupaten::class);
    }

    public function caleg() {
        return $this->hasMany(Caleg::class);
    }

    public function dapilcalegtps() {
        return $this->hasOne(DapilCalegtps::class, 'dapil_id');
    }

    public function caleg_group() {
        return $this->belongsTo(CalegGroup::class);
    }

}
