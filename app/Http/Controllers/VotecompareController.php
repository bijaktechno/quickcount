<?php

namespace App\Http\Controllers;

use App\Caleg;
use App\UserVotes;
use App\DapilCalegtps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class VotecompareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.vote.compare');
    }

    public function get(Request $request) {
        $to = $request->get('order')[0]['column'];
        $field = $request->get('columns')[$to]['name'];

        $count = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                ->leftJoin('users', 'users.id', '=', 'user_votes.user_id')
                ->leftJoin('tps', 'tps.id', '=', 'users.tps_id')
                ->leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                ->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
                ->when(!empty($request->get('caleg_group_id')) , function ($query) use($request){
                    return $query->where('dapil.group_id', $request->get('caleg_group_id'));
                })
                ->when(!empty($request->get('dapil_id')) , function ($query) use($request){
                    return $query->where('dapil.id', $request->get('dapil_id'));
                })
                ->when(!empty($request->get('kabupaten_id')) , function ($query) use($request){
                    return $query->where('kabupaten.id', $request->get('kabupaten_id'));
                })
                ->when(!empty($request->get('kecamatan_id')) , function ($query) use($request){
                    return $query->where('kecamatan.id', $request->get('kecamatan_id'));
                })
                ->when(!empty($request->get('desa_id')) , function ($query) use($request){
                    return $query->where('desa.id', $request->get('desa_id'));
                })
                ->when(!empty($request->get('tps_id')) , function ($query) use($request){
                    return $query->where('tps.id', $request->get('tps_id'));
                })
                ->select(array('tps.jumlah_dpt', 'tps.tps_name', 'desa.desa_name', 'kecamatan.kecamatan_name', 'kabupaten.kabupaten_name', 'dapil.dapil_name', DB::raw('SUM(score) as score_average')))
                ->groupBy('tps.jumlah_dpt', 'tps.tps_name', 'desa.desa_name', 'kecamatan.kecamatan_name', 'kabupaten.kabupaten_name', 'dapil.dapil_name')
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('tps.tps_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kabupaten.kabupaten_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('dapil.dapil_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->get()
                ->count();

        $caleg = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                ->leftJoin('users', 'users.id', '=', 'user_votes.user_id')
                ->leftJoin('tps', 'tps.id', '=', 'users.tps_id')
                ->leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                ->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
                ->when(!empty($request->get('caleg_group_id')) , function ($query) use($request){
                    return $query->where('dapil.group_id', $request->get('caleg_group_id'));
                })
                ->when(!empty($request->get('dapil_id')) , function ($query) use($request){
                    return $query->where('dapil.id', $request->get('dapil_id'));
                })
                ->when(!empty($request->get('kabupaten_id')) , function ($query) use($request){
                    return $query->where('kabupaten.id', $request->get('kabupaten_id'));
                })
                ->when(!empty($request->get('kecamatan_id')) , function ($query) use($request){
                    return $query->where('kecamatan.id', $request->get('kecamatan_id'));
                })
                ->when(!empty($request->get('desa_id')) , function ($query) use($request){
                    return $query->where('desa.id', $request->get('desa_id'));
                })
                ->when(!empty($request->get('tps_id')) , function ($query) use($request){
                    return $query->where('tps.id', $request->get('tps_id'));
                })
                ->select(array('tps.jumlah_dpt', 'tps.tps_name', 'desa.desa_name', 'kecamatan.kecamatan_name', 'kabupaten.kabupaten_name', 'dapil.dapil_name', DB::raw('SUM(score) as score_average')))
                ->groupBy('tps.jumlah_dpt', 'tps.tps_name', 'desa.desa_name', 'kecamatan.kecamatan_name', 'kabupaten.kabupaten_name', 'dapil.dapil_name')
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('tps.tps_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kabupaten.kabupaten_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('dapil.dapil_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->orderBy($field, $request->get('order')[0]['dir'])
                ->offset($request->get('start'))
                ->limit($request->get('length'))
                ->get();


        $data = array(
            'recordsTotal' => $count, 
            'recordsFiltered' => $count, 
            'data' => $caleg
        );

        return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
