@extends('admin.layouts.app')
@section('title', 'Rekap Suara')


@section('style')
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css"> -->

	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/dataTables.bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/buttons.dataTables.min.css')}}">
  	<link rel="stylesheet" href="{{ asset('public/admin/css/jquery.inputpicker.css')}}">
	@endsection

	@section('content')
	<!-- Page header -->
	<section class="content-header">
		<h1>
			Rekap Suara
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> Dashboard</a></li>
			<li class="active">Rekap Suara</li>
		</ol>
	</section>
	<!-- /.page header -->

	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<!-- <h3 class="box-title">Manage Rekap Suara</h3> -->

  				<div id="filter-caleg" class="form-inline row" style="width: 100%;">
  					@if(isAdmin())
			        <div class="col-lg-2">
			          <div class="form-group">
			            <input type="text" name="caleg_group_id" id="caleg_group_id" class="form-control" placeholder="Nama Group Caleg" value="2">
			            <!-- <input type="text" class="form-control" aria-label="..."> -->
			          </div>
			        </div>
			        @endif
			        <div class="col-lg-2">
			          <div class="form-group">
			            <input type="text" name="dapil_id" id="dapil_id" class="form-control" placeholder="Nama dapil" value="{{isAuthor()?$user->desa->kecamatan->kabupaten->dapil_id:''}}" {{isAuthor()?'disabled':''}}>
			            <!-- <input type="text" class="form-control" aria-label="..."> -->
			          </div>
			        </div>
			        <div class="col-lg-2">
			          <div id="form_kabupaten_id" class="form-group">
			            <input type="text" name="kabupaten_id" id="kabupaten_id" class="form-control" placeholder="Nama kabupaten" value="{{isAuthor()?$user->desa->kecamatan->kabupaten_id:''}}" {{isAuthor()?'disabled':''}}>
			            <!-- <input type="text" class="form-control" aria-label="..."> -->
			          </div>
			        </div>
			        <div class="col-lg-2">
			          <div id="form_kecamatan_id" class="form-group">
			            <input type="text" name="kecamatan_id" id="kecamatan_id" class="form-control" placeholder="Nama kecamatan">
			            <!-- <input type="text" class="form-control" aria-label="..."> -->
			          </div>
			        </div>
			        <div class="col-lg-2">
			          <div id="form_desa_id" class="form-group">
			            <input type="text" name="desa_id" id="desa_id" class="form-control" placeholder="Nama desa">
			            <!-- <input type="text" class="form-control" aria-label="..."> -->
			          </div>
			        </div>
			        <div class="col-lg-2">
			          <div id="form_tps_id" class="form-group">
			            <input type="text" name="tps_id" id="tps_id" class="form-control" placeholder="Nama tps">
			            <!-- <input type="text" class="form-control" aria-label="..."> -->
			          </div>
			        </div>

					<!-- <button type="button" class="btn btn-info btn-sm btn-flat add-button"><i class="fa fa-plus"></i> Add Rekap Suara</button> -->
				</div>

			</div>
			<!-- /.box-header -->
			<div id="cart-area" class="box-body">

	            <ul class="nav nav-tabs" role="tablist">
	                <li role="presentation" class="active"><a href="#chart" aria-controls="chart" role="tab" data-toggle="tab">Grafik</a></li>
	                <li role="presentation"><a href="#table" aria-controls="table" role="tab" data-toggle="tab">Table</a></li>
	            </ul>

	            <div class="tab-content">
	                <div role="tabpanel" class="tab-pane active" id="chart">


				      	<canvas id="calegChart" style="max-width: 100%;"></canvas>

	            	</div>

	                <div role="tabpanel" class="tab-pane" id="table">
						<div style="width: 100%; padding-left: -10px; padding-top: 20px;">
				                	
							<div class="table-responsive">
								<table id="vote-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0">
									<thead>
										<tr>
											<th>Nama Caleg</th>
											<th>Nama Dapil</th>
											<th>Nama Kabupaten</th>
											<th>Nama Kecamatan</th>
											<th>Nama Desa</th>
											<th>Nama TPS</th>
											<th>Total Suara</th>
										</tr>
									</thead>
								</table>
							</div>

						</div>

	            	</div>
	            </div>



			<!-- /.box-body -->
			<div class="box-footer clearfix">
			</div>
		</div>
	</section>
	<!-- /.main content -->

	<!-- add vote modal -->
	<div id="add-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							<span class="fa-stack fa-sm">
								<i class="fa fa-square-o fa-stack-2x"></i>
								<i class="fa fa-plus fa-stack-1x"></i>
							</span>
							Add Rekap Suara
						</h4>
					</div>
					<form role="form" id="vote_add_form" method="post">
						{{ csrf_field() }}
						<div class="modal-body">
							<!-- datum -->
							<div class="form-group">
								<label for="tag_name">Nama Rekap Suara</label>
								<input type="text" name="" class="form-control" id="" value="{{ old('') }}" placeholder="ex: ">
							</div>
							<div class="form-group">
								<label for="tag_slug">Rekap Suara Slug</label>
								<input type="text" name="" class="form-control" id="" value="{{ old('') }}" placeholder="ex: ">
							</div>

							<div class="form-group">
								<label>Publication Status</label>
								<select class="form-control" name="publication_status" id="publication_status">
									<option selected disabled>Select One</option>
									<option value="1">Published</option>
									<option value="0">Unpublished</option>
								</select>
							</div>

							<div class="bs-callout bs-callout-success">
								<h4>SEO Information</h4>
							</div>

							<div class="form-group">
								<label for="meta_title">Meata Title</label>
								<input type="text" name="meta_title" class="form-control" id="meta_title" value="{{ old('meta_title') }}" placeholder="ex: title">
							</div>
							<div class="form-group">
								<label for="meta_keywords">Meata Keywords</label>
								<input type="text" name="meta_keywords" class="form-control" id="meta_keywords" value="{{ old('meta_keywords') }}" placeholder="ex: keywords">
								<span class="text-danger" id="meta-keywords-error"></span>
							</div>
							<div class="form-group">
								<label for="meta_description">Meta Description</label>
								<textarea name="meta_description" class="form-control" id="meta_description" rows="3" placeholder="ex: dscription">{{ old('meta_description') }}</textarea>
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-info btn-flat" id="store-button">Save changes</button>
						</div>
					</form>

				</div>
		</div>
	</div>
	<!-- /.add vote modal -->

	<!-- view vote modal -->
	<div id="view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="btn-group pull-right no-print">
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
								<i class="fa fa-print"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-remove"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
					</div>
					<h4 class="modal-title" id="view-tag-name"></h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td>Meta Title</td>
								<td id="view-meta-title"></td>
							</tr>
							<tr>
								<td>Meta Keywords</td>
								<td id="view-meta-keywords"></td>
							</tr>
							<tr>
								<td>Meta Description</td>
								<td id="view-meta-description"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer no-print">
					<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /.view vote modal -->

	<!-- delete vote modal -->
	<div id="delete-modal" class="modal modal-danger fade" id="modal-danger">
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">
						<span class="fa-stack fa-sm">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-trash fa-stack-1x"></i>
						</span>
						Are you sure want to delete this ?
					</h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
					<form method="post" role="form" id="delete_form">
						{{csrf_field()}}
						{{method_field('DELETE')}}
						<button type="submit" class="btn btn-outline">Delete</button>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.delete vote modal -->

	<!-- view user modal -->
	<div id="user-view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="btn-group pull-right no-print">
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
								<i class="fa fa-print"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-remove"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
					</div>
					<h4 class="modal-title" id="view-name"></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-9">
							<table class="table table-bordered table-striped">
								<tbody>
									<tr>
										<td width="20%">Role</td>
										<td id="view-role"></td>
									</tr>
									<tr>
										<td>Username</td>
										<td id="view-username"></td>
									</tr>
									<tr>
										<td>Email</td>
										<td id="view-email"></td>
									</tr>
									<tr>
										<td>Gender</td>
										<td id="view-gender"></td>
									</tr>
									<tr>
										<td>Phone</td>
										<td id="view-phone"></td>
									</tr>
									<tr>
										<td>Address</td>
										<td id="view-address"></td>
									</tr>
									<tr>
										<td>Facebook</td>
										<td id="view-facebook"></td>
									</tr>
									<tr>
										<td>Twitter</td>
										<td id="view-twitter"></td>
									</tr>
									<tr>
										<td>Google Plus</td>
										<td id="view-google-plus"></td>
									</tr>
									<tr>
										<td>Linkedin</td>
										<td id="view-linkedin"></td>
									</tr>
									<tr>
										<td>Status</td>
										<td id="view-status"></td>
									</tr>
									<tr>
										<td>About</td>
										<td id="view-about"></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-3">
							<img id="view-avatar" class="img-responsive img-thumbnail img-rounded">
						</div>
					</div>
				</div>
				<div class="modal-footer no-print">
					<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /.view user modal -->
	@endsection

	@section('script')
	<!-- <script type="text/javascript" src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script> -->

	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.colVis.min.js') }}"></script>
  	<script type="text/javascript" src="{{ asset('public/admin/js/jquery.inputpicker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/web/js/mdb.min.js') }}"></script>

	<script type="text/javascript">
	    var dataLabels=[], dataValues=[], dataColor=[], dataTitle='';

	    $(document).ready(function(){

		var caleg_group_id = $("#caleg_group_id").val();
		var dapil_id = $("#dapil_id").val();
		var kabupaten_id = $("#kabupaten_id").val();
		var kecamatan_id = $("#kecamatan_id").val();
		var desa_id = $("#desa_id").val();
      	

      	dataChart();
  		get_table_data();
      	get_caleg_group_picker();


		if(caleg_group_id){
      		get_dapil_picker();
		}
		if(dapil_id){
			get_kabupaten_picker()
		}
		if(kabupaten_id){
			get_kecamatan_picker()			
		}
		if(kecamatan_id){
			get_desa_picker()			
		}
		if(desa_id){
			get_tps_picker()			
		}

	      $("#caleg_group_id").change(function(){
	        
	        $("#dapil_id").val('');
	        $("#kabupaten_id").val('');
	        $("#kecamatan_id").val('');
	        $("#desa_id").val('');
	        $("#tps_id").val('');

	        get_dapil_picker();
	        if ($('#kabupaten_id').hasClass("inputpicker-original")) {
	          $('#kabupaten_id').inputpicker('destroy');
	        }
	        if ($('#kecamatan_id').hasClass("inputpicker-original")) {
	          $('#kecamatan_id').inputpicker('destroy');
	        }
	        if ($('#desa_id').hasClass("inputpicker-original")) {
	          $('#desa_id').inputpicker('destroy');
	        }
	        if ($('#tps_id').hasClass("inputpicker-original")) {
	          $('#tps_id').inputpicker('destroy');
	        }
	      });

	      $("#dapil_id").change(function(){
	        
	        $("#kabupaten_id").val('');
	        $("#kecamatan_id").val('');
	        $("#desa_id").val('');
	        $("#tps_id").val('');

	        get_kabupaten_picker();
	        if ($('#kecamatan_id').hasClass("inputpicker-original")) {
	          $('#kecamatan_id').inputpicker('destroy');
	        }
	        if ($('#desa_id').hasClass("inputpicker-original")) {
	          $('#desa_id').inputpicker('destroy');
	        }
	        if ($('#tps_id').hasClass("inputpicker-original")) {
	          $('#tps_id').inputpicker('destroy');
	        }
	      });

	      $("#kabupaten_id").change(function(){
	        
	        $("#kecamatan_id").val('');
	        $("#desa_id").val('');
	        $("#tps_id").val('');

	        get_kecamatan_picker();
	        if ($('#desa_id').hasClass("inputpicker-original")) {
	          $('#desa_id').inputpicker('destroy');
	        }
	        if ($('#tps_id').hasClass("inputpicker-original")) {
	          $('#tps_id').inputpicker('destroy');
	        }
	      });

	      $("#kecamatan_id").change(function(){
	        
	        $("#desa_id").val('');
	        $("#tps_id").val('');

	        get_desa_picker();
	        if ($('#tps_id').hasClass("inputpicker-original")) {
	          $('#tps_id').inputpicker('destroy');
	        }
	      });

	      $("#desa_id").change(function(){
	        
	        $("#tps_id").val('');

	        get_tps_picker();
	      });

	      $(".btn-votes, #caleg_group_id, #dapil_id, #kabupaten_id, #kecamatan_id, #desa_id, #tps_id").on('click, change', function(){

	        dataLabels = [];
	        dataValues = [];
	        dataColor = [];
	        $("#calegChart").remove();
	        $("#chart").append('<canvas id="calegChart" style="max-width: 100%;"></canvas>');

	        dataChart();

	        $('#vote-table').DataTable().ajax.reload();
	        // update_table_data();

	      })

	    });

	    function get_caleg_group_picker()
	    {
	      $('#caleg_group_id').inputpicker({
	          url: "{{ route('allCaleggroupRoute') }}",
	          // urlParam:[{"category_id":1}],
	          fields:['caleg_group_name'],
	          fieldText:'caleg_group_name',
	          fieldValue:'id',
	          filterOpen: true,
	      });
	    }

	    function get_dapil_picker()
	    {
	    	var caleg_group_id = $("#caleg_group_id").val();

	      $('#dapil_id').inputpicker({
	          url: "{{ route('allDapilRoute') }}",
	          urlParam:{"caleg_group_id":caleg_group_id},
	          fields:['dapil_name'],
	          fieldText:'dapil_name',
	          fieldValue:'id',
	          filterOpen: true,
	      });
	    }

	    function get_kabupaten_picker(){
	    	var dapil_id = $("#dapil_id").val();

	      $('#kabupaten_id').inputpicker({
	          url: "{{ route('allKabupatenRoute') }}",
	          urlParam:{'dapil_id':dapil_id},
	          fields:['kabupaten_name'],
	          fieldText:'kabupaten_name',
	          fieldValue:'id',
	          filterOpen: true,
	      })
	    }

	    function get_kecamatan_picker(){
	    	var kabupaten_id = $("#kabupaten_id").val();

	      $('#kecamatan_id').inputpicker({
	          url: "{{ route('allKecamatanRoute') }}",
	          urlParam:{'kabupaten_id':kabupaten_id},
	          fields:['kecamatan_name'],
	          fieldText:'kecamatan_name',
	          fieldValue:'id',
	          filterOpen: true,
	      })
	    }

	    function get_desa_picker(){
	    	var kecamatan_id = $("#kecamatan_id").val();

	      $('#desa_id').inputpicker({
	          url: "{{ route('allDesaRoute') }}",
	          urlParam:{"kecamatan_id":kecamatan_id},
	          fields:['desa_name'],
	          fieldText:'desa_name',
	          fieldValue:'id',
	          filterOpen: true,
	      })
	    }

	    function get_tps_picker(){
	    	var desa_id = $("#desa_id").val();

	      $('#tps_id').inputpicker({
	          url: "{{ route('allTpsRoute') }}",
	          urlParam:{"desa_id":desa_id},
	          fields:['tps_name'],
	          fieldText:'tps_name',
	          fieldValue:'id',
	          filterOpen: true,
	      })
	    }
	    
    	function dataChart()
	   {
	        var url = "{{ route('allVoteRoute') }}";

	        var caleg_group_id 	= $('#caleg_group_id').val()?$('#caleg_group_id').val():null;
	        var dapil_id    	= $('#dapil_id').val()?$('#dapil_id').val():null;
	        var kabupaten_id	= $('#kabupaten_id').val()?$('#kabupaten_id').val():null;
	        var kecamatan_id	= $('#kecamatan_id').val()?$('#kecamatan_id').val():null;
	        var desa_id     	= $('#desa_id').val()?$('#desa_id').val():null;
	        var tps_id      	= $('#tps_id').val()?$('#tps_id').val():null;

	        var formData = {
	          'caleg_group_id'  : caleg_group_id,
	          'dapil_id'      	: dapil_id,
	          'kabupaten_id'  	: kabupaten_id,
	          'kecamatan_id'  	: kecamatan_id,
	          'desa_id'       	: desa_id,
	          'tps_id'        	: tps_id
	        }


	        $.ajax({
	          url : url,
	          type : 'GET',
	          data: formData,
	          dataType: "json",
	          success:function(data){
	            // console.log(data);
	            $.each(data.data, function(index, value)
	            {

	              dataLabels.push(value.caleg_shortname);
	              dataValues.push(Number(value.score_average));
	              dataColor.push('#'+value.frame_color);
	              dataTitle = value.dapil.dapil_name;
	               /* if (value.length != 0)
	                {
	                    messages += value+"<br>";
	                }*/


	            });

	            if(dapil_id == null) dataTitle = 'All Dapil';


	            // $('#calegChart').data('bar').update(pourcent);
	            // $("#calegChart").remove();

	            getChart();
	            
	          }
	        })
    	}

		function getChart()
		{
	      console.log(dataLabels);
	      console.log(dataValues);
	      console.log(dataColor);

	      var ctx = document.getElementById("calegChart").getContext('2d');
	      var myChart = new Chart(ctx, {
	        type: 'bar',
	        data: {
	          labels: dataLabels,
	          datasets: [{
	            label: dataTitle,
	            data: dataValues,
	            backgroundColor: dataColor,
	            borderColor: dataColor,
	            borderWidth: 1
	          }]
	        },
	        options: {
	          scales: {
	            yAxes: [{
	              ticks: {
	                beginAtZero: true
	              }
	            }]
	          }
	        }
	      });
	    }

		/** Get datatable **/
		function get_table_data(){
			var vote_table = $('#vote-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: {
					"type"   : "GET",
					"url"    : "{{ route('admin.getVoteRoute') }}",
					"data"   : function( d ) {
							      d.caleg_group_id 	= $('#caleg_group_id').val();
							      d.dapil_id 		= $('#dapil_id').val();
							      d.kabupaten_id 	= $('#kabupaten_id').val();
							      d.kecamatan_id 	= $('#kecamatan_id').val();
							      d.desa_id 		= $('#desa_id').val();
							      d.tps_id 			= $('#tps_id').val();
							   },
    				// "dataSrc": ""
				},
				columns: [
				{data: 'caleg_name', name: 'caleg.caleg_name'},
				{data: 'dapil_name', name: 'dapil.dapil_name'},
				{data: 'kabupaten_name', name: 'kabupaten.kabupaten_name'},
				{data: 'kecamatan_name', name: 'kecamatan.kecamatan_name'},
				{data: 'desa_name', name: 'desa.desa_name'},
				{data: 'tps_name', name: 'tps.tps_name'},
				{data: 'score_average', name: 'score_average'},
				],
				order: [[6, 'desc']],
			});
	
			// vote_table.ajax.reload();
		}

	</script>
	@endsection