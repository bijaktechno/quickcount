<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('groups')->insert([
            ["id" => 1, "group_name" => "admin", "publication_status" => 1],
            ["id" => 2, "group_name" => "saksi", "publication_status" => 1],
            ["id" => 3, "group_name" => "admin kabupaten", "publication_status" => 1]
        ]);
    }
}
