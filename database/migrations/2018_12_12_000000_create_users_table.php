<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
            $table->integer('group_id')->unsigned()->index();
            $table->integer('tps_id')->unsigned()->index()->nullable();
            $table->integer('desa_id')->unsigned()->index()->nullable();
			$table->string('nik', 100)->unique();


			$table->string('name', 100);
			$table->string('username', 100)->unique();
			$table->string('email', 100)->unique();
			$table->string('password');
			$table->string('passmd5');
			$table->string('avatar')->nullable();
			$table->string('gender')->nullable();
			$table->string('phone')->nullable();
			$table->string('rt')->nullable();
			$table->string('rw')->nullable();
			$table->string('address')->nullable();
			$table->string('facebook')->nullable();
			$table->string('twitter')->nullable();
			$table->string('google_plus')->nullable();
			$table->string('linkedin')->nullable();
			$table->text('about')->nullable();
			$table->string('role', 50);
			$table->tinyInteger('activation_status')->default(0);
			$table->rememberToken();

            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('tps_id')->references('id')->on('tps')->onDelete('cascade');
            $table->foreign('desa_id')->references('id')->on('desa')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('users');
	}
}
