<nav class="navbar navbar-custom navbar-fixed-top navbar-onepage">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button><a href="#page-top" class="navbar-brand page-scroll">
        <!-- Img or text logo--><img src="{{ asset('public/web/img/logodark.png') }}" alt="" class="logo"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling-->
    @include('web.includes.menubar')
    
  </div>
</nav>