<?php

namespace App\Http\Controllers;
use App\Category;
use App\Comment;
use App\Gallery;
use App\Page;
use App\Post;
use App\Setting;
use App\Subscriber;
use App\Tag;
use App\User;
use App\Dapil;
use App\Caleg;
use App\UserVotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Image;
use Purifier;

class WebController extends Controller {

	public function index() {
		$calegs = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
					// ->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
					->select(array('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', DB::raw('SUM(score) as score_average')
					    ))
					->groupBy('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik')
					->orderBy('score_average', 'desc')
					->get();

		$dpr = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
					->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
					->where('dapil.group_id', 1)
					->select(array('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', DB::raw('SUM(score) as score_average')
					    ))
					->groupBy('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik')
					->orderBy('score_average', 'desc')
					->limit(12)
					->get();

		$dprd1 = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
					->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
					->where('dapil.group_id', 2)
					->select(array('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', DB::raw('SUM(score) as score_average')
					    ))
					->groupBy('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik')
					->orderBy('score_average', 'desc')
					->limit(12)
					->get();


		$dprd2 = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
					->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
					->where('dapil.group_id', 3)
					->select(array('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', DB::raw('SUM(score) as score_average')
					    ))
					->groupBy('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik')
					->orderBy('score_average', 'desc')
					->limit(12)
					->get();

		$calegx = Dapil::leftJoin('caleg', 'caleg.dapil_id', '=', 'dapil.id')
					->leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
					->select(array('dapil.dapil_name', 'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', DB::raw('AVG(score) as score_average')))
					->groupBy('dapil.dapil_name', 'caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik')
					->get();

		$dapils = Dapil::whereNotIn('publication_status', [0])->get();

		$setting = Setting::first();
		
        // return Response::json($dapils);
		return view('web.home', compact('setting', 'calegs', 'dapils', 'dpr', 'dprd1', 'dprd2'));
	}

	public function get(Request $request)
	{

        $data['data'] = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
					->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
					->select(array('caleg.dapil_id', 'dapil.dapil_name', 'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', DB::raw('SUM(score) as score_average')
					    ))
	                ->when(!empty($request->get('caleg_group_id')) , function ($query) use($request){
	                    return $query->where('caleg.caleg_group_id', $request->get('caleg_group_id'));
	                })
	                ->when(!empty($request->get('dapil_id')) , function ($query) use($request){
	                    return $query->where('caleg.dapil_id', $request->get('dapil_id'));
	                })
					->groupBy('caleg.dapil_id', 'dapil.dapil_name', 'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik')
					->orderBy('score_average', 'desc')
					->get();


        return Response::json($data);
	}

	public function mobile()
	{
		$setting = Setting::first(['meta_title', 'meta_keywords', 'meta_description']);
		return view('mobile.home', compact('setting'));
	}

}
