<?php

namespace App\Http\Controllers;

use App\Kabupaten;
use App\Kecamatan;
use App\Desa;
use App\Tps;
use App\Dapil;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {
		$all_kabupaten = Kabupaten::whereNotIn('publication_status', [0])->count();
		$all_kecamatan = Kecamatan::whereNotIn('publication_status', [0])->count();
		$all_desa = Desa::whereNotIn('publication_status', [0])->count();
		$all_tps = Tps::whereNotIn('publication_status', [0])->count();
		/*$all_dapil = Dapil::leftJoin('caleg', 'caleg.dapil_id', '=', 'dapil.id')
					->leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
					->select(array('dapil.dapil_name', DB::raw('AVG(score) as score_average')))
					->groupBy('dapil.dapil_name')
					->get();*/
		$all_dapil = Dapil::whereNotIn('publication_status', [0])->count();
		$all_users = User::count();

		// $comments = Comment::orderBy('created_at', 'desc')->limit(5)->get();
		// $posts = Post::orderBy('created_at', 'desc')->limit(5)->get();
		$users = User::orderBy('created_at', 'desc')->limit(10)->get();

        // return Response::json($all_dapil);
		
		return view('admin.dashboard', compact('all_kabupaten', 'all_kecamatan', 'all_desa', 'all_tps', 'all_users', 'all_dapil', 'users'));
	}
}
