<?php

namespace App\Http\Controllers;
use App\User;
use App\Group;
use App\Kabupaten;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Image;
use Purifier;

class UserController extends Controller {

/*	public function index(Request $request) {
		$users = User::where('users.id', '!=', Auth::user()->id)
                ->leftJoin('groups', 'users.group_id', '=', 'groups.id')
                ->leftJoin('desa', 'desa.id', '=', 'users.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->whereNotIn('groups.group_name', ['admin'])
                ->when(isAuthor() , function ($query){
                    return $query->whereNotIn('groups.group_name', ['admin kabupaten']);
                })
                ->when(isAuthor() , function ($query){
                    return $query->where('kecamatan.kabupaten_id', [Auth::user()->desa->kecamatan->kabupaten_id]);
                })
                ->when(!empty($request->get('kabupaten_id')) , function ($query) use($request){
                    return $query->where('kecamatan.kabupaten_id', $request->get('kabupaten_id'));
                })
                ->select(array('users.*'))
                ->orderBy('users.created_at', 'DESC')->paginate(9);

        $kabupaten = Kabupaten::all();
		return view('admin.user.index', compact('users', 'kabupaten'));
	}*/

    public function index()
    {
        return view('admin.user.index');
    }

    public function get(Request $request) {
        // dd($request->get('order')[0]['column']);
        
        $to = $request->get('order')[0]['column'];
        $field = $request->get('columns')[$to]['name'];
        
        $count = User::where('users.id', '!=', Auth::user()->id)
                ->leftJoin('groups', 'users.group_id', '=', 'groups.id')
                ->leftJoin('desa', 'desa.id', '=', 'users.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->whereNotIn('groups.group_name', ['admin'])
                ->when(isAuthor() , function ($query){
                    return $query->whereNotIn('groups.group_name', ['admin kabupaten']);
                })
                ->when(isAuthor() , function ($query){
                    return $query->where('kecamatan.kabupaten_id', [Auth::user()->desa->kecamatan->kabupaten_id]);
                })
                ->when(!empty($request->get('kabupaten_id')) , function ($query) use($request){
                    return $query->where('kecamatan.kabupaten_id', $request->get('kabupaten_id'));
                })
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('users.name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->count();



        $users = User::where('users.id', '!=', Auth::user()->id)
                ->leftJoin('groups', 'users.group_id', '=', 'groups.id')
                ->leftJoin('desa', 'desa.id', '=', 'users.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->whereNotIn('groups.group_name', ['admin'])
                ->when(isAuthor() , function ($query){
                    return $query->whereNotIn('groups.group_name', ['admin kabupaten']);
                })
                ->when(isAuthor() , function ($query){
                    return $query->where('kecamatan.kabupaten_id', [Auth::user()->desa->kecamatan->kabupaten_id]);
                })
                ->when(!empty($request->get('kabupaten_id')) , function ($query) use($request){
                    return $query->where('kecamatan.kabupaten_id', $request->get('kabupaten_id'));
                })
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('users.name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->select(array('users.*', 'groups.group_name', 'kecamatan.kecamatan_name', 'desa.desa_name'))
                ->orderBy($field, $request->get('order')[0]['dir'])
                ->offset($request->get('start'))
                ->limit($request->get('length'))
                ->get();
        
        // dd($users);

        $data = array(
            'recordsTotal' => $count, 
            'recordsFiltered' => $count, 
            'data' => $users
        );

        // return Response::json($tps);
        return $data;
    }

    public function create()
    {
        $user = Auth::user();

        $groups = Group::whereNotIn('publication_status', [0])
                ->whereNotIn('group_name', ['admin'])
                ->when(isAuthor() , function ($query){
                    return $query->whereNotIn('group_name', ['admin kabupaten']);
                })
                ->get(['id', 'group_name']);

        return view('admin.user.create', compact('groups','user'));
    }


    public function store(Request $request)
    {

        if ($request->group_id == 2) {
            $tps_id = "required|unique:users";
        } else {
            $tps_id = "nullable";
        }

        $validator = $validator = Validator::make($request->all(), [
            'group_id'              => 'required',
            'tps_id'          		=> $tps_id,
            'desa_id'          		=> 'required',
            'nik'             		=> 'required|min:5|max:150|unique:users',
			'name' 					=> 'required|string|max:255',
			'email' 				=> 'required|string|email|max:100|unique:users',
			'username' 				=> 'required|alpha_dash|max:100|unique:users',
			'password' 				=> 'required|string|min:8',
            'avatar'         		=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240|dimensions:max_width=10000,max_height=10000',
            'activation_status'    	=> 'required',
            'gender'          		=> 'required',
        ], [
            'avatar.dimensions' => 'Max dimensions 600x600',
            'tps_id.unique'     => 'TPS Tersebut sudah digunakan oleh user lain',
        ]);

        if ($validator->passes()) {
            $user = User::create([
                'group_id'   			=> $request->input('group_id'),
                'tps_id'    			=> $request->input('tps_id'),
                'desa_id'    			=> $request->input('desa_id'),
                'nik'    				=> $request->input('nik'),
                'name'    				=> $request->input('name'),
                'email'    				=> $request->input('email'),
                'username'    			=> $request->input('username'),
                'password'              => bcrypt($request->input('password')),
                'passmd5'    			=> md5($request->input('password')),
                'activation_status'    	=> $request->input('activation_status'),
                'gender'    			=> $request->input('gender'),
                'role'    				=> 'saksi',
            ]);

            if ($request->hasFile('avatar')) {
                $image = $request->file('avatar');
                $file_name = $this->avatar($user->id, $image);
                User::find($user->id)->update(['avatar' => $file_name]);
            }

            if (!empty($user->id)) {
                return Response::json(['status' => true,'message' => 'Data add successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }
        }   

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    public function avatar($id, $image) {
        $filename = $id . '.' . $image->getClientOriginalExtension();
        $location = get_avatar_path($filename);
        // create new image with transparent background color
        $background = Image::canvas(600, 600);
        // read image file and resize it to 200x200
        $img = Image::make($image);
        // Image Height
        $height = $img->height();
        // Image Width
        $width = $img->width();
        $x = NULL;
        $y = NULL;
        if ($width > $height) {
            $y = 600;
        } else {
            $x = 600;
        }
        //Resize Image
        $img->resize($x, $y, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        // insert resized image centered into background
        $background->insert($img, 'center');
        // save
        $background->save($location);
        return $filename;
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        $groups = Group::whereNotIn('publication_status', [0])
                ->whereNotIn('group_name', ['admin'])
                ->when(isAuthor() , function ($query){
                    return $query->whereNotIn('group_name', ['admin kabupaten']);
                })
                ->get(['id', 'group_name']);

        return view('admin.user.edit', compact('groups', 'user'));
    }

	public function update(Request $request, $id) {
		$user = User::find($id);

		$validator = $validator = Validator::make($request->all(), [
			'role' => 'required',
			'activation_status' => 'required',
		], [
			'activation_status.required' => 'Activation status is required.',
		]);

		if ($validator->passes()) {
			$user->activation_status = $request->get('activation_status');
			$user->role = $request->get('role');
			$affected_row = $user->save();

			if (!empty($affected_row)) {
				$request->session()->flash('message', 'User update successfully.');
			} else {
				$request->session()->flash('exception', 'Operation failed !');
			}
			return Response::json(['success' => '1']);
		}
		return Response::json(['errors' => $validator->errors()]);
	}

	public function update_user(Request $request, $id)
	{

        $user = User::find($id);

        if ($user->nik == $request->nik) {
            $nik = "required|min:5|max:150";
        } else {
            $nik = "required|min:5|max:150|unique:users";
        }
        if ($user->email == $request->email) {
            $email = "required|string|email|max:100";
        } else {
            $email = "required|string|email|max:100|unique:users";
        }
        if ($user->username == $request->username) {
            $username = "required|alpha_dash|max:100";
        } else {
            $username = "required|alpha_dash|max:100|unique:users";
        }
        if ($user->tps_id == $request->tps_id) {
            if ($request->group_id == 2) {
                $tps_id = "required";
            } else {
                $tps_id = "nullable";
            }
        } else {
            $tps_id = "required|unique:users";
        }


        $validator = $validator = Validator::make($request->all(), [

            'group_id'              => 'required',
            'tps_id'          		=> $tps_id,
            'desa_id'          		=> 'required',
            'nik'             		=> $nik,
			'name' 					=> 'required|string|max:255',
			'email' 				=> $email,
			'username' 				=> $username,
			'password' 				=> 'nullable|string|min:8',
            'avatar'         		=> 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:10240|dimensions:max_width=10000,max_height=10000',
            'activation_status'    	=> 'required',
            'gender'          		=> 'required',
        ], [
            'avatar.dimensions' => 'Max dimensions 600x600',
            'tps_id.unique'     => 'TPS Tersebut sudah digunakan oleh user lain',
        ]);


        if ($validator->passes()) {

            $user->group_id  			= $request->input('group_id');
            $user->tps_id  				= $request->input('tps_id');
            $user->desa_id  			= $request->input('desa_id');
            $user->nik  				= $request->input('nik');
            $user->name  				= $request->input('name');
            $user->email  				= $request->input('email');
            $user->username  			= $request->input('username');
            $user->activation_status  	= $request->input('activation_status');
            $user->gender  				= $request->input('gender');

            if($request->input('password')){
                $user->password             = bcrypt($request->input('password'));
            	$user->passmd5  			= md5($request->input('password'));
            }

            if ($request->hasFile('avatar')) {
                $image = $request->file('avatar');
                $file_name = $this->avatar($id, $image);

                $user->avatar  = $file_name;
            }

            $affected_row = $user->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => true,'message' => 'Data update successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }
        }   

        return Response::json(['status' => false, 'message' => $validator->errors()]);
	}

	public function show($id) {
		$user = User::where('id', $id)
			->first();
		return json_encode($user);
	}

    public function destroy($id)
    {
        $user = User::find($id);
        if (count($user)) {
            //$caleg->tags()->detach();
            if ($user->avatar) {
                @unlink(get_avatar_path($user->avatar));
            }
            $user->delete();
            return redirect()->back()->with('message', 'user delete successfully.');
        } else {
            return redirect()->back()->with('exception', 'user not found !');
        }
    }

    public function published($id) {
        $affected_row = User::where('id', $id)
            ->update(['activation_status' => 1]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Activate successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    public function unpublished($id) {
        $affected_row = User::where('id', $id)
            ->update(['activation_status' => 0]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Unactivate successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }
}
