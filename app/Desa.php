<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
	protected $table = 'desa';

    protected $fillable = [
        'kecamatan_id', 'desa_name', 'publication_status'
    ];

    public function tps() {
        return $this->hasMany(Tps::class);
    }
    
    public function user() {
        return $this->hasMany(User::class);
    }

    public function kecamatan() {
        return $this->belongsTo(Kecamatan::class);
    }
}
