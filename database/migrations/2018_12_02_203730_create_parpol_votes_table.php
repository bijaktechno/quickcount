<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParpolVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parpol_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parpol_id')->unsigned();
            $table->integer('tps_id')->unsigned();
            $table->integer('valid');
            $table->integer('invalid');

            $table->foreign('parpol_id')->references('id')->on('parpol')->onDelete('cascade');
            $table->foreign('tps_id')->references('id')->on('tps')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parpol_votes');
    }
}
