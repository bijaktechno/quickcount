<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
	use HasApiTokens, Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'group_id', 'tps_id', 'desa_id', 'nik', 'name', 'email', 'username', 'password', 'passmd5', 'avatar', 'role', 'activation_status', 'gender', 'phone', 'address', 'rt', 'rw', 'facebook', 'twitter', 'google_plus', 'linkedin', 'about',

	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

    public function group() {
        return $this->belongsTo(Group::class);
    }

    public function tps() {
        return $this->belongsTo(Tps::class);
    }

    public function uservotes() {
        return $this->hasMany(UserVotes::class);
    }

    public function desa() {
        return $this->belongsTo(Desa::class);
    }

}
