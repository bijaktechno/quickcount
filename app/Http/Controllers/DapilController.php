<?php

namespace App\Http\Controllers;

use App\Dapil;
use App\WilayahDapil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

class DapilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.dapil.index');
    }

    public function get(Request $request) {
        $dapil = Dapil::when(!empty($request->get("group_id")) , function ($query) use($request){
                    return $query->where('group_id', $request->get("group_id"));
                })
                ->get();

        return datatables()->of($dapil)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->addColumn('publication_status', function ($dapil) {
                if ($dapil->publication_status == 1) {
                    return '<a href="' . route('admin.unpublishedDapilRoute', $dapil->id) . '" class="btn btn-success btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Unpublished"><i class="icon fa fa-arrow-down"></i>Published</a>';
                }
                return '<a href="' . route('admin.publishedDapilRoute', $dapil->id) . '" class="btn btn-warning btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Published"><i class="icon fa fa-arrow-up"></i> Unpublished</a>';})
            ->addColumn('action', function ($dapil) {
                return '<button class="btn btn-info btn-xs view-button" data-id="' . $dapil->id . '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button> 
                    <button class="btn btn-primary btn-xs edit-button" data-id="' . $dapil->id . '"data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
                    <button class="btn btn-danger btn-xs delete-button" data-id="' . $dapil->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})
            ->rawColumns(['publication_status', 'action'])
            ->setRowId('id')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validator = $validator = Validator::make($request->all(), [
            'group_id'       => 'required',
            'dapil_name'       => 'required',
            'publication_status' => 'required',
        ], [
            'dapil_name.required'  => 'Nama dapil harus diisi.',
        ]);

        if ($validator->passes()) {
            $dapil = Dapil::create([
                'group_id'              => $request->input('group_id'),
                'dapil_name'            => $request->input('dapil_name'),
                'kab_id'                => implode(',', array_filter(array_unique($request->input('kabupaten_id')))),
                'kec_id'                => implode(',', array_filter(array_unique($request->input('kecamatan_id')))),
                'desa_id'               => implode(',', array_filter(array_unique($request->input('desa_id')))),
                'publication_status'    => $request->input('publication_status'),
            ]);

            $dapil_id = $dapil->id;

            if (!empty($dapil_id)) {
                return Response::json(['status' => true,'message' => 'Data add successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }

        }

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id=null)
    {
        if($id==null){
            $data['data'] = Dapil::where([['dapil_name','LIKE',"%{$request->get('q')}%"]])
                            ->when(!empty($request->get('caleg_group_id')) , function ($query) use($request){
                                return $query->where('group_id', $request->get('caleg_group_id'));
                            })
                            ->whereNotIn('publication_status', [0])
                            ->get(['id', 'dapil_name']);
        }
        else{
            $data['dapil'] = Dapil::where('id', $id)->first();
        }

        return Response::json($data);
    }

    public function area(Request $request, $id)
    {
        $data['data'] = WilayahDapil::where('dapil_id', $id)
                        ->select('kabupaten_id', 'kecamatan_id', 'desa_id')
                        ->groupBy('kabupaten_id', 'kecamatan_id', 'desa_id')
                        ->get();

        return Response::json($data);
    }

    public function area_detail(Request $request, $id)
    {
        $data['dapil'] = WilayahDapil::where('dapil_id', $id)
                        ->select('dapil_name')
                        ->first();

        $data['kabupaten'] = WilayahDapil::where('dapil_id', $id)
                        ->whereNotNull('kabupaten_name')
                        ->select('kabupaten_name')
                        ->groupBy('kabupaten_name')
                        ->get();

        $data['kecamatan'] = WilayahDapil::where('dapil_id', $id)
                        ->whereNotNull('kecamatan_name')
                        ->select('kecamatan_name')
                        ->groupBy('kecamatan_name')
                        ->get();

        $data['desa'] = WilayahDapil::where('dapil_id', $id)
                        ->whereNotNull('desa_name')
                        ->select('desa_name')
                        ->groupBy('desa_name')
                        ->get();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // dd(implode(',', array_filter(array_unique($request->get('kabupaten_id')))));

        $dapil = Dapil::find($id);

        $validator = $validator = Validator::make($request->all(), [
            'group_id'       => 'required',
            'dapil_name'       => 'required',
            'publication_status' => 'required',
        ], [
            'dapil_name.required'  => 'Nama dapil harus diisi.',
        ]);

        if ($validator->passes()) {
            $dapil->group_id = $request->get('group_id');
            $dapil->dapil_name = $request->get('dapil_name');
            $dapil->kab_id = implode(',', array_filter(array_unique($request->get('kabupaten_id'))));
            $dapil->kec_id = implode(',', array_filter(array_unique($request->get('kecamatan_id'))));
            $dapil->desa_id = implode(',', array_filter(array_unique($request->get('desa_id'))));
            $dapil->publication_status = $request->get('publication_status');
            $affected_row = $dapil->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => true,'message' => 'Data update successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }

        }

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Dapil::find($id);
        if (count($data)) {
            $data->delete();
            return redirect()->back()->with('message', 'Data delete successfully.');
        } else {
            return redirect()->back()->with('exception', 'Operation failed !');
        }
    }


    public function published($id) {
        $affected_row = Dapil::where('id', $id)
            ->update(['publication_status' => 1]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Published successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    public function unpublished($id) {
        $affected_row = Dapil::where('id', $id)
            ->update(['publication_status' => 0]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Unpublished successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }
}
