@extends('web.layouts.app')

@section('title', $setting->meta_title)
@section('keywords', $setting->meta_keywords)
@section('description', $setting->meta_description)

@section('style')
  <link rel="stylesheet" href="{{ asset('public/admin/css/jquery.inputpicker.css')}}">
@endsection

@section('content')

<!-- Header-->

    <header data-background="{{ asset('public/web/img/header/0.jpg') }}" class="intro introhalf landing">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1><span class="bold">BERSAMA GOLKAR </span></h1>
        <h4>hidup mudah, murah, berkah</h4>
      </div>
    </header><img src="{{ asset('public/web/img/slider/slider.png') }}" alt="" data-wow-duration="3s" data-wow-delay=".2s" class="image-overlay img-responsive center-block wow zoomIn">

<!-- <header data-background="{{ asset('public/web/img/header/0.jpg') }}" class="intro introhalf landing">
  <div class="intro-body data-align-center">
    <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" data-wow-duration="2s" data-wow-delay=".2s" class="caleg-main img-responsive wow slideInUp animated" >

    <h1 data-wow-duration="2s" data-wow-delay=".2s" class="wow slideInRight"><span class="bold">Nama calon</span> 70%</h1>
  </div>
</header> -->
<!-- <img src="{{ asset('public/web/img/slider/9.png') }}" alt="" data-wow-duration="3s" data-wow-delay=".2s" class="image-overlay img-responsive center-block wow zoomIn"> -->
<!-- About Section-->
<!-- <section id="chart" class="bg-white">

  <div class="container wow fadeIn">

    <div class="row">
  			<canvas id="myChart" style="max-width: 100%;"></canvas>
	  </div>
	</div>
</section> -->


<section id="chart" class="section-small action bg-white" style="border-top: none;">
  <div class="container">
    <div id="cart-area" class="row">
      
<!-- 
      <button type="button" class="btn btn-dark-border btn-xs btn-votes">All</button>
    @foreach($dapils as $key => $dapil)
      <button type="button" class="btn btn-dark-border btn-xs btn-votes" data-id="{{$dapil->id}}">{{$dapil->dapil_name}}</button>
    @endforeach -->
      <div id="filter-caleg" class="form-inline">

        <div class="col-lg-2">
          <div class="form-group">
            <input type="text" name="caleg_group_id" id="caleg_group_id" class="form-control" placeholder="Nama Group Caleg" value="2">
            <!-- <input type="text" class="form-control" aria-label="..."> -->
          </div>
        </div>
        <div class="col-lg-2">
          <div class="form-group">
            <input type="text" name="dapil_id" id="dapil_id" class="form-control" placeholder="Nama dapil">
            <!-- <input type="text" class="form-control" aria-label="..."> -->
          </div>
        </div>
        <div class="col-lg-2">
          <div id="form_kabupaten_id" class="form-group">
            <input type="text" name="kabupaten_id" id="kabupaten_id" class="form-control" placeholder="Nama kabupaten">
            <!-- <input type="text" class="form-control" aria-label="..."> -->
          </div>
        </div>
        <div class="col-lg-2">
          <div id="form_kecamatan_id" class="form-group">
            <input type="text" name="kecamatan_id" id="kecamatan_id" class="form-control" placeholder="Nama kecamatan">
            <!-- <input type="text" class="form-control" aria-label="..."> -->
          </div>
        </div>
        <div class="col-lg-2">
          <div id="form_desa_id" class="form-group">
            <input type="text" name="desa_id" id="desa_id" class="form-control" placeholder="Nama desa">
            <!-- <input type="text" class="form-control" aria-label="..."> -->
          </div>
        </div>
        <div class="col-lg-2">
          <div id="form_tps_id" class="form-group">
            <input type="text" name="tps_id" id="tps_id" class="form-control" placeholder="Nama tps">
            <!-- <input type="text" class="form-control" aria-label="..."> -->
          </div>
        </div>

        
        
        
        
        
<!-- 
        <select id="dapil_id">
            <option value="">All</option>
          @foreach($dapils as $key => $dapil)
            <option value="{{$dapil->id}}">{{$dapil->dapil_name}}</option>
          @endforeach
        </select> -->
<!-- 
        <select id="kabupaten_id">
            <option selected value="">-- Pilih Kabupaten --</option>
        </select>

        <select id="kecamatan_id">
            <option selected value="">-- Pilih Kecamatan --</option>
        </select>

        <select id="desa_id">
            <option selected value="">-- Pilih Desa --</option>
        </select>

        <select id="tps_id">
            <option selected value="">-- Pilih TPS --</option>
        </select> -->

      </div>

      <canvas id="calegChart" style="max-width: 100%;"></canvas>




    </div>
  </div>
</section>

<!-- Team Section-->
<section id="caleg" class=" text-center bg-white">
  <div class="container">
    <h3>Detail Caleg</h3>
    <div id="detail-caleg" class="row">


    <div class="col-md-12">
      <div class="panel border-yellow">
        <div class="panel-heading bg-yellow">
          <h3 class="panel-title">DPR</h3>
        </div>
        <div class="panel-body">

          @foreach($dpr as $caleg)
          <div class="col-md-3 col-sm-6">
          	<div class="row">
    	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
    	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                          <figure>
                            <div class="img-circle-custom">
                              @if(!empty($caleg->caleg_picture))
                                <img src="{{ asset('public/caleg_picture/'.$caleg->caleg_picture) }}" alt="{{ $caleg->caleg_name }}" style="width:75px;" id="user-img">
                              @else
                                <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="{{ $caleg->caleg_name }}" style="width:75px;" id="user-img">
                              @endif
                            </div>
                          </figure>
                          <h5 style="text-align:center;"><strong id="user-name">{{str_limit($caleg->caleg_name, 16)}}</strong></h5>
                          <p style="text-align:left;font-size: smaller;" id="user-role">{{ $caleg->dapil->dapil_name }}</p>
                          <!-- <p style="text-align:left;font-size: smaller;" id="user-frid">{{$caleg->caleg_nik}} </p> -->
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                          <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                          <div class="col-lg-12 left progress" style="padding: 0">
                        		<div role="progressbar" aria-valuenow="{{ $caleg->score }}" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: {{ $caleg->score_average }}%; background-color: #{{ $caleg->frame_color }}">{{ number_format($caleg->score_average) }} Suara
                        		</div>
                          </div>

                        </div>
                    </div>
    	          </div>
    	      </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="panel border-yellow">
        <div class="panel-heading bg-yellow">
          <h3 class="panel-title">DPRD 1</h3>
        </div>
        <div class="panel-body">

          @foreach($dprd1 as $caleg)
          <div class="col-md-3 col-sm-6">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
                    <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                          <figure>
                            <div class="img-circle-custom">
                              @if(!empty($caleg->caleg_picture))
                                <img src="{{ asset('public/caleg_picture/'.$caleg->caleg_picture) }}" alt="{{ $caleg->caleg_name }}" style="width:75px;" id="user-img">
                              @else
                                <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="{{ $caleg->caleg_name }}" style="width:75px;" id="user-img">
                              @endif
                            </div>
                          </figure>
                          <h5 style="text-align:center;"><strong id="user-name">{{str_limit($caleg->caleg_name, 16)}}</strong></h5>
                          <p style="text-align:left;font-size: smaller;" id="user-role">{{ $caleg->dapil->dapil_name }}</p>
                          <!-- <p style="text-align:left;font-size: smaller;" id="user-frid">{{$caleg->caleg_nik}} </p> -->
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                          <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                          <div class="col-lg-12 left progress" style="padding: 0">
                            <div role="progressbar" aria-valuenow="{{ $caleg->score }}" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: {{ $caleg->score_average }}%; background-color: #{{ $caleg->frame_color }}">{{ number_format($caleg->score_average) }} Suara
                            </div>
                          </div>

                        </div>
                    </div>
                </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="panel border-yellow">
        <div class="panel-heading bg-yellow">
          <h3 class="panel-title">DPRD 2</h3>
        </div>
        <div class="panel-body">

          @foreach($dprd2 as $caleg)
          <div class="col-md-3 col-sm-6">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
                    <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                          <figure>
                            <div class="img-circle-custom">
                              @if(!empty($caleg->caleg_picture))
                                <img src="{{ asset('public/caleg_picture/'.$caleg->caleg_picture) }}" alt="{{ $caleg->caleg_name }}" style="width:75px;" id="user-img">
                              @else
                                <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="{{ $caleg->caleg_name }}" style="width:75px;" id="user-img">
                              @endif
                            </div>
                          </figure>
                          <h5 style="text-align:center;"><strong id="user-name">{{str_limit($caleg->caleg_name, 16)}}</strong></h5>
                          <p style="text-align:left;font-size: smaller;" id="user-role">{{ $caleg->dapil->dapil_name }}</p>
                          <!-- <p style="text-align:left;font-size: smaller;" id="user-frid">{{$caleg->caleg_nik}} </p> -->
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                          <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                          <div class="col-lg-12 left progress" style="padding: 0">
                            <div role="progressbar" aria-valuenow="{{ $caleg->score }}" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: {{ $caleg->score_average }}%; background-color: #{{ $caleg->frame_color }}">{{ number_format($caleg->score_average) }} Suara
                            </div>
                          </div>

                        </div>
                    </div>
                </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>

    </div>
  </div>
</section>

<!-- Services Section-->
<!-- <section id="about">
  <div class="container text-center">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <h3>Why Choose Us?</h3>
      </div>
    </div>
    <div class="row">
      <div data-wow-delay=".2s" class="col-lg-3 col-sm-6 wow fadeIn">
        <h4><i class="icon-big ion-ios-nutrition-outline"></i> Easy to Use</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p>
      </div>
      <div data-wow-delay=".4s" class="col-lg-3 col-sm-6 wow fadeIn">
        <h4><i class="icon-big ion-ios-sunny-outline"></i> Coder friendly</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p>
      </div>
      <div data-wow-delay=".6s" class="col-lg-3 col-sm-6 wow fadeIn">
        <h4><i class="icon-big ion-ios-glasses-outline"></i> Good readability</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p>
      </div>
      <div data-wow-delay=".8s" class="col-lg-3 col-sm-6 wow fadeIn">
        <h4><i class="icon-big icon ion-ios-infinite-outline"></i> Multi-purpose</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p>
      </div>
    </div>
  </div>
</section> -->
<!-- Action video-->
<!-- <section class="section-small bg-img2 text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2"><a href="https://vimeo.com/153485166" class="swipebox-video"><i class="icon icon-big ion-ios-play-outline"></i></a>
        <h2>Watch <span class="bold">Video</span>
        </h2>
        <p>A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can.</p>
      </div>
    </div>
  </div>
</section> -->
<!-- Twitter Widget-->
<!-- <div class="section container-fluid section-small bg-gray twitter-widget text-center">
  <div class="row">
    <div class="col-md-8 col-md-offset-2"><a href=""><i class="icon icon-big ion-social-twitter-outline"></i></a>
      <div id="tweecool"></div>
    </div>
  </div>
</div> -->
<!-- Contact Section-->

        <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
<!-- <section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h2>contact us</h2>
        {!! $setting->about_us !!}
        <hr>
        <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> {{ $setting->address_line_one }}
        </h5>
        <h5><i class="fa fa-envelope fa-fw fa-lg"></i> {{ $setting->email }}
        </h5>
        <h5><i class="fa fa-phone fa-fw fa-lg"></i> {{ $setting->phone }}
        </h5>
      </div>
      <div class="col-md-5 col-md-offset-2">
        <h2>Say hello</h2>
        <form id="contactForm" name="sentMessage" novalidate="">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="name" class="sr-only control-label">You Name</label>
              <input id="name" type="text" placeholder="You Name" required="" data-validation-required-message="Please enter name" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="email" class="sr-only control-label">You Email</label>
              <input id="email" type="email" placeholder="You Email" required="" data-validation-required-message="Please enter email" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="phone" class="sr-only control-label">You Phone</label>
              <input id="phone" type="tel" placeholder="You Phone" required="" data-validation-required-message="Please enter phone number" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="message" class="sr-only control-label">Message</label>
              <textarea id="message" rows="2" placeholder="Message" required="" data-validation-required-message="Please enter a message." aria-invalid="false" class="form-control input-lg"></textarea><span class="help-block text-danger"></span>
            </div>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-yellow">Send</button>
        </form>
      </div>
    </div>
  </div>
</section> -->
<!-- Map Section-->



@endsection

@section('sidebar')
@endsection

@section('script')
  <script type="text/javascript" src="{{ asset('public/admin/js/jquery.inputpicker.js') }}"></script>

  <script type="text/javascript" src="{{ asset('public/web/js/mdb.min.js') }}"></script>

  <script>

    var dataLabels=[], dataValues=[], dataColor=[], dataTitle='';

    $(document).ready(function(){

      dataChart();

      get_dapil_picker();
      get_caleg_group_picker();


      $("#caleg_group_id").change(function(){
        $("#dapil_id").val('');
        $("#kabupaten_id").val('');
        $("#kecamatan_id").val('');
        $("#desa_id").val('');
        $("#tps_id").val('');

        get_dapil_picker();
        if ($('#kabupaten_id').hasClass("inputpicker-original")) {
          $('#kabupaten_id').inputpicker('destroy');
        }
        if ($('#kecamatan_id').hasClass("inputpicker-original")) {
          $('#kecamatan_id').inputpicker('destroy');
        }
        if ($('#desa_id').hasClass("inputpicker-original")) {
          $('#desa_id').inputpicker('destroy');
        }
        if ($('#tps_id').hasClass("inputpicker-original")) {
          $('#tps_id').inputpicker('destroy');
        }
      });

      $("#dapil_id").change(function(){
        $("#kabupaten_id").val('');
        $("#kecamatan_id").val('');
        $("#desa_id").val('');
        $("#tps_id").val('');

        get_kabupaten_picker();
        if ($('#kecamatan_id').hasClass("inputpicker-original")) {
          $('#kecamatan_id').inputpicker('destroy');
        }
        if ($('#desa_id').hasClass("inputpicker-original")) {
          $('#desa_id').inputpicker('destroy');
        }
        if ($('#tps_id').hasClass("inputpicker-original")) {
          $('#tps_id').inputpicker('destroy');
        }
      });

      $("#kabupaten_id").change(function(){
        $("#kecamatan_id").val('');
        $("#desa_id").val('');
        $("#tps_id").val('');

        get_kecamatan_picker();
        if ($('#desa_id').hasClass("inputpicker-original")) {
          $('#desa_id').inputpicker('destroy');
        }
        if ($('#tps_id').hasClass("inputpicker-original")) {
          $('#tps_id').inputpicker('destroy');
        }
      });

      $("#kecamatan_id").change(function(){
        $("#desa_id").val('');
        $("#tps_id").val('');

        get_desa_picker();
        if ($('#tps_id').hasClass("inputpicker-original")) {
          $('#tps_id').inputpicker('destroy');
        }
      });

      $("#desa_id").change(function(){
        $("#tps_id").val('');

        get_tps_picker();
      });

      $(".btn-votes, #caleg_group_id, #dapil_id, #kabupaten_id, #kecamatan_id, #desa_id, #tps_id").on('click, change', function(){
        dataLabels = [];
        dataValues = [];
        dataColor = [];
        $("#calegChart").remove();
        $("#cart-area").append('<canvas id="calegChart" style="max-width: 100%;"></canvas>');

        dataChart();

      })

    });

    function get_caleg_group_picker()
    {
      $('#caleg_group_id').inputpicker({
          url: "{{ route('allCaleggroupRoute') }}",
          // urlParam:[{"category_id":1}],
          fields:['caleg_group_name'],
          fieldText:'caleg_group_name',
          fieldValue:'id',
          filterOpen: true,
      });
    }


    function get_dapil_picker()
    {
      var caleg_group_id = $("#caleg_group_id").val();

      $('#dapil_id').inputpicker({
          url: "{{ route('allDapilRoute') }}",
          urlParam:{"caleg_group_id":caleg_group_id},
          fields:['dapil_name'],
          fieldText:'dapil_name',
          fieldValue:'id',
          filterOpen: true,
      });
    }

    function get_kabupaten_picker(){
      var dapil_id = $("#dapil_id").val();

      $('#kabupaten_id').inputpicker({
          url: "{{ route('allKabupatenRoute') }}",
          urlParam:{'dapil_id':dapil_id},
          fields:['kabupaten_name'],
          fieldText:'kabupaten_name',
          fieldValue:'id',
          filterOpen: true,
      })
    }

    function get_kecamatan_picker(){
      var kabupaten_id = $("#kabupaten_id").val();

      $('#kecamatan_id').inputpicker({
          url: "{{ route('allKecamatanRoute') }}",
          urlParam:{'kabupaten_id':kabupaten_id},
          fields:['kecamatan_name'],
          fieldText:'kecamatan_name',
          fieldValue:'id',
          filterOpen: true,
      })
    }

    function get_desa_picker(){
      var kecamatan_id = $("#kecamatan_id").val();

      $('#desa_id').inputpicker({
          url: "{{ route('allDesaRoute') }}",
          urlParam:{"kecamatan_id":kecamatan_id},
          fields:['desa_name'],
          fieldText:'desa_name',
          fieldValue:'id',
          filterOpen: true,
      })
    }

    function get_tps_picker(){
      var desa_id = $("#desa_id").val();

      $('#tps_id').inputpicker({
          url: "{{ route('allTpsRoute') }}",
          urlParam:{"desa_id":desa_id},
          fields:['tps_name'],
          fieldText:'tps_name',
          fieldValue:'id',
          filterOpen: true,
      })
    }
    
    function dataChart()
    {

        var caleg_group_id  = $('#caleg_group_id').val()?$('#caleg_group_id').val():null;
        var dapil_id      = $('#dapil_id').val()?$('#dapil_id').val():null;
        var kabupaten_id  = $('#kabupaten_id').val()?$('#kabupaten_id').val():null;
        var kecamatan_id  = $('#kecamatan_id').val()?$('#kecamatan_id').val():null;
        var desa_id       = $('#desa_id').val()?$('#desa_id').val():null;
        var tps_id        = $('#tps_id').val()?$('#tps_id').val():null;

        var url = "{{ route('allVoteRoute') }}";
        var formData = {
          'caleg_group_id'  : caleg_group_id,
          'dapil_id'      : dapil_id,
          'kabupaten_id'  : kabupaten_id,
          'kecamatan_id'  : kecamatan_id,
          'desa_id'       : desa_id,
          'tps_id'        : tps_id
        }


        $.ajax({
          url : url,
          type : 'GET',
          data: formData,
          dataType: "json",
          success:function(data){
            // console.log(data);
            $.each(data.data, function(index, value)
            {

              dataLabels.push(value.caleg_shortname);
              dataValues.push(Number(value.score_average));
              dataColor.push('#'+value.frame_color);
              dataTitle = value.dapil.dapil_name;
               /* if (value.length != 0)
                {
                    messages += value+"<br>";
                }*/


            });

            if(dapil_id == null) dataTitle = 'All Dapil';


            // $('#calegChart').data('bar').update(pourcent);
            // $("#calegChart").remove();

            getChart();
            
          }
        })
    }

    
    function get_filter_caleg()
    {

        var caleg_group_id  = $('#caleg_group_id').val()?$('#caleg_group_id').val():null;
        var dapil_id      = $('#dapil_id').val()?$('#dapil_id').val():null;

        var url = "{{ route('filterCaleg') }}";
        var formData = {
          'caleg_group_id'  : caleg_group_id,
          'dapil_id'      : dapil_id
        }


        $.ajax({
          url : url,
          type : 'GET',
          data: formData,
          dataType: "json",
          success:function(data){
            // console.log(data);
            var html = '';
            $.each(data.data, function(index, value)
            {
              // alert(value.caleg_picture);
              var caleg_picture = "{{ asset('public/caleg_picture/id_caleg_picture') }}";
              caleg_picture = caleg_picture.replace("id_caleg_picture", value.caleg_picture);

              var default_picture = "{{ asset('public/web/img/caleg/calon1.png') }}";


              var short_caleg_name = value.caleg_name;
              if(short_caleg_name.length > 10) short_caleg_name = short_caleg_name.substring(0,10);

              /*if(!empty(value.caleg_picture)){
                                    +'<img src="'+caleg_picture+'" alt="'+value.caleg_name+'" style="width:75px;" id="user-img">'+
                                  }
                                  else{
                                    +'<img src="'+default_picture+'" alt="'+value.caleg_name+'" style="width:75px;" id="user-img">'+
                                  }*/


              html += '<div class="col-md-3 col-sm-6">'+
                '<div class="row">'+
                    '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">'+
                        '<div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
                            '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">'+
                              '<figure>'+
                                '<div class="img-circle-custom">'+
                                  '<img src="'+caleg_picture+'" alt="'+value.caleg_name+'" style="width:75px;" id="user-img">'+
                                '</div>'+
                              '</figure>'+
                              '<h5 style="text-align:center;"><strong id="user-name">'+short_caleg_name+'</strong></h5>'+
                              '<p style="text-align:left;font-size: smaller;" id="user-role">'+value.dapil_name+'</p>'+
                              // '<p style="text-align:left;font-size: smaller;" id="user-frid">'+value.caleg_nik+'</p>'+
                              '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>'+
                              '<p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>'+
                              '<div class="col-lg-12 left progress" style="padding: 0">'+
                                '<div role="progressbar" aria-valuenow="'+value.score+'" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: '+value.score_average+'%; background-color: #'+value.frame_color+'">'+value.score_average+'Suara'+
                                '</div>'+
                              '</div>'+

                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
              '</div>';
            });

            $("#detail-caleg").html(html);
            // console.log(html);
            
          }
        })
    }
    function getChart()
    {
      console.log(dataLabels);
      console.log(dataValues);
      console.log(dataColor);

      var ctx = document.getElementById("calegChart").getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: dataLabels,
          datasets: [{
            label: dataTitle,
            data: dataValues,
            backgroundColor: dataColor,
            borderColor: dataColor,
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
    }





/*
          var ctx = document.getElementById("Chart1").getContext('2d');
          var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: ["test-1", "test-2", "test-3", "test-4", "test-5", "test-6","test-7","test-8","test-9"],
              datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3,3,5,9],
                backgroundColor: [
                  'rgba(215, 208, 16, 0.7)',
                  'rgba(141, 108, 19, 0.7)',
                  'rgba(255, 206, 86, 0.7)',
                  'rgba(255, 186, 0, 0.7)',
                  'rgba(246, 255, 5, 0.7)',
                  'rgba(252, 202, 0, 0.7)',
                  'rgba(189, 127, 86, 0.7)',
                  'rgba(255, 159, 64, 0.7)',
                  'rgba(189, 151, 86, 0.7)'
                ],
                borderColor: [
                  'rgba(215, 208, 16,1)',
                  'rgba(141, 108, 19, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(255, 186, 0, 1)',
                  'rgba(246, 255, 5, 1)',
                  'rgba(252, 202, 0,1)',
                  'rgba(189, 127, 86, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(189, 151, 86, 1)'
                ],
                borderWidth: 1
              }]
            },
            options: {
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true
                  }
                }]
              }
            }
          });*/

  </script>
@endsection