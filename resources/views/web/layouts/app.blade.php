<!DOCTYPE html>
<html lang="en">
<!-- head -->
<head>
    @include('web.includes.head')
    @yield('style')
</head>
<!-- /.head -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" class="top">

    <!-- Preloader-->
    <div id="preloader">
      <div id="status"></div>
    </div>

    <!-- Navigation One Page-->
    @include('web.includes.header')


    <!-- left section -->
    @yield('content')
    <!-- /. left section -->

    <!-- /. right side widget -->
    @yield('sidebar')
    <!--/. right side widget-->

    
    <!-- footer -->
    @include('web.includes.footer')
    <!-- /. footer -->



    <!-- scripts -->
    @include('web.includes.scripts')
    @yield('script')
    <!-- /. script -->
</body>
</html>