<?php

if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => '/'], function () {
	Route::get('/', ['as' => 'homePage', 'uses' => 'WebController@index']);
	Route::get('/filter-caleg', ['as' => 'filterCaleg', 'uses' => 'WebController@get']);
	Route::get('/mobile', ['as' => 'mobilePage', 'uses' => 'WebController@mobile']);
	Route::get('/get-all-vote', ['as' => 'allVoteRoute', 'uses' => 'VoteController@show']);
	Route::get('/get-all-parpol', ['as' => 'allParpolRoute', 'uses' => 'ParpolController@show']);
	Route::get('/get-all-kabupaten', ['as' => 'allKabupatenRoute', 'uses' => 'KabupatenController@show']);
	Route::get('/get-all-kecamatan', ['as' => 'allKecamatanRoute', 'uses' => 'KecamatanController@show']);
	Route::get('/get-all-desa', ['as' => 'allDesaRoute', 'uses' => 'DesaController@show']);
	Route::get('/get-all-dapil', ['as' => 'allDapilRoute', 'uses' => 'DapilController@show']);
	Route::get('/get-all-wilayahdapil/{id}', ['as' => 'allWilayahdapilRoute', 'uses' => 'DapilController@area']);
	Route::get('/get-all-wilayahdetail/{id}', ['as' => 'wilayahDetailRoute', 'uses' => 'DapilController@area_detail']);
	Route::get('/get-all-tps', ['as' => 'allTpsRoute', 'uses' => 'TpsController@show']);
	Route::get('/get-all-usergroup', ['as' => 'allUsergroupRoute', 'uses' => 'UsergroupController@show']);
	Route::get('/get-all-caleggroup', ['as' => 'allCaleggroupRoute', 'uses' => 'CaleggroupController@show']);
	Route::get('/get-all-caleg', ['as' => 'allCalegRoute', 'uses' => 'CalegController@show']);
	
});

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
 */

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin'], 'as' => 'admin.'], function () {
	Route::get('/dashboard', ['as' => 'dashboardRoute', 'uses' => 'DashboardController@index']);


	/*** For Master ***/
	/*** For Kabupaten ***/
	Route::resource('kabupaten', 'KabupatenController');
	Route::get('/get-kabupaten', ['as' => 'getKabupatenRoute', 'uses' => 'KabupatenController@get']);
	Route::get('/kabupaten/published/{id}', ['as' => 'publishedKabupatenRoute', 'uses' => 'KabupatenController@published']);
	Route::get('/kabupaten/unpublished/{id}', ['as' => 'unpublishedKabupatenRoute', 'uses' => 'KabupatenController@unpublished']);
	/*** For Kecamatan ***/
	Route::resource('kecamatan', 'KecamatanController');
	Route::get('/get-kecamatan', ['as' => 'getKecamatanRoute', 'uses' => 'KecamatanController@get']);
	Route::get('/kecamatan/published/{id}', ['as' => 'publishedKecamatanRoute', 'uses' => 'KecamatanController@published']);
	Route::get('/kecamatan/unpublished/{id}', ['as' => 'unpublishedKecamatanRoute', 'uses' => 'KecamatanController@unpublished']);
	/*** For Desa ***/
	Route::resource('desa', 'DesaController');
	Route::get('/get-desa', ['as' => 'getDesaRoute', 'uses' => 'DesaController@get']);
	Route::get('/desa/published/{id}', ['as' => 'publishedDesaRoute', 'uses' => 'DesaController@published']);
	Route::get('/desa/unpublished/{id}', ['as' => 'unpublishedDesaRoute', 'uses' => 'DesaController@unpublished']);
	/*** For Dapil ***/
	Route::resource('dapil', 'DapilController');
	Route::get('/get-dapil', ['as' => 'getDapilRoute', 'uses' => 'DapilController@get']);
	Route::get('/dapil/published/{id}', ['as' => 'publishedDapilRoute', 'uses' => 'DapilController@published']);
	Route::get('/dapil/unpublished/{id}', ['as' => 'unpublishedDapilRoute', 'uses' => 'DapilController@unpublished']);
	/*** For Tps ***/
	Route::resource('tps', 'TpsController');
	Route::get('/get-tps', ['as' => 'getTpsRoute', 'uses' => 'TpsController@get']);
	Route::get('/tps/published/{id}', ['as' => 'publishedTpsRoute', 'uses' => 'TpsController@published']);
	Route::get('/tps/unpublished/{id}', ['as' => 'unpublishedTpsRoute', 'uses' => 'TpsController@unpublished']);


	/*** For Caleg ***/
	Route::resource('caleg', 'CalegController');
	Route::get('/get-caleg', ['as' => 'getCalegRoute', 'uses' => 'CalegController@get']);
	Route::post('/update-caleg/{id}', ['as' => 'updateCalegRoute', 'uses' => 'CalegController@update']);
	Route::get('/caleg/published/{id}', ['as' => 'publishedCalegRoute', 'uses' => 'CalegController@published']);
	Route::get('/caleg/unpublished/{id}', ['as' => 'unpublishedCalegRoute', 'uses' => 'CalegController@unpublished']);

	/*** For Vote ***/
	Route::resource('vote', 'VoteController');
	Route::get('/get-vote', ['as' => 'getVoteRoute', 'uses' => 'VoteController@get']);
	Route::get('/vote/published/{id}', ['as' => 'publishedVoteRoute', 'uses' => 'VoteController@published']);
	Route::get('/vote/unpublished/{id}', ['as' => 'unpublishedVoteRoute', 'uses' => 'VoteController@unpublished']);

	/*** For Parpol ***/
	Route::resource('parpol', 'ParpolController');
	Route::get('/get-parpol', ['as' => 'getParpolRoute', 'uses' => 'ParpolController@get']);
	Route::get('/parpol/published/{id}', ['as' => 'publishedParpolRoute', 'uses' => 'ParpolController@published']);
	Route::get('/parpol/unpublished/{id}', ['as' => 'unpublishedParpolRoute', 'uses' => 'ParpolController@unpublished']);

	/*** For Vote ***/
	Route::resource('usergroup', 'UsergroupController');
	Route::get('/get-usergroup', ['as' => 'getUsergroupRoute', 'uses' => 'UsergroupController@get']);
	Route::get('/usergroup/published/{id}', ['as' => 'publishedUsergroupRoute', 'uses' => 'UsergroupController@published']);
	Route::get('/usergroup/unpublished/{id}', ['as' => 'unpublishedUsergroupRoute', 'uses' => 'UsergroupController@unpublished']);
	/*** For Vote ***/
	Route::resource('users', 'UserController');
	Route::get('/get-users', ['as' => 'getUserRoute', 'uses' => 'UserController@get']);
	Route::post('/update-users/{id}', ['as' => 'updateUserRoute', 'uses' => 'UserController@update_user']);
	Route::get('/users/published/{id}', ['as' => 'publishedUserRoute', 'uses' => 'UserController@published']);
	Route::get('/users/unpublished/{id}', ['as' => 'unpublishedUserRoute', 'uses' => 'UserController@unpublished']);

	/*** For Setting ***/
	Route::resource('setting', 'SettingController');
	Route::post('/setting/logo/{id}', ['as' => 'settingLogoRoute', 'uses' => 'SettingController@logo']);
	Route::post('/setting/favicon/{id}', ['as' => 'settingFaviconRoute', 'uses' => 'SettingController@favicon']);
	Route::post('/setting/general/{id}', ['as' => 'settingGeneralRoute', 'uses' => 'SettingController@general']);
	Route::post('/setting/contact/{id}', ['as' => 'settingContactRoute', 'uses' => 'SettingController@contact']);
	Route::post('/setting/address/{id}', ['as' => 'settingAddressRoute', 'uses' => 'SettingController@address']);
	Route::post('/setting/social/{id}', ['as' => 'settingSocialRoute', 'uses' => 'SettingController@social']);
	Route::post('/setting/meta/{id}', ['as' => 'settingMetaRoute', 'uses' => 'SettingController@meta']);
	Route::post('/setting/gallery-meta/{id}', ['as' => 'settingGalleryMetaRoute', 'uses' => 'SettingController@gallery_meta']);

	/*** For Profile ***/
	Route::resource('profile', 'ProfileController');
	Route::post('/profile/avatar/{id}', ['as' => 'profileAvatarRoute', 'uses' => 'ProfileController@avatar']);
	Route::post('/profile/update-password', ['as' => 'profileUpdatePasswordRoute', 'uses' => 'ProfileController@update_password']);


	/*** For Caleg Vote ***/
	Route::resource('caleg-vote', 'CalegvoteController');
	Route::get('/get-caleg-vote', ['as' => 'getCalegvoteRoute', 'uses' => 'CalegvoteController@get']);

	/*** For Vote Compare ***/
	Route::resource('vote-compare', 'VotecompareController');
	Route::get('/get-vote-compare', ['as' => 'getVotecompareRoute', 'uses' => 'VotecompareController@get']);


});

Auth::routes();