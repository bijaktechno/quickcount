<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
	// Route::post('details/{id}', 'API\UserController@show');
	Route::get('wilayah', 'API\WilayahController@wilayah');

	Route::get('user-details/{id}', 'API\UserController@show');
	Route::get('user-list', 'API\UserController@show');

	Route::get('caleg-details/{id}', 'API\CalegController@show');
	Route::get('caleg-list', 'API\CalegController@show');

	Route::post('store-user-votes', 'API\VoteController@store');

});