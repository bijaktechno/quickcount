<section class="footer bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h3>About</h3>
        <p>Thanks for choosing Pheromone for your next project! Pheromone is a unique template for building  beautiful business or personal website.</p>
      </div>
      <div class="col-md-4">
        <h3>Why us</h3>
        <p>Easy to use and coder frendly. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem dictum.</p>
      </div>
      <div class="col-md-3">
        <h3>Contact</h3>
        <p><i class="fa fa-phone fa-fw"></i> (123) 456-7890 <br> <i class="fa fa-envelope fa-fw"></i> info@youwebsite.com <br> <i class="fa fa-map-marker fa-fw"></i> 2345 Some Avenue, New York
        </p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
        <ul class="list-inline">
          <li><a href="/"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
          <li><a href="/"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
          <li><a href="/"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
          <li><a href="/"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
        </ul>
      </div>
      <div class="col-md-8">
        <p class="small">{!! $setting->copyright !!}</p>
      </div>
    </div>
  </div>
</section>