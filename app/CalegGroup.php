<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalegGroup extends Model
{
	// protected $table = 'caleg_groups';

    protected $fillable = [
        'caleg_group_name', 'publication_status'
    ];

    public function caleg() {
        return $this->hasMany(Caleg::class);
    }

    public function dapil() {
        return $this->hasMany(Dapil::class, 'group_id');
    }
}
