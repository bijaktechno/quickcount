@extends('admin.layouts.app')
@section('title', 'Caleg')


@section('style')

<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/dataTables.bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/buttons.dataTables.min.css')}}">

<style>
.modal-title {
	font-weight: bold;
}
.social {
	padding: 5px;
	font-size: 12px;
	width: 24px;
	text-align: center;
	text-decoration: none;
	margin: 0px 2px;
}

.social:hover {
	opacity: 0.7;
}

.facebook {
	background: #3B5998;
	color: white;
}

.twitter {
	background: #55ACEE;
	color: white;
}

.google {
	background: #dd4b39;
	color: white;
}

.linkedin {
	background: #007bb5;
	color: white;
}
.edit {
	background: #007bb5;
	color: white;
}
.view {
	background: #55ACEE;
	color: white;
}
.delete {
	background: #dd4b39;
	color: white;
}
.bg-custom-purple{
	background-color: #D1C4E9;
}
</style>

@endsection

@section('content')
<!-- Page header -->
<section class="content-header">
	<h1>
		Caleg
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> Dashboard</a></li>
		<li class="active">Caleg</li>
	</ol>
</section>
<!-- /.page header -->


<!-- Main content -->
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<!-- <h3 class="box-title">Manage Caleg</h3> -->

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#thumbnail" aria-controls="thumbnail" role="tab" data-toggle="tab">Thumbnail</a></li>
                <li role="presentation"><a href="#table" aria-controls="table" role="tab" data-toggle="tab">Table</a></li>
            </ul>

			<div class="box-tools">
				<a href="{{ route('admin.caleg.create') }}" class="btn btn-info btn-sm btn-flat"><i class="fa fa-plus"></i> Add Caleg</a>
			</div>

		</div>
		<!-- /.box-header -->
		<div id="content-area" class="box-body tab-content">
            <div role="tabpanel" class="tab-pane active" id="thumbnail">
				<div style="width: 100%; padding-left: -10px;">

					<!-- <div class="row"> -->
					@foreach($calegs as $caleg)
					<div class="col-md-4">
						<!-- Widget: caleg widget style 1 -->
						<div class="box box-widget widget-user">
							<!-- Add the bg color to the header using any of the bg-* classes -->
							<!-- <div class="widget-caleg-header bg-aqua-active"> -->
							<div class="widget-user-header bg-custom-purple" style="background-color: #{{ $caleg->frame_color }}">
								<h3 class="widget-user-username">{{ $caleg->caleg_name }}</h3>
								<!-- <h5 class="widget-user-desc">NIK : {{ $caleg->caleg_nik }}</h5> -->
								<h5 class="widget-user-desc">
									@if($caleg->caleg_gender == 'L')
									<span>Laki-Laki</span>
									@else
									<span>Perempuan</span>
									@endif
								</h5>
							</div>
							<div class="widget-user-image">
								@if(!empty($caleg->caleg_picture))
								<img src="{{ asset('public/caleg_picture/' . $caleg->caleg_picture) }}" alt="{{ $caleg->caleg_name }}">
								@else
								<img src="{{ asset('public/avatar/user.png') }}" alt="{{ $caleg->caleg_name }}">
								@endif
							</div>
							<div class="box-footer">
								<div class="row">
									<div class="col-sm-12">
										<div class="description-block">
											@if($caleg->publication_status == 1)
											<h5 class="text-success"><i class="fa fa-unlock"></i> Active</h5>
											@else
											<h5 class="text-danger"><i class="fa fa-lock"></i> Block</h5>
											@endif
											<div class="pull-left">
												{{ $caleg->score_average ? $caleg->score_average:0 }} suara
											</div>
											<div class="pull-right">
												<button class="btn btn-default btn-xs btn-flat view-button" data-id="{{ $caleg->id }}" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button>
												<a class="btn btn-default btn-xs btn-flat edit-button" href="{{ route('admin.caleg.edit', $caleg->id) }}" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
												<button class="btn btn-default btn-xs btn-flat delete-button" data-id="{{ $caleg->id }}" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>
											</div>
										</div>
										<!-- /.description-block -->
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.widget-caleg -->
					</div>
					<!-- /.col -->
					@endforeach
					<div class="col-md-12 text-center">
						{{ $calegs->links() }}
					</div>
					<!-- /.col -->
					<!-- </div> -->
					<!-- /.row -->
				</div>
			</div>

            <div role="tabpanel" class="tab-pane" id="table">
            	
				<div class="table-responsive">
					<table id="caleg-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0">
						<thead>
							<tr>
								<th>Foto Caleg</th>
								<th>Nama Caleg</th>
								<!-- <th>NIK</th> -->
								<th>Nama Group Dapil</th>
								<th>Nama Dapil</th>
								<!-- <th>Created At</th> -->
								<!-- <th>Updated At</th> -->
								<!-- <th width="10%">Publication Status</th> -->
								<th width="7%">Action</th>
							</tr>
						</thead>
					</table>
				</div>

        	</div>

		</div>
	</div>
</section>


<!-- Main content -->

	<!-- view post modal -->
	<div id="view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="btn-group pull-right no-print">
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
								<i class="fa fa-print"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-remove"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
					</div>
					<h4 class="modal-title" id="view-name"></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-9">
							<table class="table table-bordered table-striped">
								<tbody>
									<tr>
										<td>Username</td>
										<td id="view-username"></td>
									</tr>
									<tr>
										<td>NIK</td>
										<td id="view-nik"></td>
									</tr>
									<tr>
										<td>Gender</td>
										<td id="view-gender"></td>
									</tr>
									<tr>
										<td>Status</td>
										<td id="view-status"></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-3">
							<img id="view-avatar" class="img-responsive img-thumbnail img-rounded">
						</div>
					</div>
				</div>
				<div class="modal-footer no-print">
					<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /.view post modal -->

	<!-- delete post modal -->
	<div id="delete-modal" class="modal modal-danger fade" id="modal-danger">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">
						<span class="fa-stack fa-sm">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-trash fa-stack-1x"></i>
						</span>
						Are you sure want to delete this ?
					</h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
					<form method="post" role="form" id="delete_form">
						{{csrf_field()}}
						{{method_field('DELETE')}}
						<button type="submit" class="btn btn-outline">Delete</button>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.delete post modal -->

	<!-- /.edit category modal -->
	@endsection

	@section('script')
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.colVis.min.js') }}"></script>

	<script type="text/javascript">
	    $(document).ready(function(){

		  get_table_data();

		});
		/** View **/
		$("#content-area").on('click','.view-button', function(){
			var id = $(this).data("id");
			var url = "{{ route('admin.caleg.show', 'id') }}";
			url = url.replace("id", id);
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					var src = '{{ asset('public/caleg_picture') }}/';
					var default_avatar = '{{ asset('public/avatar/user.png') }}';
					$('#view-modal').modal('show');

					$('#view-name').text(data.caleg['caleg_name']);
					$('#view-username').text(data.caleg['caleg_name']);
					$('#view-nik').text(data.caleg['caleg_nik']);
					$("#view-avatar").attr("src", src+data.caleg['caleg_picture']);
					if(data.caleg['caleg_picture']){
						$("#view-avatar").attr("src", src+data.caleg['caleg_picture']);
					}else{
						$("#view-avatar").attr("src", default_avatar);
					}
					if(data.caleg['caleg_gender'] == 'P'){
						$('#view-gender').text('Perempuan');
					}else{
						$('#view-gender').text('Laki-Laki');
					}
					if(data.caleg['publication_status'] == 1){
						$('#view-status').text('Active');
					}else{
						$('#view-status').text('Block');
					}
				}});
		});

		/** Delete **/
		$("#content-area").on('click','.delete-button', function(){
			var id = $(this).data("id");
			var url = "{{ route('admin.caleg.destroy', 'id') }}";
			url = url.replace("id", id);
			$('#delete-modal').modal('show');
			$('#delete_form').attr('action', url);
		});


		/** Get datatable **/
		function get_table_data(){
			$('#caleg-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: "{{ route('admin.getCalegRoute') }}",
				columns: [

				{data: function (data, type, row) {
			                if(data.caleg_picture){
			            	var image_url = "{{asset('public/caleg_picture')}}"+"/"+data.caleg_picture;
			            	// image_url = image_url.replace("caleg_picture_data", data.caleg_picture);;

			                var image = '<img src="'+image_url+'" alt="'+data.caleg_name+'" width="60" class="img img-thumbnail img-responsive">';
			                }
			                else{
			            	var image_url = "{{asset('public/avatar/user.png')}}";

			                var image = '<img src="'+image_url+'" alt="'+data.caleg_name+'" width="60" class="img img-thumbnail img-responsive">';
			                }

		                    return image;
                }, orderable: false, searchable: false},
				// {data: 'caleg_name', name: 'caleg_picture', orderable: false, searchable: false},
				{data: 'caleg_name', name: 'caleg.caleg_name'},
				// {data: 'caleg_nik'},
				{data: 'caleg_group_name', name: 'caleg_groups.caleg_group_name'},
				{data: 'dapil_name', name: 'dapil.dapil_name'},
				// {data: 'created_at'},
				// {data: 'updated_at'},
				// {data: 'publication_status', name: 'publication_status', orderable: false, searchable: false},
				// {data: 'action', name: 'action', orderable: false, searchable: false},
				{data: function (data, type, row) {
							var url_edit = "{{ route('admin.caleg.edit', 'caleg_id') }}";
							url_edit = url_edit.replace("caleg_id", data.id);

							var action = '<button style="margin-right:3px;" class="btn btn-info btn-xs view-button" data-id="' + data.id + '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button>'+ 
                				'<a style="margin-right:3px;" class="btn btn-primary btn-xs edit-button" href="'+url_edit+'" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>'+
                    			'<button style="margin-right:3px;" class="btn btn-danger btn-xs delete-button" data-id="' + data.id + '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';
		                    return action;
                }, orderable: false, searchable: false},
				],
				order: [[2, 'desc']],
			});
		}

	</script>
	@endsection