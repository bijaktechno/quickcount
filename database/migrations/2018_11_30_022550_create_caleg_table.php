<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalegTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caleg', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dapil_id')->unsigned()->index();
            $table->integer('kabupaten_id')->unsigned();
            $table->integer('caleg_group_id')->unsigned()->index();
            $table->string('caleg_name');
            $table->string('caleg_picture')->nullable();
            $table->string('caleg_gender')->nullable();
            $table->string('frame_color');
            $table->string('caleg_nik')->nullable();;
            $table->integer('no_urut');

            $table->tinyInteger('publication_status')->default(0);
            $table->timestamps();
            $table->foreign('dapil_id')->references('id')->on('dapil')->onDelete('cascade');
            $table->foreign('kabupaten_id')->references('id')->on('kabupaten')->onDelete('cascade');
            $table->foreign('caleg_group_id')->references('id')->on('caleg_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caleg');
    }
}
