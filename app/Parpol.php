<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parpol extends Model
{
	protected $table = 'parpol';

    protected $fillable = [
        'no_urut', 'parpol_name', 'parpol_alias', 'parpol_picture', 'publication_status'
    ];

    public function parpolvotes() {
        return $this->hasMany(ParpolVotes::class);
    }
}
