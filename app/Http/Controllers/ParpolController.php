<?php

namespace App\Http\Controllers;

use App\Caleg;
use App\UserVotes;
use App\DapilCalegtps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class ParpolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.parpol.index');
    }

    public function get(Request $request) {
        $to = $request->get('order')[0]['column'];
        $field = $request->get('columns')[$to]['name'];

        $count = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                ->leftJoin('users', 'users.id', '=', 'user_votes.user_id')
                ->leftJoin('tps', 'tps.id', '=', 'users.tps_id')
                ->leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                ->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
                // ->where('dapil.group_id', 1)
                ->when(!empty($request->get('filter_parpol')) , function ($query) use($request){
                    switch ($request->get('filter_parpol')) {
                        case 'kabupaten':
                            return $query->select(array('kabupaten.kabupaten_name as data_name', DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('data_name');
                            break;
                        case 'kecamatan':
                            return $query->select(array('kecamatan.kecamatan_name as data_name', DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('data_name');
                            break;
                        case 'desa':
                            return $query->select(array('desa.desa_name as data_name', DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('data_name');
                            break;
                        case 'tps':
                            return $query->select(array('desa.desa_name', DB::raw("CONCAT(desa.desa_name,'-',tps.tps_name) as data_name"), DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('desa.desa_name', 'data_name');
                            break;
                        
                        default:
                            return $query->select(array('dapil.dapil_name as data_name', DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('data_name');
                    }
                })
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kabupaten.kabupaten_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('tps.tps_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->get()
                ->count();

        $parpol = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                ->leftJoin('users', 'users.id', '=', 'user_votes.user_id')
                ->leftJoin('tps', 'tps.id', '=', 'users.tps_id')
                ->leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                ->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
                // ->where('dapil.group_id', 1)
                ->when(!empty($request->get('filter_parpol')) , function ($query) use($request){
                    switch ($request->get('filter_parpol')) {
                        case 'kabupaten':
                            return $query->select(array('kabupaten.kabupaten_name as data_name', DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('data_name');
                            break;
                        case 'kecamatan':
                            return $query->select(array('kecamatan.kecamatan_name as data_name', DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('data_name');
                            break;
                        case 'desa':
                            return $query->select(array('desa.desa_name as data_name', DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('data_name');
                            break;
                        case 'tps':
                            return $query->select(array('desa.desa_name', DB::raw("CONCAT(desa.desa_name,'-',tps.tps_name) as data_name"), DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('desa.desa_name', 'data_name');
                            break;
                        
                        default:
                            return $query->select(array('dapil.dapil_name as data_name', DB::raw('SUM(score) as score_average')
                                ))
                                ->groupBy('data_name');
                    }
                })
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kabupaten.kabupaten_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('tps.tps_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->orderBy($field, $request->get('order')[0]['dir'])
                ->offset($request->get('start'))
                ->limit($request->get('length'))
                ->get();

        $data = array(
            'recordsTotal' => $count, 
            'recordsFiltered' => $count, 
            'data' => $parpol
        );

        // return Response::json($tps);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id=null)
    {

        if($id==null){
            $data['data'] = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                        ->leftJoin('users', 'users.id', '=', 'user_votes.user_id')
                        ->leftJoin('tps', 'tps.id', '=', 'users.tps_id')
                        ->leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                        ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                        ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                        ->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
                        // ->where('dapil.group_id', 1)
                        ->when(!empty($request->get('filter_parpol')) , function ($query) use($request){
                            switch ($request->get('filter_parpol')) {
                                case 'kabupaten':
                                    return $query->select(array('kabupaten.kabupaten_name as data_name', DB::raw('SUM(score) as score_average')
                                        ))
                                        ->groupBy('data_name')
                                        ->orderBy('score_average', 'desc');

                                    break;
                                case 'kecamatan':
                                    return $query->select(array('kecamatan.kecamatan_name as data_name', DB::raw('SUM(score) as score_average')
                                        ))
                                        ->groupBy('data_name')
                                        ->orderBy('score_average', 'desc');

                                    break;
                                case 'desa':
                                    return $query->select(array('desa.desa_name as data_name', DB::raw('SUM(score) as score_average')
                                        ))
                                        ->groupBy('data_name')
                                        ->orderBy('score_average', 'desc');

                                    break;
                                case 'tps':
                                    return $query->select(array('desa.desa_name', DB::raw("CONCAT(desa.desa_name,'-',tps.tps_name) as data_name"), DB::raw('SUM(score) as score_average')
                                        ))
                                        ->groupBy('desa.desa_name', 'data_name')
                                        ->orderBy('score_average', 'desc');

                                    break;
                                
                                default:
                                    return $query->select(array('dapil.dapil_name as data_name', DB::raw('SUM(score) as score_average')
                                        ))
                                        ->groupBy('data_name')
                                        ->orderBy('score_average', 'desc');
                                    break;
                            }
                        })
                        ->get();
        }
        else{
            // $data['kabupaten'] = UserVotes::where('id', $id)->first();
        }

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
