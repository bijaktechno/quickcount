<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tps extends Model
{
	protected $table = 'tps';

    protected $fillable = [
        'desa_id', 'tps_name', 'jumlah_dpt', 'tps_coordinate', 'publication_status'
    ];

    public function desa() {
        return $this->belongsTo(Desa::class);
    }

    public function user() {
        return $this->hasMany(User::class);
    }
}
