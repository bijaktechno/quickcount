<meta charset="utf-8">
<title>@yield('title') - {{ $setting->website_title }}</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon"  href="{{ asset('public/web/favicon/favicon.png') }}" />
<meta name="description" content="">
<meta name="author" content="">
<!-- Bootstrap Core CSS-->
<link rel="stylesheet" href="{{ asset('public/web/css/bootstrap.min.css') }}">
<!-- Custom CSS-->
<link rel="stylesheet" href="{{ asset('public/web/css/pheromone.css') }}">

<!-- <link href="{{ asset('public/web/css/mdb.min.css') }}" rel="stylesheet"> -->

<link rel="stylesheet" href="{{ asset('public/web/css/style.css') }}">
