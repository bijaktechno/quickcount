<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$this->call(GroupsTableSeeder::class);
		$this->call(DapilSeeder::class);
		$this->call(KabupatenSeeder::class);
		$this->call(KecamatanSeeder::class);
		$this->call(DesaSeeder::class);
		$this->call(TpsSeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(SettingsTableSeeder::class);
		$this->call(ParpolSeeder::class);
		$this->call(CalegGroupsSeeder::class);
		$this->call(CalegSeeder::class);
	}
}
