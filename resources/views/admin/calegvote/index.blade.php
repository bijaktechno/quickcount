@extends('admin.layouts.app')
@section('title', 'Rekap Suara Percaleg')


@section('style')
<!-- <link rel="stylesheet" href="htuser://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
<link rel="stylesheet" href="htuser://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css"> -->

<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/dataTables.bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/buttons.dataTables.min.css')}}">
<link rel="stylesheet" href="{{ asset('public/admin/css/jquery.inputpicker.css')}}">
@endsection

@section('content')
<!-- Page header -->
<section class="content-header">
	<h1>
		Rekap Suara Percaleg
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> Dashboard</a></li>
		<li class="active">Rekap Suara Percaleg</li>
	</ol>
</section>
<!-- /.page header -->

<!-- Main content -->
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			
				<div id="filter-caleg" class="form-inline row" style="width: 100%;">
		        <div class="col-lg-2">
		          <div class="form-group">
		            <input type="text" name="caleg_group_id" id="caleg_group_id" class="form-control" placeholder="Nama Group Caleg" value="2">
		          </div>
		        </div>
		        <div class="col-lg-2">
		          <div class="form-group">
		            <input type="text" name="dapil_id" id="dapil_id" class="form-control" placeholder="Nama dapil">
		          </div>
		        </div>
		        <div class="col-lg-2">
		          <div id="form_caleg_id" class="form-group">
		            <input type="text" name="caleg_id" id="caleg_id" class="form-control" placeholder="Nama caleg">
		          </div>
		        </div>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div style="width: 100%; padding-left: -10px;">
				<div class="table-responsive">
					<table id="calegvote-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0">
						<thead>
							<tr>
								<th>Wilayah</th>
								<th>Total Suara</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
		<div class="box-footer clearfix">
		</div>
	</div>
</section>
<!-- /.main content -->

<!-- view User modal -->
<div id="user-view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="btn-group pull-right no-print">
					<div class="btn-group">
						<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
							<i class="fa fa-print"></i>
							<span class="hidden-sm hidden-xs"></span>
						</button>
					</div>
					<div class="btn-group">
						<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
							<i class="fa fa-remove"></i>
							<span class="hidden-sm hidden-xs"></span>
						</button>
					</div>
				</div>
				<h4 class="modal-title" id="view-name"></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-9">
						<table class="table table-bordered table-striped">
							<tbody>
								<tr>
									<td width="20%">Role</td>
									<td id="view-role"></td>
								</tr>
								<tr>
									<td>Username</td>
									<td id="view-username"></td>
								</tr>
								<tr>
									<td>Email</td>
									<td id="view-email"></td>
								</tr>
								<tr>
									<td>Gender</td>
									<td id="view-gender"></td>
								</tr>
								<tr>
									<td>Phone</td>
									<td id="view-phone"></td>
								</tr>
								<tr>
									<td>Address</td>
									<td id="view-address"></td>
								</tr>
								<tr>
									<td>Facebook</td>
									<td id="view-facebook"></td>
								</tr>
								<tr>
									<td>Twitter</td>
									<td id="view-twitter"></td>
								</tr>
								<tr>
									<td>Google Plus</td>
									<td id="view-google-plus"></td>
								</tr>
								<tr>
									<td>Linkedin</td>
									<td id="view-linkedin"></td>
								</tr>
								<tr>
									<td>Status</td>
									<td id="view-status"></td>
								</tr>
								<tr>
									<td>About</td>
									<td id="view-about"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-3">
						<img id="view-avatar" class="img-responsive img-thumbnail img-rounded">
					</div>
				</div>
			</div>
			<div class="modal-footer no-print">
				<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- /.view User modal -->
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('public/admin/datatable/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/datatable/js/datatables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/datatable/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/datatable/js/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/datatable/js/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.colVis.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/jquery.inputpicker.js') }}"></script>

<script type="text/javascript">
	/** Load datatable **/
	$(document).ready(function() {


		get_table_data();

		var filter = '<label><select class="form-control" aria-controls="calegvote-table" name="filter_voteby" id="filter_voteby" style="width:150px; margin-left:5px;">'+
			'<option selected value="kabupaten">Kabupaten</option>'+
			'<option value="kecamatan">Kecamatan</option>'+
			'<option value="desa">Desa</option>'+
			'<option value="tps">TPS</option>'+
			'</select></label>';
		
		$("#calegvote-table_length").append(filter);

		$("#calegvote-table").hide();



		var caleg_group_id = $("#caleg_group_id").val();
		var dapil_id = $("#dapil_id").val();

  		get_caleg_group_picker();


		if(caleg_group_id){
  			get_dapil_picker();
		}
		if(dapil_id){
			get_caleg_picker()
		}


		$("#caleg_group_id").change(function(){

			$("#dapil_id").val('');
			$("#caleg_id").val('');

			get_dapil_picker();
			if ($('#caleg_id').hasClass("inputpicker-original")) {
			  $('#caleg_id').inputpicker('destroy');
			}
		});

		$("#dapil_id").change(function(){

			$("#caleg_id").val('');

			get_caleg_picker();
		});

		$("#caleg_id").change(function(){
	     	$('#calegvote-table').DataTable().ajax.reload();
			$("#calegvote-table").show();

		});


		$(".table-responsive").on('change', '#filter_voteby', function(){
	     $('#calegvote-table').DataTable().ajax.reload();

		});

	});


	function get_caleg_group_picker()
	{
		$('#caleg_group_id').inputpicker({
		  url: "{{ route('allCaleggroupRoute') }}",
		  // urlParam:[{"category_id":1}],
		  fields:['caleg_group_name'],
		  fieldText:'caleg_group_name',
		  fieldValue:'id',
		  filterOpen: true,
		});
	}

	function get_dapil_picker()
	{
		var caleg_group_id = $("#caleg_group_id").val();

	 	$('#dapil_id').inputpicker({
			url: "{{ route('allDapilRoute') }}",
			urlParam:{"caleg_group_id":caleg_group_id},
			fields:['dapil_name'],
			fieldText:'dapil_name',
			fieldValue:'id',
			filterOpen: true,
	 	});
	}

	function get_caleg_picker(){
		var dapil_id = $("#dapil_id").val();

	 	$('#caleg_id').inputpicker({
			url: "{{ route('allCalegRoute') }}",
			urlParam:{'dapil_id':dapil_id},
			fields:['no_urut', 'caleg_name'],
			fieldText:'caleg_name',
			fieldValue:'id',
    		headShow: true,
			filterOpen: true,
	 	})
	}

	/** Get datatable **/
	function get_table_data(){

		var table = $('#calegvote-table').DataTable({
			dom: 'Blfrtip',
			buttons: [
			{ extend: 'copy', exportOptions: { columns: ':visible'}},
			{ extend: 'print', exportOptions: { columns: ':visible'}},
			{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
			{ extend: 'csv', exportOptions: { columns: ':visible'}},
			{ extend: 'colvis', text:'Column'},
			],
			columnDefs: [ {
				targets: -1,
				visible: true
			} ],
			lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			processing: true,
			serverSide: true,
			ajax: {
				"type"   : "GET",
				"url"    : "{{ route('admin.getCalegvoteRoute') }}",
				"data"   : function( d ) {
						      d.caleg_id 	= $('#caleg_id').val();
						      d.filter_voteby 	= ($('#filter_voteby').val()?$('#filter_voteby').val():'kabupaten');
						   },
 				// "dataSrc": ""
			},
			columns: [
			{data: 'data_name', name: 'data_name'},
			{data: 'score_average', name: 'score_average'},
			],
			order: [[1, 'desc']],
		});


		// table.destroy();
		// table.destroy();
	}
</script>
@endsection