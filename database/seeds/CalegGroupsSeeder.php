<?php

use Illuminate\Database\Seeder;

class CalegGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('caleg_groups')->insert([
			["id" => 1, "caleg_group_name" => "DPR", "publication_status" => 1],
			["id" => 2, "caleg_group_name" => "DPRD 1", "publication_status" => 1],
			["id" => 3, "caleg_group_name" => "DPRD 2", "publication_status" => 1]
		]);
    }
}
