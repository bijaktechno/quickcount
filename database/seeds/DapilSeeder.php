<?php

use Illuminate\Database\Seeder;

class DapilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('dapil')->insert([
			['id' => 1, 'dapil_name' => 'Jawa Barat 1', 'publication_status' => 1],
			['id' => 2, 'dapil_name' => 'Jawa Barat 2', 'publication_status' => 1],
			['id' => 3, 'dapil_name' => 'Jawa Barat 3', 'publication_status' => 1],
			['id' => 4, 'dapil_name' => 'Jawa Barat 4', 'publication_status' => 1],
			['id' => 5, 'dapil_name' => 'Jawa Barat 5', 'publication_status' => 1],
			['id' => 6, 'dapil_name' => 'Jawa Barat 6', 'publication_status' => 1],
			['id' => 7, 'dapil_name' => 'Jawa Barat 7', 'publication_status' => 1],
			['id' => 8, 'dapil_name' => 'Jawa Barat 8', 'publication_status' => 1],
			['id' => 9, 'dapil_name' => 'Jawa Barat 9', 'publication_status' => 1],
			['id' => 10, 'dapil_name' => 'Jawa Barat 10', 'publication_status' => 1],
			['id' => 11, 'dapil_name' => 'Jawa Barat 11', 'publication_status' => 1],
			['id' => 12, 'dapil_name' => 'Jawa Barat 12', 'publication_status' => 1],
			['id' => 13, 'dapil_name' => 'Jawa Barat 13', 'publication_status' => 1],
			['id' => 14, 'dapil_name' => 'Jawa Barat 14', 'publication_status' => 1],
			['id' => 15, 'dapil_name' => 'Jawa Barat 15', 'publication_status' => 1],
			['id' => 16, 'dapil_name' => 'kosong', 'publication_status' => 0]

        ]);
    }
}
