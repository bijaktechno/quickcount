<?php

use Illuminate\Database\Seeder;

class KabupatenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
    	DB::table('kabupaten')->insert([
			['id' => 127, 'dapil_id' => 1, 'kabupaten_name' => 'Bogor', 'publication_status' => 1],
			['id' => 128, 'dapil_id' => 1, 'kabupaten_name' => 'Sukabumi', 'publication_status' => 1],
			['id' => 129, 'dapil_id' => 1, 'kabupaten_name' => 'Cianjur', 'publication_status' => 1],
			['id' => 130, 'dapil_id' => 1, 'kabupaten_name' => 'Bandung', 'publication_status' => 1],
			['id' => 131, 'dapil_id' => 1, 'kabupaten_name' => 'Garut', 'publication_status' => 1],
			['id' => 132, 'dapil_id' => 1, 'kabupaten_name' => 'Tasikmalaya', 'publication_status' => 1],
			['id' => 133, 'dapil_id' => 1, 'kabupaten_name' => 'Ciamis', 'publication_status' => 1],
			['id' => 134, 'dapil_id' => 1, 'kabupaten_name' => 'Kuningan', 'publication_status' => 1],
			['id' => 135, 'dapil_id' => 1, 'kabupaten_name' => 'Cirebon', 'publication_status' => 1],
			['id' => 136, 'dapil_id' => 1, 'kabupaten_name' => 'Majalengka', 'publication_status' => 1],
			['id' => 137, 'dapil_id' => 1, 'kabupaten_name' => 'Sumedang', 'publication_status' => 1],
			['id' => 138, 'dapil_id' => 1, 'kabupaten_name' => 'Indramayu', 'publication_status' => 1],
			['id' => 139, 'dapil_id' => 1, 'kabupaten_name' => 'Subang', 'publication_status' => 1],
			['id' => 140, 'dapil_id' => 1, 'kabupaten_name' => 'Purwakarta', 'publication_status' => 1],
			['id' => 141, 'dapil_id' => 1, 'kabupaten_name' => 'Karawang', 'publication_status' => 1],
			['id' => 142, 'dapil_id' => 1, 'kabupaten_name' => 'Bekasi', 'publication_status' => 1],
			['id' => 143, 'dapil_id' => 1, 'kabupaten_name' => 'Kota Bogor', 'publication_status' => 1],
			['id' => 144, 'dapil_id' => 1, 'kabupaten_name' => 'Kota Sukabumi', 'publication_status' => 1],
			['id' => 145, 'dapil_id' => 1, 'kabupaten_name' => 'Kota Bandung', 'publication_status' => 1],
			['id' => 146, 'dapil_id' => 1, 'kabupaten_name' => 'Kota Cirebon', 'publication_status' => 1],
			['id' => 147, 'dapil_id' => 1, 'kabupaten_name' => 'Kota Bekasi', 'publication_status' => 1],
			['id' => 148, 'dapil_id' => 1, 'kabupaten_name' => 'Kota Depok', 'publication_status' => 1],
			['id' => 149, 'dapil_id' => 1, 'kabupaten_name' => 'Kota Cimahi', 'publication_status' => 1],
			['id' => 150, 'dapil_id' => 1, 'kabupaten_name' => 'Kota Tasikmalaya', 'publication_status' => 1],
			['id' => 445, 'dapil_id' => 1, 'kabupaten_name' => 'Kota Banjar', 'publication_status' => 1]
        ]);
    }
}
