<section class="footer bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h3>About</h3>
        {!! $setting->about_us !!}
      </div>
      <div class="col-md-5">
        <h3>Contact</h3>
        <p><i class="fa fa-phone fa-fw"></i> {{ $setting->phone }} <br> <i class="fa fa-envelope fa-fw"></i> {{ $setting->email }} <br> <i class="fa fa-map-marker fa-fw"></i> {{ $setting->address_line_one }}
        </p>
      </div>
    </div>
    <hr>
    <div class="row">
      <!-- <div class="col-md-4">
        <ul class="list-inline">
          <li><a href="/"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
          <li><a href="/"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
          <li><a href="/"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
          <li><a href="/"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
        </ul>
      </div> -->
      <div class="col-md-12">
        <p class="small">{!! $setting->copyright !!}</p>
      </div>
    </div>
  </div>
</section>