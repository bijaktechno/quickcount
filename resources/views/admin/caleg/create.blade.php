@extends('admin.layouts.app')
@section('title', 'Caleg')


@section('style')
<link rel="stylesheet" href="{{ asset('public/admin/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/admin/css/jquery.inputpicker.css')}}">
<style type="text/css">
.modal-body {
    position: relative;
    padding: 25px;
}
.modal-content {
    position: relative;
    background-color: #fff;
    border: 1px solid #999;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 6px;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
    background-clip: padding-box;
    outline: 0;
}
</style>
@endsection

@section('content')
<!-- Page header -->

<section class="content-header">
	<h1>
		Caleg
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> Dashboard</a></li>
		<li><a href="{{ route('admin.caleg.index') }}">Caleg</a></li>
		<li class="active">Add Caleg</li>
	</ol>
</section>
<!-- /.page header -->

<!-- Main content -->
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Caleg</h3>

			<div class="box-tools">
				<a href="{{ route('admin.caleg.index') }}" class="btn btn-info btn-sm btn-flat"><i class="fa fa-list"></i> Manage Caleg</a>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<form id="caleg_add_form" name="caleg_add_form" class="form-horizontal" action="{{ route('admin.caleg.store') }}" method="POST" enctype="multipart/form-data"  onSubmit="return false">
				{{ csrf_field() }}


				<div class="form-group">
					<label for="caleg_group_id" class="col-md-2 control-label">Group Caleg</label>
					<div class="col-md-9">
						<select name="caleg_group_id" class="form-control" id="caleg_group_id">
							<option value="" selected disabled>Select One</option>
							@foreach($caleg_groups as $group)
							<option value="{{ $group->id }}">{{ $group->caleg_group_name}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="dapil_id" class="col-md-2 control-label">Nama Dapil</label>
					<div class="col-md-9">
						<input type="text" name="dapil_id" class="form-control" id="dapil_id" value="{{ old('dapil_id') }}" placeholder="ex: Nama Dapil">
					</div>
				</div>


				<div class="form-group">
					<label for="no_urut" class="col-md-2 control-label">Nomor Urut</label>
					<div class="col-md-5">
						<input type="number" name="no_urut" class="form-control" id="no_urut" value="{{ old('no_urut') }}" min="1" placeholder="ex: Nomor Urut Caleg">
					</div>
				</div>
<!-- 
				<div class="form-group">
					<label for="caleg_nik" class="col-md-2 control-label">NIK Caleg</label>
					<div class="col-md-9">
						<input type="text" name="caleg_nik" class="form-control" id="caleg_nik" value="{{ old('caleg_nik') }}" placeholder="ex: NIK Caleg">
					</div>
				</div> -->

				<div class="form-group">
					<label for="caleg_name" class="col-md-2 control-label">Nama Caleg</label>
					<div class="col-md-9">
						<input type="text" name="caleg_name" class="form-control" id="caleg_name" value="{{ old('caleg_name') }}" placeholder="ex: Nama Caleg">
					</div>
				</div>

				<div class="form-group">
					<label for="caleg_picture" class="col-md-2 control-label">Foto Caleg</label>
					<div class="col-md-5">
						<input type="file" name="caleg_picture" id="caleg_picture" class="form-control">
					</div>
				</div>
				
				<div class="form-group">
					<label for="caleg_gender" class="col-md-2 control-label">Jenis Kelamin</label>
					<div class="col-md-5">
						<select name="caleg_gender" class="form-control" id="caleg_gender">
							<option value="" selected disabled>Select One</option>
							<option value="L">Laki-Laki</option>
							<option value="P">Perempuan</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="kabupaten_id" class="col-md-2 control-label">Tempat Tinggal</label>
					<div class="col-md-9">
						<input type="text" name="kabupaten_id" class="form-control" id="kabupaten_id" value="{{ old('kabupaten_id') }}" placeholder="ex: Tempat Tinggal">
					</div>
				</div>

				<div class="form-group">
					<label for="frame_color" class="col-md-2 control-label">Warna Frame</label>
					<div class="col-md-5">
						<input class="jscolor form-control" id="frame_color" name="frame_color" value="ffed00">
					</div>
				</div>

				<div class="form-group">
					<label for="publication_status" class="col-md-2 control-label">Publication Status</label>
					<div class="col-md-5">
						<select name="publication_status" class="form-control" id="publication_status">
							<option value="" selected disabled>Select One</option>
							<option value="1">Published</option>
							<option value="0">Unpublished</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<button type="submit" class="btn btn-info btn-flat" id="store-button">Add Caleg</button>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box-body -->
		<div class="box-footer clearfix">
		</div>
	</div>
</section>
<!-- /.main content -->
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('public/admin/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/jscolor.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/jquery.inputpicker.js') }}"></script>

<script type="text/javascript">
	// $(function () {
	$(document).ready(function() {
		var date = new Date();
		//date.setDate(date.getDate()-1);

		get_input_picker();

        $('#caleg_date').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            startDate: date,
        });
        $('#caleg_date').datepicker('setDate', 'now');

		$('.summernote').summernote({
			height: 200
		})

		/** Store **/

		$("#caleg_add_form").on('submit', function(){
	        var idBtn = "#store-button";
			var defaultBtn = $("#store-button").html();
        	// var form_data = $(this).closest('form').serialize();
        	var act = $(this).attr('action');
        	var method = $(this).attr('method');

			$.ajax({
				url:act,
				type:method,
				data:new FormData(this),
	            contentType: false,
	            cache: false,
	            processData:false,
				success:function(data) {
					if(data.status == false) {
	        			var arr = data.message;
	        			var messages = '';

		                $.each(arr, function(index, value)
		                {
		                    if (value.length != 0)
		                    {
		                    	messages += value+"<br>";
		                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
		                    }
		                });

	                    $('.alert-danger').animate({ top: "0" }, 500).show();
	                    $('.alert-danger').html(messages);

	                    setTimeout(function(){
	                        hideAllMessages();
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                    }, 4000);
					}
					if(data.status == true) {

	                    $('.alert-success').animate({ top: "0" }, 500).show();
	                    $('.alert-success').html(data.message);

	                    setTimeout(function () {
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                        location.reload();
	                    }, 2000);
					}
				},
			});
		});
/*
		$("#store-button").click(function(){
	        var idBtn = "#"+this.id;
			var defaultBtn = $(this).html();
        	var form_data = $(this).closest('form').serialize();
        	var act = $(this).closest('form').attr('action');
        	var method = $(this).closest('form').attr('method');

			$.ajax({
				url:act,
				type:method,
				data:form_data,
				success:function(data) {
					if(data.status == false) {
	        			var arr = data.message;
	        			var messages = '';

		                $.each(arr, function(index, value)
		                {
		                    if (value.length != 0)
		                    {
		                    	messages += value+"<br>";
		                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
		                    }
		                });

	                    $('.alert-danger').animate({ top: "0" }, 500).show();
	                    $('.alert-danger').html(messages);

	                    setTimeout(function(){
	                        hideAllMessages();
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                    }, 4000);
					}
					if(data.status == true) {

	                    $('.alert-success').animate({ top: "0" }, 500).show();
	                    $('.alert-success').html(data.message);

	                    setTimeout(function () {
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                        location.reload();
	                    }, 2000);
					}
				},
			});
		});
*/

      	$("#caleg_group_id").change(function(){
	        
	        $("#dapil_id").val('');

	        get_dapil_picker();
      	});
	})

    function get_dapil_picker()
    {
    	var caleg_group_id = $("#caleg_group_id").val();

      $('#dapil_id').inputpicker({
          url: "{{ route('allDapilRoute') }}",
          urlParam:{"caleg_group_id":caleg_group_id},
          fields:['dapil_name'],
          fieldText:'dapil_name',
          fieldValue:'id',
          filterOpen: true,
      });
    }

	function get_input_picker()
	{
		$('#kabupaten_id').inputpicker({
		    url: "{{ route('allKabupatenRoute') }}",
		    // urlParam:[{"category_id":1}],
		    fields:['kabupaten_name'],
		    fieldText:'kabupaten_name',
		    fieldValue:'id',
		    filterOpen: true,
		});
	}
</script>
<script type="text/javascript">
	document.forms['caleg_add_form'].elements['category_id'].value = "{{ old('category_id') }}";
	document.forms['caleg_add_form'].elements['publication_status'].value = "{{ old('publication_status') }}";
</script>
@endsection