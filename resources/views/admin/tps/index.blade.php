@extends('admin.layouts.app')
@section('title', 'TPS')


@section('style')
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css"> -->

	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/dataTables.bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/buttons.dataTables.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/admin/css/jquery.inputpicker.css')}}">
	@endsection

	@section('content')
	<!-- Page header -->
	<section class="content-header">
		<h1>
			TPS
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> Dashboard</a></li>
			<li class="active">TPS</li>
		</ol>
	</section>
	<!-- /.page header -->

	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Manage TPS</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-info btn-sm btn-flat add-button"><i class="fa fa-plus"></i> Add TPS</button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div style="width: 100%; padding-left: -10px;">
					<div class="table-responsive">
						<table id="tps-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0">
							<thead>
								<tr>
									<th>Nama Kabupaten</th>
									<th>Nama Kecamatan</th>
									<th>Nama Desa</th>
									<th>Nama TPS</th>
									<th>Total DPT</th>
									<th width="10%">Publication Status</th>
									<th width="7%">Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
			</div>
		</div>
	</section>
	<!-- /.main content -->

	<!-- add TPS modal -->
	<div id="add-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							<span class="fa-stack fa-sm">
								<i class="fa fa-square-o fa-stack-2x"></i>
								<i class="fa fa-plus fa-stack-1x"></i>
							</span>
							Add TPS
						</h4>
					</div>
					<form role="form" id="tps_add_form" method="post" onSubmit="return false">
						{{ csrf_field() }}
						<div class="modal-body">
							<!-- datum -->
							<div class="row">
								<!-- <div class="form-group col-xs-6">
									<label for="tdapil_name">Nama Dapil</label>
									<input type="text" name="dapil_id" class="form-control" id="dapil_id" placeholder="ex: Dapil" value="">
								</div> -->
								<div class="form-group col-xs-4">
									<label for="tps_name">Nama Kabupaten</label>
									<input type="text" name="kabupaten_id" class="form-control" id="kabupaten_id" placeholder="ex: Kabupaten" value="">
								</div>
								
							<!-- </div>

							<div class="row"> -->
								<div class="form-group col-xs-4">
									<label for="tps_name">Nama Kecamatan</label>
									<input type="text" name="kecamatan_id" class="form-control" id="kecamatan_id" placeholder="ex: Kecamatan" value="">
								</div>
								<div class="form-group col-xs-4">
									<label for="tps_name">Nama Desa</label>
									<input type="text" name="desa_id" class="form-control" id="desa_id" placeholder="ex: Desa" value="">
								</div>
								
							</div>

							<div class="form-group">
								<label for="tps_name">Nama TPS</label>
								<input type="text" name="tps_name" class="form-control" id="tps_name" value="{{ old('tps_name') }}" placeholder="ex: tps">
							</div>

							<div class="row">
								<div class="col-xs-4">
									<div class="form-group">
										<label for="jumlah_dpt">Total DPT</label>
										<input type="number" name="jumlah_dpt" class="form-control" id="jumlah_dpt" value="{{ old('jumlah_dpt') }}" placeholder="ex: total DPT">
									</div>
									
								</div>
								<div class="col-xs-8">
									<div class="form-group">
										<label for="tps_name">Koordinat TPS</label>
										<input type="text" name="tps_coordinate" class="form-control" id="tps_coordinate" value="{{ old('tps_coordinate') }}" placeholder="ex: tps coordinate">
									</div>
								</div>
								
							</div>


							<div class="form-group">
								<label>Publication Status</label>
								<select class="form-control" name="publication_status" id="publication_status">
									<option selected disabled>Select One</option>
									<option value="1">Published</option>
									<option value="0">Unpublished</option>
								</select>
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-info btn-flat" id="store-button">Save changes</button>
						</div>
					</form>

				</div>
		</div>
	</div>
	<!-- /.add tps modal -->

	<!-- view tps modal -->
	<div id="view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="btn-group pull-right no-print">
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
								<i class="fa fa-print"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-remove"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
					</div>
					<h4 class="modal-title" id="view-tps-name-title"></h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped">
						<tbody>

							<tr>
								<td>Nama TPS</td>
								<td id="view-tps-name"></td>
							</tr>
							<tr>
								<td>Publication Status</td>
								<td id="view-publication-status"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer no-print">
					<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /.view tps modal -->

	<!-- delete tps modal -->
	<div id="delete-modal" class="modal modal-danger fade" id="modal-danger">
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">
						<span class="fa-stack fa-sm">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-trash fa-stack-1x"></i>
						</span>
						Are you sure want to delete this ?
					</h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
					<form method="post" role="form" id="delete_form">
						{{csrf_field()}}
						{{method_field('DELETE')}}
						<button type="submit" class="btn btn-outline">Delete</button>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.delete tps modal -->

	<!-- view user modal -->
	<div id="user-view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="btn-group pull-right no-print">
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
								<i class="fa fa-print"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-remove"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
					</div>
					<h4 class="modal-title" id="view-name"></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-9">
							<table class="table table-bordered table-striped">
								<tbody>
									<tr>
										<td width="20%">Role</td>
										<td id="view-role"></td>
									</tr>
									<tr>
										<td>Username</td>
										<td id="view-username"></td>
									</tr>
									<tr>
										<td>Email</td>
										<td id="view-email"></td>
									</tr>
									<tr>
										<td>Gender</td>
										<td id="view-gender"></td>
									</tr>
									<tr>
										<td>Phone</td>
										<td id="view-phone"></td>
									</tr>
									<tr>
										<td>Address</td>
										<td id="view-address"></td>
									</tr>
									<tr>
										<td>Facebook</td>
										<td id="view-facebook"></td>
									</tr>
									<tr>
										<td>Twitter</td>
										<td id="view-twitter"></td>
									</tr>
									<tr>
										<td>Google Plus</td>
										<td id="view-google-plus"></td>
									</tr>
									<tr>
										<td>Linkedin</td>
										<td id="view-linkedin"></td>
									</tr>
									<tr>
										<td>Status</td>
										<td id="view-status"></td>
									</tr>
									<tr>
										<td>About</td>
										<td id="view-about"></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-3">
							<img id="view-avatar" class="img-responsive img-thumbnail img-rounded">
						</div>
					</div>
				</div>
				<div class="modal-footer no-print">
					<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /.view user modal -->
	@endsection

	@section('script')
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.colVis.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/js/jquery.inputpicker.js') }}"></script>

	<script type="text/javascript">
		/** Load datatable **/
		$(document).ready(function() {
			get_table_data();

			get_dapil_picker();
			get_kabupaten_picker();

		});

		/** Edit **/
		$("#tps-table").on("click", ".edit-button", function(){
			var tps_id = $(this).data("id");
			var url = "{{ route('admin.tps.show', 'tps_id') }}";
			url = url.replace("tps_id", tps_id);
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					$('#add-modal').modal('show');
					var url = "{{ route('admin.tps.update', 'tps_id') }}";
					url = url.replace("tps_id", data.tps.id);
					$("#tps_add_form").attr("action",url);
					$("#tps_add_form").attr("method",'PUT');

					// $('#dapil_id').val(data.tps.desa.kecamatan.kabupaten.dapil_id);
					$('#kabupaten_id').val(data.tps.desa.kecamatan.kabupaten_id);
					$('#kecamatan_id').val(data.tps.desa.kecamatan_id);
					$('#desa_id').val(data.tps.desa_id);
					$('#tps_name').val(data.tps.tps_name);
					$('#jumlah_dpt').val(data.tps.jumlah_dpt);
					$('#tps_coordinate').val(data.tps.tps_coordinate);
					$('#publication_status').val(data.tps.publication_status);
							
					get_dapil_picker();
					
					get_kabupaten_picker();

					get_kecamatan_picker();

					get_desa_picker();

			}});
		});

		/** Delete **/
		$("#tps-table").on("click", ".delete-button", function(){
			var tps_id = $(this).data("id");
			var url = "{{ route('admin.tps.destroy', 'tps_id') }}";
			url = url.replace("tps_id", tps_id);
			$('#delete-modal').modal('show');
			$('#delete_form').attr('action', url);
		});

		/** View **/
		$("#tps-table").on("click", ".view-button", function(){
			var tps_id = $(this).data("id");
			var url = "{{ route('admin.tps.show', 'tps_id') }}";
			url = url.replace("tps_id", tps_id);
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					$('#view-modal').modal('show');
					$('#view-tps-name-title').text(data.tps.tps_name);
					$('#view-tps-name').text(data.tps.tps_name);
					if(data.tps.publication_status == 1){
						$('#view-publication-status').text('Published');
					}else{
						$('#view-publication-status').text('Unpublished');
					}
				}});
		});

		/** Add **/
		$(".add-button").click(function(){
			$('#add-modal').modal('show');

			var url = "{{ route('admin.tps.store') }}";
			$("#tps_add_form").attr("action",url);
			$("#tps_add_form").attr("method",'POST');
			
			get_dapil_picker();
		});

		/** Store **/
		$("#store-button").click(function(){
	        var idBtn = "#"+this.id;
			var defaultBtn = $(this).html();
        	var form_data = $(this).closest('form').serialize();
        	var act = $(this).closest('form').attr('action');
        	var method = $(this).closest('form').attr('method');

			$.ajax({
				url:act,
				type:method,
				data:form_data,
				success:function(data) {
					if(data.status == false) {
	        			var arr = data.message;
	        			var messages = '';

		                $.each(arr, function(index, value)
		                {
		                    if (value.length != 0)
		                    {
		                    	messages += value+"<br>";
		                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
		                    }
		                });

	                    $('.alert-danger').animate({ top: "0" }, 500).show();
	                    $('.alert-danger').html(messages);

	                    setTimeout(function(){
	                        hideAllMessages();
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                    }, 4000);
					}
					if(data.status == true) {

	                    $('.alert-success').animate({ top: "0" }, 500).show();
	                    $('.alert-success').html(data.message);

	                    setTimeout(function () {
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                        location.reload();
	                    }, 2000);
					}
				},
			});
		});

		$("#dapil_id").change(function(){

			$("#kabupaten_id").val('');
			$("#kecamatan_id").val('');
			$("#desa_id").val('');
			$("#tps_id").val('');

			get_kabupaten_picker();
	        if ($('#kecamatan_id').hasClass("inputpicker-original")) {
	          $('#kecamatan_id').inputpicker('destroy');
	        }
	        if ($('#desa_id').hasClass("inputpicker-original")) {
	          $('#desa_id').inputpicker('destroy');
	        }
		});

		$("#kabupaten_id").change(function(){

			$("#kecamatan_id").val('');
			$("#desa_id").val('');
			$("#tps_id").val('');

			get_kecamatan_picker();
	        if ($('#desa_id').hasClass("inputpicker-original")) {
	          $('#desa_id').inputpicker('destroy');
	        }
		});

		$("#kecamatan_id").change(function(){

			$("#desa_id").val('');
			$("#tps_id").val('');

			get_desa_picker();
		});


		function get_dapil_picker()
		{
			$('#dapil_id').inputpicker({
			    url: "{{ route('allDapilRoute') }}",
			    // urlParam:[{"category_id":1}],
			    fields:['dapil_name'],
			    fieldText:'dapil_name',
			    fieldValue:'id',
			    filterOpen: true,
			});
		}
		
	    function get_kabupaten_picker(){
	    	var dapil_id = $("#dapil_id").val();

	      $('#kabupaten_id').inputpicker({
	          url: "{{ route('allKabupatenRoute') }}",
	          urlParam:{'dapil_id':dapil_id},
	          fields:['kabupaten_name'],
	          fieldText:'kabupaten_name',
	          fieldValue:'id',
	          filterOpen: true,
	      })
	    }

	    function get_kecamatan_picker(){
	    	var kabupaten_id = $("#kabupaten_id").val();

	      $('#kecamatan_id').inputpicker({
	          url: "{{ route('allKecamatanRoute') }}",
	          urlParam:{'kabupaten_id':kabupaten_id},
	          fields:['kecamatan_name'],
	          fieldText:'kecamatan_name',
	          fieldValue:'id',
	          filterOpen: true,
	      })
	    }

	    function get_desa_picker(){
	    	var kecamatan_id = $("#kecamatan_id").val();

	      $('#desa_id').inputpicker({
	          url: "{{ route('allDesaRoute') }}",
	          urlParam:{"kecamatan_id":kecamatan_id},
	          fields:['desa_name'],
	          fieldText:'desa_name',
	          fieldValue:'id',
	          filterOpen: true,
	      })
	    }

		/** Get datatable **/
		function get_table_data(){

			$('#tps-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: {
					"type"   : "GET",
					"url"    : "{{ route('admin.getTpsRoute') }}",
					"data"   : function( d ) {
							      d.page 	= d.draw;
							   },
    				// "dataSrc": ""
				},
				columns: [
				{data: 'kabupaten_name', name: 'kabupaten.kabupaten_name'},
				{data: 'kecamatan_name', name: 'kecamatan.kecamatan_name'},
				{data: 'desa_name', name: 'desa.desa_name'},
				{data: 'tps_name', name: 'tps.tps_name'},
				{data: 'jumlah_dpt', name: 'tps.jumlah_dpt'},
				{data: function (data, type, row) {
							if(data.publication_status == 1){
								var url_unpublish = "{{ route('admin.unpublishedTpsRoute', 'tps_id') }}";
								url_unpublish = url_unpublish.replace("tps_id", data.id);

								var publication = '<a href="'+url_unpublish+'" class="btn btn-success btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Unpublished"><i class="icon fa fa-arrow-down"></i>Published</a>';
							}
							else{
								var url_publish = "{{ route('admin.publishedTpsRoute', 'tps_id') }}";
								url_publish = url_publish.replace("tps_id", data.id);

								var publication = '<a href="'+url_publish+'" class="btn btn-warning btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Published"><i class="icon fa fa-arrow-up"></i> Unpublished</a>'

							}
		                    return publication;
                },orderable: false, searchable: false},
                // 'publication_status', name: 'publication_status', orderable: false, searchable: false},
				{data: function (data, type, row) {
							var action = '<button style="margin-right:3px;" class="btn btn-info btn-xs view-button" data-id="' + data.id + '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button>'+ 
                				'<button style="margin-right:3px;" class="btn btn-primary btn-xs edit-button" data-id="' + data.id + '"data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button>'+
                    			'<button style="margin-right:3px;" class="btn btn-danger btn-xs delete-button" data-id="' + data.id + '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';
		                    return action;
                }, orderable: false, searchable: false},
				],
				order: [[0, 'desc']],
			});
		}

		/** User View **/
		$("#tps-table").on("click", ".user-view-button", function(){
			var id = $(this).data("id");
			var url = "{{ route('admin.users.show', 'id') }}";
			url = url.replace("id", id);
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					var src = '{{ asset('public/avatar') }}/';
					var default_avatar = '{{ asset('public/avatar/user.png') }}';
					$('#user-view-modal').modal('show');

					$('#view-name').text(data['name']);
					$('#view-username').text(data['username']);
					$('#view-email').text(data['email']);
					$("#view-avatar").attr("src", src+data['avatar']);
					if(data['avatar']){
						$("#view-avatar").attr("src", src+data['avatar']);
					}else{
						$("#view-avatar").attr("src", default_avatar);
					}
					if(data['gender'] == 'm'){
						$('#view-gender').text('Male');
					}else{
						$('#view-gender').text('Female');
					}
					$('#view-phone').text(data['phone']);
					$('#view-address').text(data['address']);
					$('#view-facebook').text(data['facebook']);
					$('#view-twitter').text(data['twitter']);
					$('#view-google-plus').text(data['google_plus']);
					$('#view-linkedin').text(data['linkedin']);
					$('#view-about').text(data['about']);
					if(data['role'] == 'admin'){
						$('#view-role').text('Admin');
					}else{
						$('#view-role').text('User');
					}
					if(data['activation_status'] == 1){
						$('#view-status').text('Active');
					}else{
						$('#view-status').text('Block');
					}
				}});
		});
	</script>
	@endsection