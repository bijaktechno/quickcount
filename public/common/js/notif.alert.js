

var myMessages = ['alert-success','alert-info','alert-warning','alert-danger']; // define the messages types      


$(document).ready(function(){
     
     hideAllMessages();
     
     $('.alert').click(function(){        
          $(this).animate({top: -$(this).outerHeight()}, 500).show();
      });    
     
});  

function hideAllMessages()
{
    // console.log(myMessages);

     var messagesHeights = new Array(); // this array will store height for each
   
     for (i=0; i<myMessages.length; i++)
     {
          messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
        // console.log(messagesHeights[i]);
          $('.' + myMessages[i]).css('top', -messagesHeights[i]); //move element outside viewport   
     }
}