@extends('admin.layouts.app')
@section('title', 'Dapil')


@section('style')
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css"> -->

	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/dataTables.bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/buttons.dataTables.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/admin/css/jquery.inputpicker.css')}}">
	@endsection

	@section('content')
	<!-- Page header -->
	<section class="content-header">
		<h1>
			Dapil
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> Dashboard</a></li>
			<li class="active">Dapil</li>
		</ol>
	</section>
	<!-- /.page header -->

	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Dapil</h3>

				<div class="box-tools">
					<button type="button" class="btn btn-info btn-sm btn-flat add-button"><i class="fa fa-plus"></i> Add Dapil</button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div style="width: 100%; padding-left: -10px;">
					<div class="table-responsive">
						<table id="dapil-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0">
							<thead>
								<tr>
									<th>Nama Dapil</th>
									<th>Created At</th>
									<th>Updated At</th>
									<th width="10%">Publication Status</th>
									<th width="7%">Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">
			</div>
		</div>
	</section>
	<!-- /.main content -->

	<!-- add dapil modal -->
	<div id="add-modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							<span class="fa-stack fa-sm">
								<i class="fa fa-square-o fa-stack-2x"></i>
								<i class="fa fa-plus fa-stack-1x"></i>
							</span>
							Add Dapil
						</h4>
					</div>
					<form role="form" id="dapil_add_form" method="post" onSubmit="return false">
						{{ csrf_field() }}
						<div class="modal-body">
							<!-- datum -->
							<div class="row">
					  	<div class="col-lg-3">
									<div class="form-group">
										<label for="group_id">Group Dapil</label>
										<input type="text" name="group_id" class="form-control" id="group_id" value="{{ old('group_id') }}" placeholder="ex: Group Dapil">
									</div>
								</div>

					  	<div class="col-lg-6">
									<div class="form-group">
										<label for="dapil_name">Nama Dapil</label>
										<input type="text" name="dapil_name" class="form-control" id="dapil_name" value="{{ old('dapil_name') }}" placeholder="ex: Dapil">
									</div>
								</div>

					  	<div class="col-lg-3">
									<div class="form-group">
										<label>Publication Status</label>
										<select class="form-control" name="publication_status" id="publication_status">
											<option selected disabled>Select One</option>
											<option value="1">Published</option>
											<option value="0">Unpublished</option>
										</select>
									</div>
					  	</div>
					  </div>


							<div class="form-group multi-file">
								<label>Area Wilayah Dapil</label>
								<div class="row control-group increment">
								  	<div class="col-lg-4">
						          		<input id="kabupaten_form_data_1" type="text" name="kabupaten_id[]" class="form-control files-data kabupaten_form" data-count="1" placeholder="kabupaten">
								  	</div><!-- /.col-lg-4 -->
								  	<div class="col-lg-4">
						          		<input id="kecamatan_form_data_1" type="text" name="kecamatan_id[]" class="form-control files-data kecamatan_form" data-count="1" placeholder="kecamatan">
								  	</div><!-- /.col-lg-4 -->
								  	<div class="col-lg-3">
						          		<input id="desa_form_data_1" type="text" name="desa_id[]" class="form-control files-data desa_form" data-count="1" placeholder="desa">
								  	</div>
								  	<div class="col-lg-1">
					            	<button class="btn btn-success file-add" type="button"><i class="glyphicon glyphicon-plus"></i></button>
								  	</div>
<!-- 								  	<div class="col-lg-4">
							        	<div class="input-group" >
							          		<input id="desa_form_data_1" type="text" name="desa_id[]" class="form-control files-data desa_form" data-count="1" placeholder="desa">
							          		<div class="input-group-btn"> 
								            	<button class="btn btn-success file-add" type="button"><i class="glyphicon glyphicon-plus"></i></button>
								          	</div>
							        	</div>
								  	</div> -->
								  	<!-- /.col-lg-6 -->
								</div><!-- /.row -->


							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-info btn-flat" id="store-button">Save changes</button>
						</div>
					</form>

				</div>
		</div>
	</div>
	<!-- /.add dapil modal -->

	<!-- view dapil modal -->
	<div id="view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="btn-group pull-right no-print">
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
								<i class="fa fa-print"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-remove"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
					</div>
					<h4 class="modal-title" id="view-dapil-name-title"></h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered table-striped">
						<tbody>

							<tr>
								<td>Nama dapil</td>
								<td id="view-dapil-name"></td>
							</tr>
							<tr>
								<td>Area kabupaten</td>
								<td id="view-area-kabupaten"></td>
							</tr>
							<tr>
								<td>Area kecamatan</td>
								<td id="view-area-kecamatan"></td>
							</tr>
							<tr>
								<td>Area desa</td>
								<td id="view-area-desa"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer no-print">
					<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /.view dapil modal -->

	<!-- delete dapil modal -->
	<div id="delete-modal" class="modal modal-danger fade" id="modal-danger">
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">
						<span class="fa-stack fa-sm">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fa fa-trash fa-stack-1x"></i>
						</span>
						Are you sure want to delete this ?
					</h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
					<form method="post" role="form" id="delete_form">
						{{csrf_field()}}
						{{method_field('DELETE')}}
						<button type="submit" class="btn btn-outline">Delete</button>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.delete dapil modal -->

	<!-- view user modal -->
	<div id="user-view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="btn-group pull-right no-print">
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
								<i class="fa fa-print"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-remove"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
					</div>
					<h4 class="modal-title" id="view-name"></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-9">
							<table class="table table-bordered table-striped">
								<tbody>
									<tr>
										<td width="20%">Role</td>
										<td id="view-role"></td>
									</tr>
									<tr>
										<td>Username</td>
										<td id="view-username"></td>
									</tr>
									<tr>
										<td>Email</td>
										<td id="view-email"></td>
									</tr>
									<tr>
										<td>Gender</td>
										<td id="view-gender"></td>
									</tr>
									<tr>
										<td>Phone</td>
										<td id="view-phone"></td>
									</tr>
									<tr>
										<td>Address</td>
										<td id="view-address"></td>
									</tr>
									<tr>
										<td>Facebook</td>
										<td id="view-facebook"></td>
									</tr>
									<tr>
										<td>Twitter</td>
										<td id="view-twitter"></td>
									</tr>
									<tr>
										<td>Google Plus</td>
										<td id="view-google-plus"></td>
									</tr>
									<tr>
										<td>Linkedin</td>
										<td id="view-linkedin"></td>
									</tr>
									<tr>
										<td>Status</td>
										<td id="view-status"></td>
									</tr>
									<tr>
										<td>About</td>
										<td id="view-about"></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-3">
							<img id="view-avatar" class="img-responsive img-thumbnail img-rounded">
						</div>
					</div>
				</div>
				<div class="modal-footer no-print">
					<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /.view user modal -->
	@endsection

	@section('script')
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.colVis.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/js/jquery.inputpicker.js') }}"></script>

	<script type="text/javascript">
		/** Load datatable **/
		$(document).ready(function() {
			get_table_data();
			get_group_picker();
	
			get_kabupaten_picker();



			var url = "{{ route('allCaleggroupRoute') }}";
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(result){

					var groupcaleg = '<label><select id="filter_group_id" name="filter_group_id" aria-controls="user-table" class="form-control" style="width:150px; margin-left:5px;">';
				  	
				  	groupcaleg += '<option value="" selected disabled>pilih group</option>';

					result.data.forEach(function(item){
					  groupcaleg += '<option value="'+item.id+'">'+item.caleg_group_name+'</option>';
					});

				  	groupcaleg += '</select></label>';
					
					$("#dapil-table_length").append(groupcaleg);

				}

			});



			$(".table-responsive").on('change', '#filter_group_id', function(){
		        $('#dapil-table').DataTable().ajax.reload();

			});


			$(".file-add").click(function(){ 
				var addCount = $('.control-group').length;
				addCount ++;
				var html = create_table_data(addCount);
			  	// var html = $(".clone").html();
	  	$(".multi-file").append(html);
				get_kabupaten_picker(addCount)
			});

			$("body").on("click",".file-remove",function(){ 
			  	$(this).parents(".control-group").remove();
			});

			$("body").on('change', '.kabupaten_form', function(){
				var count = $(this).data('count');

				$("#kecamatan_form_data_"+count).val('');
				$("#desa_form_data_"+count).val('');

				get_kecamatan_picker(count);
		    if ($('#desa_form_data_'+count).hasClass("inputpicker-original")) {
		      $('#desa_form_data_'+count).inputpicker('destroy');
		    }
			});


			$("body").on('change', '.kecamatan_form', function(){
				var count = $(this).data('count');

				$("#desa_form_data_"+count).val('');

				get_desa_picker(count);
			});

		});

		/** Edit **/
		$("#dapil-table").on("click", ".edit-button", function(){
			var dapil_id = $(this).data("id");
			var url = "{{ route('admin.dapil.show', 'dapil_id') }}";
			url = url.replace("dapil_id", dapil_id);

			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					$('#add-modal').modal('show');
					var url = "{{ route('admin.dapil.update', 'dapil_id') }}";
					url = url.replace("dapil_id", data.dapil.id);
					$("#dapil_add_form").attr("action",url);
					$("#dapil_add_form").attr("method",'PUT');

					$('#group_id').val(data.dapil.group_id);
					$('#dapil_name').val(data.dapil.dapil_name);
					$('#publication_status').val(data.dapil.publication_status);

					$('.clone-group').remove();
					get_group_picker();
					edit_wilayah_dapil(dapil_id);
			}});

		});

		/** Delete **/
		$("#dapil-table").on("click", ".delete-button", function(){
			var dapil_id = $(this).data("id");
			var url = "{{ route('admin.dapil.destroy', 'dapil_id') }}";
			url = url.replace("dapil_id", dapil_id);
			$('#delete-modal').modal('show');
			$('#delete_form').attr('action', url);
		});

		/** View **/
		$("#dapil-table").on("click", ".view-button", function(){
			var dapil_id = $(this).data("id");
			var url = "{{ route('wilayahDetailRoute', 'dapil_id') }}";
			url = url.replace("dapil_id", dapil_id);
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					$('#view-modal').modal('show');
					$('#view-dapil-name-title').text(data.dapil.dapil_name);
					$('#view-dapil-name').text(data.dapil.dapil_name);

					// if(data.kabupaten.isArray){
					var kabupaten = '';
					var kecamatan = '';
					var desa = '';

					// console.log(data.kabupaten)
					// console.log(data.desa)
					if(data.kabupaten.length > 0){
						data.kabupaten.forEach(function(element) {
							kabupaten += '<span class="label label-warning" style="margin-right:5px;">'+element.kabupaten_name+'</span>'
						});
					}
					if(data.kecamatan.length > 0){
						data.kecamatan.forEach(function(element) {
							kecamatan += '<span class="label label-warning" style="margin-right:5px;">'+element.kecamatan_name+'</span>'
						});
					}
					if(data.desa.length > 0){
						data.desa.forEach(function(element) {
							desa += '<span class="label label-warning" style="margin-right:5px;">'+element.desa_name+'</span>'
						});
					}

					$('#view-area-kabupaten').html(kabupaten);
					$('#view-area-kecamatan').html(kecamatan);
					$('#view-area-desa').html(desa);					

					// }
				}});
		});

		/** Add **/
		$(".add-button").click(function(){
			$('#add-modal').modal('show');

			var url = "{{ route('admin.dapil.store') }}";
			$("#dapil_add_form").attr("action",url);
			$("#dapil_add_form").attr("method",'POST');
		});

		/** Store **/
		$("#store-button").click(function(){
      var idBtn = "#"+this.id;
			var defaultBtn = $(this).html();
    	var form_data = $(this).closest('form').serialize();
    	var act = $(this).closest('form').attr('action');
    	var method = $(this).closest('form').attr('method');

			$.ajax({
				url:act,
				type:method,
				data:form_data,
				success:function(data) {
					if(data.status == false) {
	        			var arr = data.message;
	        			var messages = '';

		                $.each(arr, function(index, value)
		                {
		                    if (value.length != 0)
		                    {
		                    	messages += value+"<br>";
		                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
		                    }
		                });

	                    $('.alert-danger').animate({ top: "0" }, 500).show();
	                    $('.alert-danger').html(messages);

	                    setTimeout(function(){
	                        hideAllMessages();
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                    }, 4000);
					}
					if(data.status == true) {

	                    $('.alert-success').animate({ top: "0" }, 500).show();
	                    $('.alert-success').html(data.message);

	                    setTimeout(function () {
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                        location.reload();
	                    }, 2000);
					}
				},
			});
		});

		function edit_wilayah_dapil(dapil_id)
		{
			var url = "{{ route('allWilayahdapilRoute', 'dapil_id') }}";
			url = url.replace("dapil_id", dapil_id);

			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(result){

					var id = 2;

					result.data.forEach(function(element) {
									var kabupaten_id = (element.kabupaten_id ? element.kabupaten_id : '');
									var kecamatan_id = (element.kecamatan_id ? element.kecamatan_id : '');
									var desa_id 					= (element.desa_id ? element.desa_id : '');

					    var html = create_table_data(id, kabupaten_id, kecamatan_id, desa_id)
								  	// var html = $(".clone").html();
						  	$(".multi-file").append(html);
									get_kabupaten_picker(id);
									if(kecamatan_id){
										get_kecamatan_picker(id);
									}
									if(desa_id){
										get_desa_picker(id);
									}

					    id ++;
					});

			}});

		}

		function create_table_data(id, kabupaten_id = '', kecamatan_id = '', desa_id = '')
		{

			var html = '';

			html += '<div class="row control-group clone-group" style="margin-top:10px">';
			html += '<div class="col-lg-4">';
			html += '<input id="kabupaten_form_data_'+id+'" type="text" name="kabupaten_id[]" class="form-control files-data kabupaten_form" data-count="'+id+'" placeholder="kabupaten" value="'+kabupaten_id+'">';
			html += '</div>';
			html += '<div class="col-lg-4">';
			html += '<input id="kecamatan_form_data_'+id+'" type="text" name="kecamatan_id[]" class="form-control files-data kecamatan_form" data-count="'+id+'" placeholder="kecamatan" value="'+kecamatan_id+'">';
			html += '</div>';
			html += '<div class="col-lg-3">';
			html += '<input id="desa_form_data_'+id+'" type="text" name="desa_id[]" class="form-control files-data desa_form" data-count="'+id+'" placeholder="desa" value="'+desa_id+'">';
			html += '</div>';
			html += '<div class="col-lg-1">';
			html += '<button class="btn btn-danger file-remove" type="button"><i class="glyphicon glyphicon-remove"></i></button>';
			html += '</div>';
			html += '</div>';

			return html;

		}

		/** Get datatable **/
		function get_table_data(){
			$('#dapil-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: {
					"type"   : "GET",
					"url"    : "{{ route('admin.getDapilRoute') }}",
					"data"   : function( d ) {
							      d.group_id 	= $('#filter_group_id').val();
							   },
    				// "dataSrc": ""
				},
				columns: [
				{data: 'dapil_name'},
				{data: 'created_at'},
				{data: 'updated_at'},
				{data: 'publication_status', name: 'publication_status', orderable: false, searchable: false},
				{data: 'action', name: 'action', orderable: false, searchable: false},
				],
				order: [[0, 'asc']],
			});
		}

  function get_group_picker(){
  	// var dapil_id = $(".kabupaten_form_data").val();

    $('#group_id').inputpicker({
        url: "{{ route('allCaleggroupRoute') }}",
        // urlParam:{'dapil_id':dapil_id},
        fields:['caleg_group_name'],
        fieldText:'caleg_group_name',
        fieldValue:'id',
        filterOpen: true,
    })
  }

  function get_kabupaten_picker(count = 1){
  	// var dapil_id = $(".kabupaten_form_data").val();

    $('#kabupaten_form_data_'+count).inputpicker({
        url: "{{ route('allKabupatenRoute') }}",
        // urlParam:{'dapil_id':dapil_id},
        fields:['kabupaten_name'],
        fieldText:'kabupaten_name',
        fieldValue:'id',
        filterOpen: true,
    })
  }


  function get_kecamatan_picker(count){
  	var kabupaten_id = $("#kabupaten_form_data_"+count).val();

    $('#kecamatan_form_data_'+count).inputpicker({
        url: "{{ route('allKecamatanRoute') }}",
        urlParam:{'kabupaten_id':kabupaten_id},
        fields:['kecamatan_name'],
        fieldText:'kecamatan_name',
        fieldValue:'id',
        filterOpen: true,
    })
  }

  function get_desa_picker(count){
  	var kecamatan_id = $("#kecamatan_form_data_"+count).val();

    $('#desa_form_data_'+count).inputpicker({
        url: "{{ route('allDesaRoute') }}",
        urlParam:{"kecamatan_id":kecamatan_id},
        fields:['desa_name'],
        fieldText:'desa_name',
        fieldValue:'id',
        filterOpen: true,
    })
  }
		/** User View **/
		$("#dapil-table").on("click", ".user-view-button", function(){
			var id = $(this).data("id");
			var url = "{{ route('admin.users.show', 'id') }}";
			url = url.replace("id", id);
			$.ajax({
				url: url,
				method: "GET",
				dataType: "json",
				success:function(data){
					var src = '{{ asset('public/avatar') }}/';
					var default_avatar = '{{ asset('public/avatar/user.png') }}';
					$('#user-view-modal').modal('show');

					$('#view-name').text(data['name']);
					$('#view-username').text(data['username']);
					$('#view-email').text(data['email']);
					$("#view-avatar").attr("src", src+data['avatar']);
					if(data['avatar']){
						$("#view-avatar").attr("src", src+data['avatar']);
					}else{
						$("#view-avatar").attr("src", default_avatar);
					}
					if(data['gender'] == 'm'){
						$('#view-gender').text('Male');
					}else{
						$('#view-gender').text('Female');
					}
					$('#view-phone').text(data['phone']);
					$('#view-address').text(data['address']);
					$('#view-facebook').text(data['facebook']);
					$('#view-twitter').text(data['twitter']);
					$('#view-google-plus').text(data['google_plus']);
					$('#view-linkedin').text(data['linkedin']);
					$('#view-about').text(data['about']);
					if(data['role'] == 'admin'){
						$('#view-role').text('Admin');
					}else{
						$('#view-role').text('User');
					}
					if(data['activation_status'] == 1){
						$('#view-status').text('Active');
					}else{
						$('#view-status').text('Block');
					}
				}});
		});
	</script>
	@endsection