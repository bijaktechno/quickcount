<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\User::class, 10)->create();
        
    	DB::table('users')->insert([

            [
                'id' => 1,
                'group_id' => 1,
                'tps_id' => NULL,
                'desa_id' => NULL,
                'nik' => 'admin',
    			'name' => 'admin',
                'username' => 'admin',
                'email' => 'admin@mail.com',
                'password' => bcrypt('secret'),
                'passmd5' => md5('secret'),
    			'role' => 'admin',
    			'remember_token' => str_random(10)
            ],
            [
                'id' => 2,
                'group_id' => 2,
                'tps_id' => 1,
                'desa_id' => 20037,
                'nik' => 'saksi',
                'name' => 'saksi',
                'username' => 'saksi',
                'email' => 'saksi@mail.com',
                'password' => bcrypt('secret'),
                'passmd5' => md5('secret'),
                'role' => 'saksi',
                'remember_token' => str_random(10)
            ]
        ]);
    }
}
