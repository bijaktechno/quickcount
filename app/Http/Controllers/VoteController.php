<?php

namespace App\Http\Controllers;

use App\Caleg;
use App\UserVotes;
use App\DapilCalegtps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        //
        return view('admin.vote.index', compact('user'));
    }

    public function get(Request $request) {
        $to = $request->get('order')[0]['column'];
        $field = $request->get('columns')[$to]['name'];

        $count = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                ->leftJoin('users', 'users.id', '=', 'user_votes.user_id')
                ->leftJoin('tps', 'tps.id', '=', 'users.tps_id')
                ->leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                ->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
                ->when(!empty($request->get('caleg_group_id')) , function ($query) use($request){
                    return $query->where('dapil.group_id', $request->get('caleg_group_id'));
                })
                ->when(!empty($request->get('dapil_id')) , function ($query) use($request){
                    return $query->where('dapil.id', $request->get('dapil_id'));
                })
                ->when(!empty($request->get('kabupaten_id')) , function ($query) use($request){
                    return $query->where('kabupaten.id', $request->get('kabupaten_id'));
                })
                ->when(!empty($request->get('kecamatan_id')) , function ($query) use($request){
                    return $query->where('kecamatan.id', $request->get('kecamatan_id'));
                })
                ->when(!empty($request->get('desa_id')) , function ($query) use($request){
                    return $query->where('desa.id', $request->get('desa_id'));
                })
                ->when(!empty($request->get('tps_id')) , function ($query) use($request){
                    return $query->where('tps.id', $request->get('tps_id'));
                })
                ->select(array('tps.tps_name', 'desa.desa_name', 'kecamatan.kecamatan_name', 'kabupaten.kabupaten_name', 'dapil.dapil_name',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', DB::raw('SUM(score) as score_average')))
                ->groupBy('tps.tps_name', 'desa.desa_name', 'kecamatan.kecamatan_name', 'kabupaten.kabupaten_name', 'dapil.dapil_name',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik')
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('tps.tps_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kabupaten.kabupaten_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('dapil.dapil_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('caleg.caleg_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->get()
                ->count();

        $caleg = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                ->leftJoin('users', 'users.id', '=', 'user_votes.user_id')
                ->leftJoin('tps', 'tps.id', '=', 'users.tps_id')
                ->leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                ->leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
                ->when(!empty($request->get('caleg_group_id')) , function ($query) use($request){
                    return $query->where('dapil.group_id', $request->get('caleg_group_id'));
                })
                ->when(!empty($request->get('dapil_id')) , function ($query) use($request){
                    return $query->where('dapil.id', $request->get('dapil_id'));
                })
                ->when(!empty($request->get('kabupaten_id')) , function ($query) use($request){
                    return $query->where('kabupaten.id', $request->get('kabupaten_id'));
                })
                ->when(!empty($request->get('kecamatan_id')) , function ($query) use($request){
                    return $query->where('kecamatan.id', $request->get('kecamatan_id'));
                })
                ->when(!empty($request->get('desa_id')) , function ($query) use($request){
                    return $query->where('desa.id', $request->get('desa_id'));
                })
                ->when(!empty($request->get('tps_id')) , function ($query) use($request){
                    return $query->where('tps.id', $request->get('tps_id'));
                })
                ->select(array('tps.tps_name', 'desa.desa_name', 'kecamatan.kecamatan_name', 'kabupaten.kabupaten_name', 'dapil.dapil_name',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', DB::raw('SUM(score) as score_average')))
                ->groupBy('tps.tps_name', 'desa.desa_name', 'kecamatan.kecamatan_name', 'kabupaten.kabupaten_name', 'dapil.dapil_name',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik')
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('tps.tps_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kabupaten.kabupaten_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('dapil.dapil_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('caleg.caleg_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                // ->orderBy('score_average', 'desc')
                ->orderBy($field, $request->get('order')[0]['dir'])
                ->offset($request->get('start'))
                ->limit($request->get('length'))
                ->get();


        $data = array(
            'recordsTotal' => $count, 
            'recordsFiltered' => $count, 
            'data' => $caleg
        );

        // return Response::json($tps);
        return $data;
        /*return datatables()->of($caleg)
            ->setRowId('id')
            ->make(true);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function show_(Request $request, $id=null)
    {

        if($id==null){
            $data['data'] = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                        ->leftJoin('users', 'users.id', '=', 'user_votes.user_id')
                        ->leftJoin('tps', 'tps.id', '=', 'users.tps_id')
                        ->leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                        ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                        ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                        ->leftJoin('dapil', 'dapil.id', '=', 'kabupaten.dapil_id')
                        ->select(array('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', DB::raw('SUM(score) as score_average')
                            ))
                        ->selectRaw('SUBSTRING(caleg.caleg_name, 1, 10) as caleg_shortname')
                        ->when(!empty($request->get('caleg_group_id')) , function ($query) use($request){
                            return $query->where('caleg.caleg_group_id', $request->get('caleg_group_id'));
                        })
                        ->when(!empty($request->get('dapil_id')) , function ($query) use($request){
                            return $query->where('dapil.id', $request->get('dapil_id'));
                        })
                        ->when(!empty($request->get('kabupaten_id')) , function ($query) use($request){
                            return $query->where('kabupaten.id', $request->get('kabupaten_id'));
                        })
                        ->when(!empty($request->get('kecamatan_id')) , function ($query) use($request){
                            return $query->where('kecamatan.id', $request->get('kecamatan_id'));
                        })
                        ->when(!empty($request->get('desa_id')) , function ($query) use($request){
                            return $query->where('desa.id', $request->get('desa_id'));
                        })
                        ->when(!empty($request->get('tps_id')) , function ($query) use($request){
                            return $query->where('tps.id', $request->get('tps_id'));
                        })
                        ->with('dapil')
                        ->groupBy('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik')
                        ->orderBy('score_average', 'desc')
                        ->get();
        }
        else{
        }

        return Response::json($data);
    }*/

    public function show(Request $request, $id=null)
    {

        if($id==null){
            $data['data'] = DapilCalegtps::select(array('dapil_id',  'caleg_name', 'caleg_id as id', 'caleg_picture', 'frame_color', 'caleg_nik', DB::raw('SUM(score) as score_average')
                            ))
                        ->selectRaw('SUBSTRING(caleg_name, 1, 10) as caleg_shortname')
                        ->when(!empty($request->get('caleg_group_id')) , function ($query) use($request){
                            return $query->where('caleg_group_id', $request->get('caleg_group_id'));
                        })
                        ->when(!empty($request->get('dapil_id')) , function ($query) use($request){
                            return $query->where('dapil_id', $request->get('dapil_id'));
                        })
                        ->when(!empty($request->get('kabupaten_id')) , function ($query) use($request){
                            return $query->where('kabupaten_id', $request->get('kabupaten_id'));
                        })
                        ->when(!empty($request->get('kecamatan_id')) , function ($query) use($request){
                            return $query->where('kecamatan_id', $request->get('kecamatan_id'));
                        })
                        ->when(!empty($request->get('desa_id')) , function ($query) use($request){
                            return $query->where('desa_id', $request->get('desa_id'));
                        })
                        ->when(!empty($request->get('tps_id')) , function ($query) use($request){
                            return $query->where('tps_id', $request->get('tps_id'));
                        })
                        ->with('dapil')
                        ->groupBy('dapil_id',  'caleg_name', 'caleg_id', 'caleg_picture', 'frame_color', 'caleg_nik')
                        ->orderBy('score_average', 'desc')
                        ->get();
        }
        else{
            // $data['kabupaten'] = UserVotes::where('id', $id)->first();
        }

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
