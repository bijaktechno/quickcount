<div class="collapse navbar-collapse navbar-right navbar-main-collapse">
  <ul class="nav navbar-nav">
    <!-- Hidden li included to remove active class from about link when scrolled up past about section-->
    <li class="hidden"><a href="#page-top"></a></li>
    <li><a href="#chart" class="page-scroll">Perhitungan</a></li>
    <li><a href="#caleg" class="page-scroll">Caleg</a></li>
    <!-- <li><a href="#about" class="page-scroll">About</a></li> -->
    <li><a href="#contact" class="page-scroll">Contact</a></li>
  </ul>
</div>