<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVotes extends Model
{
	protected $table = 'user_votes';

    protected $fillable = [
        'caleg_id', 'user_id', 'score', 'user_coordinate'
    ];

    public function caleg() {
        return $this->belongsTo(Caleg::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
