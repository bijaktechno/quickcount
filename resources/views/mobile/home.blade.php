@extends('mobile.layouts.app')

@section('title', $setting->meta_title)
@section('keywords', $setting->meta_keywords)
@section('description', $setting->meta_description)

@section('style')
@endsection

@section('content')

<section id="chart" class="bg-white">

  <div class="container wow fadeIn">

    <div class="row">
  			<canvas id="myChart" style="max-width: 100%;"></canvas>
	  </div>
	</div>
</section>


<!-- Team Section-->
<section id="caleg" class=" text-center bg-gray">
  <div class="container">
    <h3>Detail Caleg</h3>
    <div class="row">
      <div class="col-md-3 col-sm-6">
      	<div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <figure>
                           <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" class="img-circle" style="width:75px;" id="user-img">
                      </figure>
                      <h5 style="text-align:center;"><strong id="user-name">Nama Caleg</strong></h5>
                      <p style="text-align:center;font-size: smaller;" id="user-frid">NIK123456 </p>
                      <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email">email@gmail.com </p>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                      <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                      <!-- <p style="text-align:center;font-size: smaller;" id="user-role">Software Engineer</p> -->
                      <div class="col-lg-12 left progress" style="padding: 0">
                    		<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: 40%; background-color: rgba(215, 208, 16, 1);">caleg 40%
                    		</div>
                      </div>

                    </div>
                </div>
	          </div>
	      </div>
      </div>

      <div class="col-md-3 col-sm-6">
      	<div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <figure>
                           <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" class="img-circle" style="width:75px;" id="user-img">
                      </figure>
                      <h5 style="text-align:center;"><strong id="user-name">Nama Caleg</strong></h5>
                      <p style="text-align:center;font-size: smaller;" id="user-frid">NIK123456 </p>
                      <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email">email@gmail.com </p>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                      <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                      <!-- <p style="text-align:center;font-size: smaller;" id="user-role">Software Engineer</p> -->
                      <div class="col-lg-12 left progress" style="padding: 0">
                    		<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: 40%; background-color: rgba(141, 108, 19, 1);">caleg 40%
                    		</div>
                      </div>

                    </div>
                </div>
	          </div>
	      </div>
      </div>

      <div class="col-md-3 col-sm-6">
      	<div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <figure>
                           <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" class="img-circle" style="width:75px;" id="user-img">
                      </figure>
                      <h5 style="text-align:center;"><strong id="user-name">Nama Caleg</strong></h5>
                      <p style="text-align:center;font-size: smaller;" id="user-frid">NIK123456 </p>
                      <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email">email@gmail.com </p>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                      <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                      <!-- <p style="text-align:center;font-size: smaller;" id="user-role">Software Engineer</p> -->
                      <div class="col-lg-12 left progress" style="padding: 0">
                    		<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: 40%; background-color: rgba(255, 206, 86, 1);">caleg 40%
                    		</div>
                      </div>

                    </div>
                </div>
	          </div>
	      </div>
      </div>

      <div class="col-md-3 col-sm-6">
      	<div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <figure>
                           <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" class="img-circle" style="width:75px;" id="user-img">
                      </figure>
                      <h5 style="text-align:center;"><strong id="user-name">Nama Caleg</strong></h5>
                      <p style="text-align:center;font-size: smaller;" id="user-frid">NIK123456 </p>
                      <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email">email@gmail.com </p>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                      <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                      <!-- <p style="text-align:center;font-size: smaller;" id="user-role">Software Engineer</p> -->
                      <div class="col-lg-12 left progress" style="padding: 0">
                    		<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: 40%; background-color: rgba(255, 186, 0, 1);">caleg 40%
                    		</div>
                      </div>

                    </div>
                </div>
	          </div>
	      </div>
      </div>

      <div class="col-md-3 col-sm-6">
      	<div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <figure>
                           <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" class="img-circle" style="width:75px;" id="user-img">
                      </figure>
                      <h5 style="text-align:center;"><strong id="user-name">Nama Caleg</strong></h5>
                      <p style="text-align:center;font-size: smaller;" id="user-frid">NIK123456 </p>
                      <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email">email@gmail.com </p>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                      <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                      <!-- <p style="text-align:center;font-size: smaller;" id="user-role">Software Engineer</p> -->
                      <div class="col-lg-12 left progress" style="padding: 0">
                    		<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: 40%; background-color: rgba(246, 255, 5, 1);">caleg 40%
                    		</div>
                      </div>

                    </div>
                </div>
	          </div>
	      </div>
      </div>

      <div class="col-md-3 col-sm-6">
      	<div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <figure>
                           <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" class="img-circle" style="width:75px;" id="user-img">
                      </figure>
                      <h5 style="text-align:center;"><strong id="user-name">Nama Caleg</strong></h5>
                      <p style="text-align:center;font-size: smaller;" id="user-frid">NIK123456 </p>
                      <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email">email@gmail.com </p>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                      <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                      <!-- <p style="text-align:center;font-size: smaller;" id="user-role">Software Engineer</p> -->
                      <div class="col-lg-12 left progress" style="padding: 0">
                    		<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: 40%; background-color: rgba(252, 202, 0, 1);">caleg 40%
                    		</div>
                      </div>

                    </div>
                </div>
	          </div>
	      </div>
      </div>

      <div class="col-md-3 col-sm-6">
      	<div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <figure>
                           <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" class="img-circle" style="width:75px;" id="user-img">
                      </figure>
                      <h5 style="text-align:center;"><strong id="user-name">Nama Caleg</strong></h5>
                      <p style="text-align:center;font-size: smaller;" id="user-frid">NIK123456 </p>
                      <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email">email@gmail.com </p>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                      <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                      <!-- <p style="text-align:center;font-size: smaller;" id="user-role">Software Engineer</p> -->
                      <div class="col-lg-12 left progress" style="padding: 0">
                    		<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: 40%; background-color: rgba(189, 127, 86, 1);">caleg 40%
                    		</div>
                      </div>

                    </div>
                </div>
	          </div>
	      </div>
      </div>

      <div class="col-md-3 col-sm-6">
      	<div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <figure>
                           <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" class="img-circle" style="width:75px;" id="user-img">
                      </figure>
                      <h5 style="text-align:center;"><strong id="user-name">Nama Caleg</strong></h5>
                      <p style="text-align:center;font-size: smaller;" id="user-frid">NIK123456 </p>
                      <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email">email@gmail.com </p>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                      <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                      <!-- <p style="text-align:center;font-size: smaller;" id="user-role">Software Engineer</p> -->
                      <div class="col-lg-12 left progress" style="padding: 0">
                    		<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: 40%; background-color: rgba(255, 159, 64, 1);">caleg 40%
                    		</div>
                      </div>

                    </div>
                </div>
	          </div>
	      </div>
      </div>

      <div class="col-md-3 col-sm-6">
      	<div class="row">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
	              <div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
                		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <figure>
                           <img src="{{ asset('public/web/img/caleg/calon1.png') }}" alt="" class="img-circle" style="width:75px;" id="user-img">
                      </figure>
                      <h5 style="text-align:center;"><strong id="user-name">Nama Caleg</strong></h5>
                      <p style="text-align:center;font-size: smaller;" id="user-frid">NIK123456 </p>
                      <p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email">email@gmail.com </p>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
                      <p style="text-align:center;font-size: smaller;"><strong>Perolehan suara</strong></p>
                      <!-- <p style="text-align:center;font-size: smaller;" id="user-role">Software Engineer</p> -->
                      <div class="col-lg-12 left progress" style="padding: 0">
                    		<div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-striped active" style="width: 40%; background-color: rgba(189, 151, 86, 1);">caleg 40%
                    		</div>
                      </div>

                    </div>
                </div>
	          </div>
	      </div>
      </div>
    </div>
  </div>
</section>

<!-- Services Section-->
<section id="about">
  <div class="container text-center">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <h3>Why Choose Us?</h3>
      </div>
    </div>
    <div class="row">
      <div data-wow-delay=".2s" class="col-lg-3 col-sm-6 wow fadeIn">
        <h4><i class="icon-big ion-ios-nutrition-outline"></i> Easy to Use</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p>
      </div>
      <div data-wow-delay=".4s" class="col-lg-3 col-sm-6 wow fadeIn">
        <h4><i class="icon-big ion-ios-sunny-outline"></i> Coder friendly</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p>
      </div>
      <div data-wow-delay=".6s" class="col-lg-3 col-sm-6 wow fadeIn">
        <h4><i class="icon-big ion-ios-glasses-outline"></i> Good readability</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p>
      </div>
      <div data-wow-delay=".8s" class="col-lg-3 col-sm-6 wow fadeIn">
        <h4><i class="icon-big icon ion-ios-infinite-outline"></i> Multi-purpose</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p>
      </div>
    </div>
  </div>
</section>
<!-- Action video-->
<section class="section-small bg-img2 text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2"><a href="https://vimeo.com/153485166" class="swipebox-video"><i class="icon icon-big ion-ios-play-outline"></i></a>
        <h2>Watch <span class="bold">Video</span>
        </h2>
        <p>A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can.</p>
      </div>
    </div>
  </div>
</section>
<!-- Twitter Widget-->
<div class="section container-fluid section-small bg-gray twitter-widget text-center">
  <div class="row">
    <div class="col-md-8 col-md-offset-2"><a href="https://twitter.com/jenyelkind"><i class="icon icon-big ion-social-twitter-outline"></i></a>
      <div id="tweecool"></div>
    </div>
  </div>
</div>
<!-- Contact Section-->
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h2>contact us</h2>
        <p>Feel free to contact us to provide some feedback on our templates, give us suggestions for new templates and themes, or to just say hello! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar.</p>
        <hr>
        <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> 1234 Some Avenue, New York, NY 56789
        </h5>
        <h5><i class="fa fa-envelope fa-fw fa-lg"></i> info@youwebsite.com
        </h5>
        <h5><i class="fa fa-phone fa-fw fa-lg"></i> (123) 456-7890
        </h5>
      </div>
      <div class="col-md-5 col-md-offset-2">
        <h2>Say hello</h2>
        <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
        <form id="contactForm" name="sentMessage" novalidate="">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="name" class="sr-only control-label">You Name</label>
              <input id="name" type="text" placeholder="You Name" required="" data-validation-required-message="Please enter name" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="email" class="sr-only control-label">You Email</label>
              <input id="email" type="email" placeholder="You Email" required="" data-validation-required-message="Please enter email" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="phone" class="sr-only control-label">You Phone</label>
              <input id="phone" type="tel" placeholder="You Phone" required="" data-validation-required-message="Please enter phone number" class="form-control input-lg"><span class="help-block text-danger"></span>
            </div>
          </div>
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label for="message" class="sr-only control-label">Message</label>
              <textarea id="message" rows="2" placeholder="Message" required="" data-validation-required-message="Please enter a message." aria-invalid="false" class="form-control input-lg"></textarea><span class="help-block text-danger"></span>
            </div>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-yellow">Send</button>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- Map Section-->



@endsection

@section('sidebar')

@endsection

@section('script')

  <script type="text/javascript" src="{{ asset('public/web/js/mdb.min.js') }}"></script>

  <script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ["test-1", "test-2", "test-3", "test-4", "test-5", "test-6","test-7","test-8","test-9"],
        datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3,3,5,9],
          backgroundColor: [
            'rgba(215, 208, 16, 0.7)',
            'rgba(141, 108, 19, 0.7)',
            'rgba(255, 206, 86, 0.7)',
            'rgba(255, 186, 0, 0.7)',
            'rgba(246, 255, 5, 0.7)',
            'rgba(252, 202, 0, 0.7)',
            'rgba(189, 127, 86, 0.7)',
            'rgba(255, 159, 64, 0.7)',
            'rgba(189, 151, 86, 0.7)'
          ],
          borderColor: [
            'rgba(215, 208, 16,1)',
            'rgba(141, 108, 19, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(255, 186, 0, 1)',
            'rgba(246, 255, 5, 1)',
            'rgba(252, 202, 0,1)',
            'rgba(189, 127, 86, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(189, 151, 86, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

  </script>
@endsection