<?php

namespace App\Http\Controllers;

use App\Desa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

class DesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.desa.index');
    }

    public function get() {
        $desa = Desa::all();

        return datatables()->of($desa)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->addColumn('kabupaten_name', function ($desa) {
                return $desa->kecamatan->kabupaten->kabupaten_name;
            })
            ->addColumn('kecamatan_name', function ($desa) {
                return $desa->kecamatan->kecamatan_name;
            })
            ->addColumn('publication_status', function ($desa) {
                if ($desa->publication_status == 1) {
                    return '<a href="' . route('admin.unpublishedDesaRoute', $desa->id) . '" class="btn btn-success btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Unpublished"><i class="icon fa fa-arrow-down"></i>Published</a>';
                }
                return '<a href="' . route('admin.publishedDesaRoute', $desa->id) . '" class="btn btn-warning btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Published"><i class="icon fa fa-arrow-up"></i> Unpublished</a>';})
            ->addColumn('action', function ($desa) {
                return '<button class="btn btn-info btn-xs view-button" data-id="' . $desa->id . '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button> 
                    <button class="btn btn-primary btn-xs edit-button" data-id="' . $desa->id . '"data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
                    <button class="btn btn-danger btn-xs delete-button" data-id="' . $desa->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})
            ->rawColumns(['publication_status', 'action', 'kabupaten_name', 'kecamatan_name'])
            ->setRowId('id')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'kecamatan_id'          => 'required',
            'desa_name'             => 'required',
            'publication_status'    => 'required',
        ], [
            'desa_name.required'  => 'Nama desa harus diisi.',
        ]);

        if ($validator->passes()) {
            $desa = Desa::create([
                'kecamatan_id'          => $request->input('kecamatan_id'),
                'desa_name'             => $request->input('desa_name'),
                'publication_status'    => $request->input('publication_status'),
            ]);

            $desa_id = $desa->id;

            if (!empty($desa_id)) {
                return Response::json(['status' => true,'message' => 'Data add successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }

        }

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id=null)
    {
        if($id==null){
            $data['data'] = Desa::where([['desa_name','LIKE',"%{$request->get('q')}%"]])->when(!empty($request->get('kecamatan_id')) , function ($query) use($request){
                    return $query->where('kecamatan_id', $request->get('kecamatan_id'));
                })->whereNotIn('publication_status', [0])->get(['id', 'desa_name']);
        }
        else{
            $data['desa'] = Desa::where('id', $id)->first();
        }

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $desa = Desa::find($id);

        $validator = $validator = Validator::make($request->all(), [
            'kecamatan_id'       => 'required',
            'desa_name'       => 'required',
            'publication_status' => 'required',
        ], [
            'desa_name.required'  => 'Nama desa harus diisi.',
        ]);

        if ($validator->passes()) {

            $desa->kecamatan_id = $request->get('kecamatan_id');
            $desa->desa_name = $request->get('desa_name');
            $desa->publication_status = $request->get('publication_status');
            $affected_row = $desa->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => true,'message' => 'Data update successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }

        }

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Desa::find($id);
        if (count($data)) {
            $data->delete();
            return redirect()->back()->with('message', 'Data delete successfully.');
        } else {
            return redirect()->back()->with('exception', 'Operation failed !');
        }
    }

    public function published($id) {
        $affected_row = Desa::where('id', $id)
            ->update(['publication_status' => 1]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Published successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    public function unpublished($id) {
        $affected_row = Desa::where('id', $id)
            ->update(['publication_status' => 0]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Unpublished successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }
}
