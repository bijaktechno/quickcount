<?php

use Illuminate\Database\Seeder;

class TpsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('tps')->insert([
            ["id" => 1, "desa_id" => 20037, "tps_name" => "TPS 01", "publication_status" => 1]
        ]);
    }
}
