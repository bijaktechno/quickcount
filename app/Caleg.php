<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caleg extends Model
{
	protected $table = 'caleg';

    protected $fillable = [
        'dapil_id', 'kabupaten_id', 'caleg_group_id', 'caleg_name', 'caleg_picture', 'caleg_gender','frame_color' ,'caleg_nik', 'no_urut', 'publication_status'
    ];
/*
    public function average($id) {
        return $id;

    }*/


    public function showablefields()
    {
        # code...
        return ['dapil_id', 'kabupaten_id', 'caleg_group_id', 'caleg_name', 'caleg_picture', 'caleg_gender','frame_color' ,'caleg_nik', 'no_urut', 'publication_status'];
    }

    public function countVotes($id)
    {
        return $this->select('average(score) as caleg_votes')
                    ->leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                    // ->where('dapil_id', )
                    ->first();
    }

    public function kabupaten() {
        return $this->belongsTo(Kabupaten::class);
    }

    public function dapil() {
        return $this->belongsTo(Dapil::class);
    }

    public function caleg_group() {
        return $this->belongsTo(CalegGroup::class);
    }

    public function uservotes() {
        return $this->hasMany(UserVotes::class);
    }
}
