<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
	protected $table = 'kabupaten';

    protected $fillable = [
        'dapil_id', 'kabupaten_name', 'publication_status'
    ];

    public function kecamatan() {
        return $this->hasMany(Kecamatan::class);
    }

    public function caleg() {
        return $this->hasMany(Caleg::class);
    }

    public function dapil() {
        return $this->belongsTo(Dapil::class);
    }
}
