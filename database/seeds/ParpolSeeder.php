<?php

use Illuminate\Database\Seeder;

class ParpolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('parpol')->insert([
			['no_urut' => 1, 'parpol_name' => 'PARTAI KEBANGKITAN BANGSA', 'parpol_alias' => 'PKB', 'parpol_picture' => '1 PKB.png'],
			['no_urut' => 2, 'parpol_name' => 'PARTAI GERAKAN INDONESIA RAYA', 'parpol_alias' => 'GERINDRA', 'parpol_picture' => '2 Gerindra.png'],
			['no_urut' => 3, 'parpol_name' => 'PARTAI DEMOKRASI INDONESIA PERJUANGAN', 'parpol_alias' => 'PDI PERJUANGAN', 'parpol_picture' => '3 PDIP.png'],
			['no_urut' => 4, 'parpol_name' => 'PARTAI GOLONGAN KARYA', 'parpol_alias' => 'GOLKAR', 'parpol_picture' => '4 Golkar.png'],
			['no_urut' => 5, 'parpol_name' => 'PARTAI NASDEM', 'parpol_alias' => 'NASDEM', 'parpol_picture' => '5 Partai Nasdem.png'],
			['no_urut' => 6, 'parpol_name' => 'PARTAI GERAKAN PERUBAHAN INDONESIA', 'parpol_alias' => 'GARUDA', 'parpol_picture' => '6 Partai Garuda.png'],
			['no_urut' => 7, 'parpol_name' => 'PARTAI BERKARYA', 'parpol_alias' => 'BERKARYA', 'parpol_picture' => '7 Partai Berkarya.png'],
			['no_urut' => 8, 'parpol_name' => 'PARTAI KEADILAN SEJAHTERA', 'parpol_alias' => 'PKS', 'parpol_picture' => '8 PKS.png'],
			['no_urut' => 9, 'parpol_name' => 'PARTAI PERSATUAN INDONESIA', 'parpol_alias' => 'PERINDO', 'parpol_picture' => '9 Perindo.png'],
			['no_urut' => 10, 'parpol_name' => 'PARTAI PERSATUAN PEMBANGUNAN', 'parpol_alias' => 'PPP', 'parpol_picture' => '10 PPP.png'],
			['no_urut' => 11, 'parpol_name' => 'PARTAI SOLIDARITAS INDONESIA', 'parpol_alias' => 'PSI', 'parpol_picture' => '11 PSI.png'],
			['no_urut' => 12, 'parpol_name' => 'PARTAI AMANAT NASIONAL', 'parpol_alias' => 'PAN', 'parpol_picture' => '12 PAN.png'],
			['no_urut' => 13, 'parpol_name' => 'PARTAI HATI NURANI RAKYAT', 'parpol_alias' => 'HANURA', 'parpol_picture' => '13 Hanura.png'],
			['no_urut' => 14, 'parpol_name' => 'PARTAI DEMOKRAT', 'parpol_alias' => 'DEMOKRAT', 'parpol_picture' => '14 Partai Demokrat.png']
        ]);
    }
}
