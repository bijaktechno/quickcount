<?php

use Faker\Generator as Faker;

$factory->define(App\Setting::class, function (Faker $faker) {
	return [
		'website_title' => 'Golkar',
		'logo' => 'logo.png',
		'favicon' => 'favicon.png',
		'about_us' => 'Golkar.',
		'copyright' => 'Copyright 2018 Golkar, All rights reserved.',
		'email' => 'golkar@gmail.com',
		'phone' => '+8801717888464',
		'mobile' => '+8801761913331',
		'fax' => '808080',
		'address_line_one' => 'House# 83, Road# 16, Sector# 11',
		'address_line_two' => 'Uttara',
		'state' => 'Uttara',
		'city' => 'Dhaka',
		'zip' => '1230',
		'country' => 'Jabar',
		'map_iframe' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2642905.2881059386!2d89.27605108245604!3d23.817470325158617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30adaaed80e18ba7%3A0xf2d28e0c4e1fc6b!2sBangladesh!5e0!3m2!1sen!2sbd!4v1520764767552" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
		'facebook' => 'https://facebook.com/Golkar',
		'twitter' => 'https://twitter.com/Golkar',
		'google_plus' => 'https://plus.google.com/+Golkar',
		'linkedin' => 'https://www.linkedin.com/company/Golkar/',
		'meta_title' => 'Golkar',
		'meta_keywords' => 'Golkar',
		'meta_description' => 'Golkar.',
		'gallery_meta_title' => 'Golkar',
		'gallery_meta_keywords' => 'Golkar',
		'gallery_meta_description' => 'Golkar.',
	];
});
