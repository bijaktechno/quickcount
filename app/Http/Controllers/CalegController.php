<?php

namespace App\Http\Controllers;

use App\Caleg;
use App\CalegGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Image;
use Purifier;
use Illuminate\Support\Facades\DB;

class CalegController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $calegs = Caleg::leftJoin('user_votes', 'user_votes.caleg_id', '=', 'caleg.id')
                    ->select(array('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', 'caleg.publication_status', 'caleg.caleg_gender', DB::raw('SUM(score) as score_average')
                    ))
                    ->whereNotIn('publication_status', [0])
                    ->groupBy('caleg.dapil_id',  'caleg.caleg_name', 'caleg.id', 'caleg.caleg_picture', 'caleg.frame_color', 'caleg.caleg_nik', 'caleg.publication_status', 'caleg.caleg_gender')
                    ->orderBy('score_average', 'desc')
                    ->paginate(9);
        return view('admin.caleg.index', compact('calegs'));
    }


    public function get(Request $request) {
        $to = $request->get('order')[0]['column'];
        $field = $request->get('columns')[$to]['name'];

        $count = Caleg::leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
                ->leftJoin('caleg_groups', 'caleg_groups.id', '=', 'dapil.group_id')
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('caleg.caleg_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('caleg_groups.caleg_group_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('dapil.dapil_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->count();

        $caleg = Caleg::leftJoin('dapil', 'dapil.id', '=', 'caleg.dapil_id')
                ->leftJoin('caleg_groups', 'caleg_groups.id', '=', 'dapil.group_id')
                ->select(array('caleg.*', 'dapil.dapil_name', 'caleg_groups.caleg_group_name'))
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('caleg.caleg_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('caleg_groups.caleg_group_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('dapil.dapil_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->offset($request->get('start'))
                ->limit($request->get('length'))
                ->orderBy($field, $request->get('order')[0]['dir'])
                ->get();
        
        // dd($caleg);

        $data = array(
            'recordsTotal' => $count, 
            'recordsFiltered' => $count, 
            'data' => $caleg
        );

        // return Response::json($tps);
        return $data;

        // $caleg = Caleg::with('caleg_group')->get();
        // var_dump($caleg);

        /*return datatables()->of($caleg)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->addColumn('dapil_name', function ($caleg) {
                return $caleg->dapil->dapil_name;
            })
            ->addColumn('caleg_group_name', function ($caleg) {
                return $caleg->caleg_group->caleg_group_name;
            })
            ->addColumn('publication_status', function ($caleg) {
                if ($caleg->publication_status == 1) {
                    return '<a href="' . route('admin.unpublishedCalegRoute', $caleg->id) . '" class="btn btn-success btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Unpublished"><i class="icon fa fa-arrow-down"></i>Published</a>';
                }
                return '<a href="' . route('admin.publishedCalegRoute', $caleg->id) . '" class="btn btn-warning btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Published"><i class="icon fa fa-arrow-up"></i> Unpublished</a>';})
            ->addColumn('action', function ($caleg) {
                return '<button class="btn btn-info btn-xs view-button" data-id="' . $caleg->id . '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button> 
                    <a href="'. route('admin.caleg.edit', $caleg->id) .'" class="btn btn-primary btn-xs edit-button" data-id="' . $caleg->id . '"data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a> 
                    <button class="btn btn-danger btn-xs delete-button" data-id="' . $caleg->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})
            ->addColumn('caleg_picture', function ($caleg) {
                if(!empty($caleg->caleg_picture)):
                $image = '<img src="'.asset('public/caleg_picture/' . $caleg->caleg_picture).'" alt="'.$caleg->caleg_name.'" width="60" class="img img-thumbnail img-responsive">';
                else:
                $image = '<img src="'.asset('public/avatar/user.png').'" alt="'.$caleg->caleg_name.'" width="60" class="img img-thumbnail img-responsive">';
                endif;
                return $image;})
            ->rawColumns(['publication_status', 'action', 'dapil_name', 'caleg_group_name', 'caleg_picture'])
            ->setRowId('id')
            ->make(true);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $caleg_groups = CalegGroup::whereNotIn('publication_status', [0])->get();

        return view('admin.caleg.create', compact('caleg_groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'dapil_id'              => 'required',
            'kabupaten_id'          => 'required',
            'caleg_group_id'        => 'required',
            'caleg_name'            => 'required|string|max:255',
            'caleg_picture'         => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:10240|dimensions:max_width=10000,max_height=10000',
            'caleg_gender'          => 'required',
            'frame_color'           => 'required',
            // 'caleg_nik'             => 'nullable|min:5|max:150|unique:caleg',
            'no_urut'               => 'required',
            'publication_status'    => 'required',
        ], [
            'caleg_picture.dimensions' => 'Max dimensions 600x600',
        ]);

        if ($validator->passes()) {
            $caleg = Caleg::create([
                'dapil_id'              => $request->input('dapil_id'),
                'kabupaten_id'          => $request->input('kabupaten_id'),
                'caleg_group_id'        => $request->input('caleg_group_id'),
                'caleg_name'            => $request->input('caleg_name'),
                'caleg_gender'          => $request->input('caleg_gender'),
                'frame_color'           => $request->input('frame_color'),
                // 'caleg_nik'             => $request->input('caleg_nik'),
                'no_urut'               => $request->input('no_urut'),
                'publication_status'    => $request->input('publication_status'),
            ]);

            if ($request->hasFile('caleg_picture')) {
                $image = $request->file('caleg_picture');
                $file_name = $this->caleg_picture($caleg->id, $image);
                Caleg::find($caleg->id)->update(['caleg_picture' => $file_name]);
            }

            if (!empty($caleg->id)) {
                return Response::json(['status' => true,'message' => 'Data add successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }
        }   

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    public function caleg_picture($id, $image) {
        $filename = $id . '.' . $image->getClientOriginalExtension();
        $location = get_caleg_picture_path($filename);
        // create new image with transparent background color
        $background = Image::canvas(100, 100);
        // read image file and resize it to 200x200
        $img = Image::make($image);
        // Image Height
        $height = $img->height();
        // Image Width
        $width = $img->width();
        $x = NULL;
        $y = NULL;
        if ($width > $height) {
            $y = 100;
        } else {
            $x = 100;
        }
        //Resize Image
        $img->resize($x, $y, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        // insert resized image centered into background
        $background->insert($img, 'center');
        // save
        $background->save($location);
        return $filename;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id=null)
    {
        if($id==null){
            $data['data'] = Caleg::where([['caleg_name','LIKE',"%{$request->get('q')}%"]])
                ->when(!empty($request->get('dapil_id')) , function ($query) use($request){
                        return $query->where('dapil_id', $request->get('dapil_id'));
                })
                ->whereNotIn('publication_status', [0])
                ->orderBy('no_urut', 'asc')
                ->get(['id', 'caleg_name', 'no_urut']);
        }
        else{
            $data['caleg'] = Caleg::where('id', $id)->first();
        }

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $caleg = Caleg::where('id', $id)->first();
        $caleg_groups = CalegGroup::whereNotIn('publication_status', [0])->get();

        return view('admin.caleg.edit', compact('caleg', 'caleg_groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($id);
        $caleg = Caleg::find($id);

        if ($caleg->caleg_nik == $request->caleg_nik) {
            $caleg_nik = "required|min:5|max:150";
        } else {
            $caleg_nik = "required|min:5|max:150|unique:caleg";
        }

        if ($caleg->no_urut == $request->no_urut) {
            $no_urut = "required";
        } else {
            $no_urut = "required|unique:caleg";
        }

        $validator = $validator = Validator::make($request->all(), [
            'dapil_id'              => 'required',
            'kabupaten_id'          => 'required',
            'caleg_group_id'        => 'required',
            'caleg_name'            => 'required|string|max:255',
            'caleg_picture'         => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:10240|dimensions:max_width=10000,max_height=10000',
            'caleg_gender'          => 'required',
            'frame_color'           => 'required',
            // 'caleg_nik'             => $caleg_nik,
            'no_urut'               => 'required',
            'publication_status'    => 'required',
        ], [
            'caleg_picture.dimensions' => 'Max dimensions 600x600',
        ]);


        if ($validator->passes()) {

            $caleg->dapil_id            = $request->input('dapil_id');
            $caleg->kabupaten_id        = $request->input('kabupaten_id');
            $caleg->caleg_group_id      = $request->input('caleg_group_id');
            $caleg->caleg_name          = $request->input('caleg_name');
            $caleg->caleg_gender        = $request->input('caleg_gender');
            $caleg->frame_color         = $request->input('frame_color');
            // $caleg->caleg_nik           = $request->input('caleg_nik');
            $caleg->no_urut             = $request->input('no_urut');
            $caleg->publication_status  = $request->input('publication_status');

            if ($request->hasFile('caleg_picture')) {
                $image = $request->file('caleg_picture');
                $file_name = $this->caleg_picture($id, $image);

                $caleg->caleg_picture  = $file_name;
            }

            $affected_row = $caleg->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => true,'message' => 'Data update successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }
        }   

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $caleg = Caleg::find($id);
        if (count($caleg)) {
            //$caleg->tags()->detach();
            if ($caleg->caleg_picture) {
                @unlink(get_caleg_picture_path($caleg->caleg_picture));
            }
            $caleg->delete();
            return redirect()->back()->with('message', 'caleg delete successfully.');
        } else {
            return redirect()->back()->with('exception', 'caleg not found !');
        }
    }

    public function published($id) {
        $affected_row = Caleg::where('id', $id)
            ->update(['publication_status' => 1]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Published successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    public function unpublished($id) {
        $affected_row = Caleg::where('id', $id)
            ->update(['publication_status' => 0]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Unpublished successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }
}
