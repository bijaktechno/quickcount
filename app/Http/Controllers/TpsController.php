<?php

namespace App\Http\Controllers;

use App\Tps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

class TpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.tps.index');
    }

    /*public function gets(Request $request) {
        $tps = Tps::limit(50)->get();

        return datatables()->of($tps)
            ->editColumn('created_at', '{{ date("d F Y", strtotime($created_at)) }}')
            ->editColumn('updated_at', '{{ date("d F Y", strtotime($updated_at)) }}')
            ->addColumn('desa_name', function ($tps) {
                return $tps->desa->desa_name;
            })
            ->addColumn('publication_status', function ($tps) {
                if ($tps->publication_status == 1) {
                    return '<a href="' . route('admin.unpublishedTpsRoute', $tps->id) . '" class="btn btn-success btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Unpublished"><i class="icon fa fa-arrow-down"></i>Published</a>';
                }
                return '<a href="' . route('admin.publishedTpsRoute', $tps->id) . '" class="btn btn-warning btn-xs btn-flat btn-block" data-toggle="tooltip" data-original-title="Click to Published"><i class="icon fa fa-arrow-up"></i> Unpublished</a>';})
            ->addColumn('action', function ($tps) {
                return '<button class="btn btn-info btn-xs view-button" data-id="' . $tps->id . '" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button> 
                    <button class="btn btn-primary btn-xs edit-button" data-id="' . $tps->id . '"data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button> 
                    <button class="btn btn-danger btn-xs delete-button" data-id="' . $tps->id . '"data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></button>';})
            ->rawColumns(['publication_status', 'action', 'desa_name'])
            ->setRowId('id')
            ->make(true);
    }*/


    public function get(Request $request) {
        // dd($request->get('order')[0]['column']);
        
        $to = $request->get('order')[0]['column'];
        $field = $request->get('columns')[$to]['name'];

        $count = Tps::leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                    ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                    ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                    ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                        $query->where(function($q) use($request){
                            return $q->where('tps.tps_name', 'like', '%'.$request->get("search")['value'].'%')
                                ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%')
                                ->orWhere('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                                ->orWhere('kabupaten.kabupaten_name', 'like', '%'.$request->get("search")['value'].'%');
                        });
                    })
                    ->count();

        $tps = Tps::leftJoin('desa', 'desa.id', '=', 'tps.desa_id')
                ->leftJoin('kecamatan', 'kecamatan.id', '=', 'desa.kecamatan_id')
                ->leftJoin('kabupaten', 'kabupaten.id', '=', 'kecamatan.kabupaten_id')
                ->when(!empty($request->get("search")['value']) , function ($query) use($request){
                    $query->where(function($q) use($request){
                        return $q->where('tps.tps_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('desa.desa_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kecamatan.kecamatan_name', 'like', '%'.$request->get("search")['value'].'%')
                            ->orWhere('kabupaten.kabupaten_name', 'like', '%'.$request->get("search")['value'].'%');
                    });
                })
                ->select(array('tps.id', 'tps.desa_id', 'tps.tps_name', 'tps.jumlah_dpt', 'tps.tps_coordinate', 'tps.publication_status',  'tps.created_at', 'tps.updated_at', 'desa.desa_name', 'kecamatan.kecamatan_name','kabupaten.kabupaten_name', 'tps.tps_coordinate as action'))
                ->offset($request->get('start'))
                ->limit($request->get('length'))
                ->orderBy($field, $request->get('order')[0]['dir'])
                ->get();

        $data = array(
            'recordsTotal' => $count, 
            'recordsFiltered' => $count, 
            'data' => $tps
        );

        // return Response::json($tps);
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $validator = Validator::make($request->all(), [
            'desa_id'               => 'required',
            'tps_name'              => 'required',
            'jumlah_dpt'            => 'nullable',
            'tps_coordinate'        => 'nullable',
            'publication_status'    => 'required',
        ], [
            'tps_name.required'     => 'Nama tps harus diisi.',
        ]);

        if ($validator->passes()) {
            $tps = Tps::create([
                'desa_id'               => $request->input('desa_id'),
                'tps_name'              => $request->input('tps_name'),
                'jumlah_dpt'            => $request->input('jumlah_dpt'),
                'tps_coordinate'        => $request->input('tps_coordinate'),
                'publication_status'    => $request->input('publication_status'),
            ]);

            $tps_id = $tps->id;

            if (!empty($tps_id)) {
                return Response::json(['status' => true,'message' => 'Data add successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }

        }

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id=null)
    {
        if($id==null){
            $data['data'] = Tps::where([['tps_name','LIKE',"%{$request->get('q')}%"]])->when(!empty($request->get('desa_id')) , function ($query) use($request){
                    return $query->where('desa_id', $request->get('desa_id'));
                })->whereNotIn('publication_status', [0])->get(['id', 'tps_name']);
        }
        else{
            $data['tps'] = Tps::where('id', $id)->with('desa','desa.kecamatan','desa.kecamatan.kabupaten')->first();
        }

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tps = Tps::find($id);

        $validator = $validator = Validator::make($request->all(), [
            'desa_id'               => 'required',
            'tps_name'              => 'required',
            'jumlah_dpt'            => 'nullable',
            'tps_coordinate'        => 'nullable',
            'publication_status'    => 'required',
        ], [
            'tps_name.required'  => 'Nama tps harus diisi.',
        ]);

        if ($validator->passes()) {

            $tps->desa_id = $request->get('desa_id');
            $tps->tps_name = $request->get('tps_name');
            $tps->jumlah_dpt = $request->get('jumlah_dpt');
            $tps->tps_coordinate = $request->get('tps_coordinate');
            $tps->publication_status = $request->get('publication_status');
            $affected_row = $tps->save();

            if (!empty($affected_row)) {
                return Response::json(['status' => true,'message' => 'Data update successfully.']);
            } else {
                return Response::json(['status' => false,'message' => 'Operation failed !']);
            }

        }

        return Response::json(['status' => false, 'message' => $validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Tps::find($id);
        if (count($data)) {
            $data->delete();
            return redirect()->back()->with('message', 'Data delete successfully.');
        } else {
            return redirect()->back()->with('exception', 'Operation failed !');
        }
    }

    public function published($id) {
        $affected_row = Tps::where('id', $id)
            ->update(['publication_status' => 1]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Published successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }

    public function unpublished($id) {
        $affected_row = Tps::where('id', $id)
            ->update(['publication_status' => 0]);

        if (!empty($affected_row)) {
            return redirect()->back()->with('message', 'Unpublished successfully.');
        }
        return redirect()->back()->with('exception', 'Operation failed !');
    }
}
