<?php

namespace App\Http\Controllers;

use App\CalegGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

class CaleggroupController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id=null)
    {
        if($id==null){
            $data['data'] = CalegGroup::where([['caleg_group_name','LIKE',"%{$request->get('q')}%"]])->whereNotIn('publication_status', [0])->get(['id', 'caleg_group_name']);
        }
        else{
            $data['caleg_groups'] = CalegGroup::where('id', $id)->first();
        }

        return Response::json($data);
    }
}
