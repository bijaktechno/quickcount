@extends('admin.layouts.app')
@section('title', 'Rekap Suara')


@section('style')
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css"> -->

	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/dataTables.bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{ asset('public/admin/datatable/css/buttons.dataTables.min.css')}}">
  	<link rel="stylesheet" href="{{ asset('public/admin/css/jquery.inputpicker.css')}}">
	@endsection

	@section('content')
	<!-- Page header -->
	<section class="content-header">
		<h1>
			Rekap Suara
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> Dashboard</a></li>
			<li class="active">Rekap Suara</li>
		</ol>
	</section>
	<!-- /.page header -->

	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<!-- <h3 class="box-title">Manage Rekap Suara</h3> -->

  				<div id="filter-parpol" class="form-inline" style="width: 100%;">

			        <!-- <div class="col-lg-2"> -->
			          <div class="form-group">
						<select class="form-control" name="filter_parpol" id="filter_parpol">
							<option selected value="dapil">Dapil</option>
							<option value="kabupaten">Kabupaten</option>
							<option value="kecamatan">Kecamatan</option>
							<option value="desa">Desa</option>
							<option value="tps">TPS</option>
						</select>
			          </div>
			        <!-- </div> -->

					<!-- <button type="button" class="btn btn-info btn-sm btn-flat add-button"><i class="fa fa-plus"></i> Add Rekap Suara</button> -->
				</div>

			</div>
			<!-- /.box-header -->
			<div id="cart-area" class="box-body">

	            <ul class="nav nav-tabs" role="tablist">
	                <li role="presentation" class="active"><a href="#chart" aria-controls="chart" role="tab" data-toggle="tab">Grafik</a></li>
	                <li role="presentation"><a href="#table" aria-controls="table" role="tab" data-toggle="tab">Table</a></li>
	            </ul>

	            <div class="tab-content">
	                <div role="tabpanel" class="tab-pane active" id="chart">


				      	<canvas id="calegChart" style="max-width: 100%;"></canvas>

	            	</div>

	                <div role="tabpanel" class="tab-pane" id="table">
						<div style="width: 100%; padding-left: -10px; padding-top: 20px;">
				                	
							<div class="table-responsive">
								<table id="vote-table" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0">
									<thead>
										<tr>
											<th>Wilayah</th>
											<th>Total Suara</th>
										</tr>
									</thead>
								</table>
							</div>

						</div>

	            	</div>
	            </div>



			<!-- /.box-body -->
			<div class="box-footer clearfix">
			</div>
		</div>
	</section>
	<!-- /.main content -->



	<!-- view user modal -->
	<div id="user-view-modal" class="modal fade bs-example-modal-lg print-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="btn-group pull-right no-print">
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" id="print-button" data-toggle="tooltip" data-original-title="Print">
								<i class="fa fa-print"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
						<div class="btn-group">
							<button class="tip btn btn-default btn-flat btn-sm" data-toggle="tooltip" data-original-title="Close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-remove"></i>
								<span class="hidden-sm hidden-xs"></span>
							</button>
						</div>
					</div>
					<h4 class="modal-title" id="view-name"></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-9">
							<table class="table table-bordered table-striped">
								<tbody>
									<tr>
										<td width="20%">Role</td>
										<td id="view-role"></td>
									</tr>
									<tr>
										<td>Username</td>
										<td id="view-username"></td>
									</tr>
									<tr>
										<td>Email</td>
										<td id="view-email"></td>
									</tr>
									<tr>
										<td>Gender</td>
										<td id="view-gender"></td>
									</tr>
									<tr>
										<td>Phone</td>
										<td id="view-phone"></td>
									</tr>
									<tr>
										<td>Address</td>
										<td id="view-address"></td>
									</tr>
									<tr>
										<td>Facebook</td>
										<td id="view-facebook"></td>
									</tr>
									<tr>
										<td>Twitter</td>
										<td id="view-twitter"></td>
									</tr>
									<tr>
										<td>Google Plus</td>
										<td id="view-google-plus"></td>
									</tr>
									<tr>
										<td>Linkedin</td>
										<td id="view-linkedin"></td>
									</tr>
									<tr>
										<td>Status</td>
										<td id="view-status"></td>
									</tr>
									<tr>
										<td>About</td>
										<td id="view-about"></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-3">
							<img id="view-avatar" class="img-responsive img-thumbnail img-rounded">
						</div>
					</div>
				</div>
				<div class="modal-footer no-print">
					<button type="button" class="btn btn-default btn-flat" data-dismiss="modal" aria-label="Close">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- /.view user modal -->
	@endsection

	@section('script')
	<!-- <script type="text/javascript" src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script> -->

	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/datatables.bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.flash.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/pdfmake.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/vfs_fonts.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.html5.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.print.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/admin/datatable/js/buttons.colVis.min.js') }}"></script>
  	<script type="text/javascript" src="{{ asset('public/admin/js/jquery.inputpicker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/web/js/mdb.min.js') }}"></script>

	<script type="text/javascript">
	    var dataLabels=[], dataValues=[], dataColor=[], dataTitle='';

	    $(document).ready(function(){

	      dataChart();
		  
		  get_table_data();


	      $("#filter_parpol").on('change', function(){

	        // alert(dapil_id);
	        // var dapil_id = $(this).data("id");

	        dataLabels = [];
	        dataValues = [];
	        dataColor = [];
	        $("#calegChart").remove();
	        $("#chart").append('<canvas id="calegChart" style="max-width: 100%;"></canvas>');

	        dataChart();

	        $('#vote-table').DataTable().ajax.reload();

	      })

	    });
	    
	    function dataChart()
	    {
	        var filter_parpol 	= $('#filter_parpol').val()?$('#filter_parpol').val():null;

	        var url = "{{ route('allParpolRoute') }}";
	        var formData = {
	          'filter_parpol'  : filter_parpol
	        }


	        $.ajax({
	          url : url,
	          type : 'GET',
	          data: formData,
	          dataType: "json",
	          success:function(data){
	            // console.log(data);
	            $.each(data.data, function(index, value)
	            {

	              dataLabels.push(value.data_name);
	              dataValues.push(Number(value.score_average));
	              dataColor.push('#ffed00');
	              dataTitle = filter_parpol;
	            });

	            // $('#calegChart').data('bar').update(pourcent);
	            // $("#calegChart").remove();

	            getChart();
	            
	          }
	        })
	    }

	    function getChart()
	    {
	      console.log(dataLabels);
	      console.log(dataValues);
	      console.log(dataColor);

	      var ctx = document.getElementById("calegChart").getContext('2d');
	      var myChart = new Chart(ctx, {
	        type: 'bar',
	        data: {
	          labels: dataLabels,
	          datasets: [{
	            label: dataTitle,
	            data: dataValues,
	            backgroundColor: dataColor,
	            borderColor: dataColor,
	            borderWidth: 1
	          }]
	        },
	        options: {
	          scales: {
	            yAxes: [{
	              ticks: {
	                beginAtZero: true
	              }
	            }]
	          }
	        }
	      });
	    }

		/** Get datatable **/
		function get_table_data(){
			var vote_table = $('#vote-table').DataTable({
				dom: 'Blfrtip',
				buttons: [
				{ extend: 'copy', exportOptions: { columns: ':visible'}},
				{ extend: 'print', exportOptions: { columns: ':visible'}},
				{ extend: 'pdf', orientation: 'landscape', pageSize: 'A4', exportOptions: { columns: ':visible'}},
				{ extend: 'csv', exportOptions: { columns: ':visible'}},
				{ extend: 'colvis', text:'Column'},
				],
				columnDefs: [ {
					targets: -1,
					visible: true
				} ],
				lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				processing: true,
				serverSide: true,
				ajax: {
					"type"   : "GET",
					"url"    : "{{ route('admin.getParpolRoute') }}",
					"data"   : function( d ) {
							      d.filter_parpol 	= $('#filter_parpol').val();
							   },
    				// "dataSrc": ""
				},
				columns: [
				{data: 'data_name', name: 'data_name'},
				{data: 'score_average', name: 'score_average'},
				],
				// order: [[1, 'desc']],
			});
	
			// vote_table.ajax.reload();
		}

	</script>
	@endsection