<?php

namespace App\Providers;

use App\Category;
use App\Comment;
use App\Page;
use App\Post;
use App\Setting;
use App\Tag;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		//View::share('setting', 'query');

		View::composer(['web.includes.header', 'web.includes.footer', 'web.includes.sidebar', 'admin.includes.header', 'admin.includes.footer', 'web.includes.head', 'mobile.includes.head', 'admin.includes.head', 'auth.login'], function ($view) {
			$setting = Setting::first();
			$view->with(compact('setting'));
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}
}
