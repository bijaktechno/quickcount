<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'group_name', 'publication_status'
    ];

    public function user() {
        return $this->hasMany(User::class);
    }
}
