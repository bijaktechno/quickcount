@extends('admin.layouts.app')
@section('title', 'User')


@section('style')
<link rel="stylesheet" href="{{ asset('public/admin/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/admin/css/jquery.inputpicker.css')}}">
<style type="text/css">
.modal-body {
    position: relative;
    padding: 25px;
}
.modal-content {
    position: relative;
    background-color: #fff;
    border: 1px solid #999;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 6px;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
    background-clip: padding-box;
    outline: 0;
}
</style>
@endsection

@section('content')
<!-- Page header -->

<section class="content-header">
	<h1>
		User
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ route('admin.dashboardRoute') }}"><i class="fa fa-home"></i> Dashboard</a></li>
		<li><a href="{{ route('admin.users.index') }}">User</a></li>
		<li class="active">Edit User</li>
	</ol>
</section>
<!-- /.page header -->

<!-- Main content -->
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit User</h3>

			<div class="box-tools">
				<a href="{{ route('admin.users.index') }}" class="btn btn-info btn-sm btn-flat"><i class="fa fa-list"></i> Manage User</a>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<form id="user_add_form" name="user_add_form" class="form-horizontal" action="{{ route('admin.updateUserRoute', $user->id) }}" method="POST" enctype="multipart/form-data"  onSubmit="return false">
				{{ csrf_field() }}

				<div class="form-group">
					<label for="group_id" class="col-md-2 control-label">Group</label>
					<div class="col-md-9">
						<select name="group_id" class="form-control" id="group_id">
							<option value="" selected disabled>Select One</option>
							@foreach($groups as $group)
							<option value="{{ $group->id }}">{{ $group->group_name}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="kabupaten_id" class="col-md-2 control-label">kabupaten Asal</label>

					<div class="col-md-9">
						<input type="text" name="kabupaten_id" class="form-control" id="kabupaten_id" placeholder="ex: Nama kabupaten" value="{{ $user->desa->kecamatan->kabupaten_id }}" {{isAuthor()?'disabled':''}}>
					</div>
				</div>

				<div class="form-group">
					<label for="kecamatan_id" class="col-md-2 control-label">kecamatan Asal</label>

					<div class="col-md-9">
						<input type="text" name="kecamatan_id" class="form-control" id="kecamatan_id" placeholder="ex: Nama kecamatan" value="{{ $user->desa->kecamatan_id }}">
					</div>
				</div>

				<div class="form-group">
					<label for="desa_id" class="col-md-2 control-label">desa Asal</label>

					<div class="col-md-9">
						<input type="text" name="desa_id" class="form-control" id="desa_id" placeholder="ex: Nama desa" value="{{ $user->desa_id }}">
					</div>
				</div>

				<div id="form_tps_id" class="form-group">
					<label for="tps_id" class="col-md-2 control-label">TPS</label>

					<div class="col-md-9">
						<input type="text" name="tps_id" class="form-control" id="tps_id" placeholder="ex: Nama TPS" value="{{ $user->tps_id }}">
					</div>
				</div>

				<div class="form-group">
					<label for="nik" class="col-md-2 control-label">NIK User</label>
					<div class="col-md-9">
						<input type="text" name="nik" class="form-control" id="nik" value="{{ $user->nik }}" placeholder="ex: NIK User">
					</div>
				</div>

				<div class="form-group">
					<label for="name" class="col-md-2 control-label">Nama Lengkap</label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control" id="name" value="{{ $user->name }}" placeholder="ex: Nama Lengkap">
					</div>
				</div>


				<div class="form-group">
					<label for="username" class="col-md-2 control-label">Username</label>
					<div class="col-md-9">
						<input type="text" name="username" class="form-control" id="username" value="{{ $user->username }}" placeholder="ex: Nama user">
					</div>
				</div>

				<div class="form-group">
					<label for="password" class="col-md-2 control-label">Password</label>
					<div class="col-md-9">
						<input type="password" name="password" class="form-control" id="password" value="" placeholder="ex: Password">
					</div>
				</div>

				<div class="form-group">
					<label for="email" class="col-md-2 control-label">email</label>
					<div class="col-md-9">
						<input type="email" name="email" class="form-control" id="email" value="{{ $user->email }}" placeholder="ex: Email">
					</div>
				</div>

				<div class="form-group">
					<label for="avatar" class="col-md-2 control-label">Foto Profil</label>
					<div class="col-md-9">
						<input type="file" name="avatar" id="avatar" class="form-control">
					</div>
				</div>
				
				<div class="form-group">
					<label for="gender" class="col-md-2 control-label">Jenis Kelamin</label>
					<div class="col-md-9">
						<select name="gender" class="form-control" id="gender">
							<option value="" selected disabled>Select One</option>
							<option value="L">Laki-Laki</option>
							<option value="P">Perempuan</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="activation_status" class="col-md-2 control-label">Status Aktifasi</label>
					<div class="col-md-9">
						<select name="activation_status" class="form-control" id="activation_status">
							<option value="" selected disabled>Select One</option>
							<option value="1">Activate</option>
							<option value="0">Unactivate</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<button type="submit" class="btn btn-info btn-flat" id="store-button">Edit User</button>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box-body -->
		<div class="box-footer clearfix">
		</div>
	</div>
</section>
<!-- /.main content -->
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('public/admin/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/jscolor.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/jquery.inputpicker.js') }}"></script>

<script type="text/javascript">
	// $(function () {
	$(document).ready(function() {
		var date = new Date();
		//date.setDate(date.getDate()-1);
		cek_form_tps();
		get_kabupaten_picker();

		if(kabupaten_id){
			get_kecamatan_picker();	
		}
		if(kecamatan_id){
			get_desa_picker();	
		}
		if(desa_id){
			get_tps_picker();		
		}

        $('#user_date').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            startDate: date,
        });
        $('#user_date').datepicker('setDate', 'now');

		$('.summernote').summernote({
			height: 200
		})

		$("#kabupaten_id").change(function(){

			$("#kecamatan_id").val('');
			$("#desa_id").val('');
			$("#tps_id").val('');

			get_kecamatan_picker();
			get_desa_picker();
			get_tps_picker();
		});

		$("#kecamatan_id").change(function(){

			$("#desa_id").val('');
			$("#tps_id").val('');

			get_desa_picker();
			get_tps_picker();
		});

		$("#desa_id").change(function(){

			$("#tps_id").val('');

			get_tps_picker();
		});

		$("#group_id").change(function(){
			cek_form_tps();
		})

		/** Store **/

		$("#user_add_form").on('submit', function(){
	        var idBtn = "#store-button";
			var defaultBtn = $("#store-button").html();
        	// var form_data = $(this).closest('form').serialize();
        	var act = $(this).attr('action');
        	var method = $(this).attr('method');

			$.ajax({
				url:act,
				type:method,
				data:new FormData(this),
	            contentType: false,
	            cache: false,
	            processData:false,
				success:function(data) {
					if(data.status == false) {
	        			var arr = data.message;
	        			var messages = '';

		                $.each(arr, function(index, value)
		                {
		                    if (value.length != 0)
		                    {
		                    	messages += value+"<br>";
		                        // $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
		                    }
		                });

	                    $('.alert-danger').animate({ top: "0" }, 500).show();
	                    $('.alert-danger').html(messages);

	                    setTimeout(function(){
	                        hideAllMessages();
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                    }, 4000);
					}
					if(data.status == true) {

	                    $('.alert-success').animate({ top: "0" }, 500).show();
	                    $('.alert-success').html(data.message);

	                    setTimeout(function () {
	                        $(idBtn).removeClass("disabled");
	                        $(idBtn).html(defaultBtn);
	                        location.reload();
	                    }, 2000);
					}
				},
			});
		});
	})

	function cek_form_tps(){
		var group_id = $("#group_id").val();

		if(group_id == 2){
			$("#form_tps_id").show();
		}else{
			$("#tps_id").val('');
			$("#form_tps_id").hide();
		}

		get_tps_picker();
	}

    function get_kabupaten_picker(){
    	var dapil_id = $("#dapil_id").val();

      $('#kabupaten_id').inputpicker({
          url: "{{ route('allKabupatenRoute') }}",
          urlParam:{'dapil_id':dapil_id},
          fields:['kabupaten_name'],
          fieldText:'kabupaten_name',
          fieldValue:'id',
          filterOpen: true,
      })
    }

    function get_kecamatan_picker(){
    	var kabupaten_id = $("#kabupaten_id").val();

      $('#kecamatan_id').inputpicker({
          url: "{{ route('allKecamatanRoute') }}",
          urlParam:{'kabupaten_id':kabupaten_id},
          fields:['kecamatan_name'],
          fieldText:'kecamatan_name',
          fieldValue:'id',
          filterOpen: true,
      })
    }

    function get_desa_picker(){
    	var kecamatan_id = $("#kecamatan_id").val();

      $('#desa_id').inputpicker({
          url: "{{ route('allDesaRoute') }}",
          urlParam:{"kecamatan_id":kecamatan_id},
          fields:['desa_name'],
          fieldText:'desa_name',
          fieldValue:'id',
          filterOpen: true,
      })
    }

    function get_tps_picker(){
    	var desa_id = $("#desa_id").val();

      $('#tps_id').inputpicker({
          url: "{{ route('allTpsRoute') }}",
          urlParam:{"desa_id":desa_id},
          fields:['tps_name'],
          fieldText:'tps_name',
          fieldValue:'id',
          filterOpen: true,
      })
    }
</script>
<script type="text/javascript">
	document.forms['user_add_form'].elements['group_id'].value = "{{ $user->group_id }}";
	document.forms['user_add_form'].elements['gender'].value = "{{ $user->gender }}";
	document.forms['user_add_form'].elements['activation_status'].value = "{{ $user->activation_status }}";
</script>
@endsection