<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Dapil;
use App\Kabupaten;
use App\Kecamatan;
use App\Desa;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class WilayahController extends Controller
{
    public function wilayah()
    {
        $data = Dapil::with('kabupaten','kabupaten.kecamatan','kabupaten.kecamatan.desa','kabupaten.kecamatan.desa.tps')->get();

        return Response::json($data);

    }
}
