<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DapilCalegtps extends Model
{
	protected $table = 'dapil_caleg_tps';

    protected $fillable = [
        'caleg_id', 'caleg_group_id', 'caleg_name', 'dapil_id', 'dapil_name', 'kabupaten_id', 'caleg_picture', 'no_urut', 'caleg_nik', 'frame_color', 'kecamatan_id', 'desa_id', 'tps_id', 'tps_name', 'score'
    ];


    public function dapil()
    {
        return $this->belongsTo(Dapil::class);
    }
}
